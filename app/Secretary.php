<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Hash;

class Secretary extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $guard = 'secr';
    protected $fillable = ['name', 'email', 'password'];
    protected $hidden = ['password', 'remember_token'];

    public function getSecrPersonnelNoSubordinates($boss = null)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        
        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $boss
            . "/clossestSubordinates"
            , false
            , stream_context_create($arrContextOptions)
        );
        $data = [];
        $data = json_decode($json, true);
        return $data;   
    }

    public function getBossSubordinates()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        
        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $this->boss
            . "/clossestSubordinates"
            , false
            , stream_context_create($arrContextOptions)
        );

        $data = json_decode($json, true);
        $datanik = array_column($data,'personnel_no');
        return $datanik;   
    }

    public function getSecretaryBossSubordinates()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        
        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $this->boss
            . "/clossestSubordinates"
            , false
            , stream_context_create($arrContextOptions)
        );

        $bossjson = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $this->boss
            , false
            , stream_context_create($arrContextOptions)
        );

      
        $data = $bossdata = [];
        $data = json_decode($json, true);
        $bossdata = json_decode($bossjson, true);
        $customData = $datadet = $customdataboss = [];
      
        foreach($data as $result)
        {
            $datadet =  [
                            'label' => $result['personnel_no']." ".$result['name'], 'value' => $result['personnel_no']
                        ];

            array_push($customData,$datadet);
        }
       
        $customdataboss = [
            'label' => $bossdata['personnel_no']." ".$bossdata['name'], 'value' => $bossdata['personnel_no']
        ];

        array_push($customData,$customdataboss);

        return $customData;
    }

    public function saveSecretary($email = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/Secretary/"
            . $email
            , false
            , stream_context_create($arrContextOptions)
        );

        $data = json_decode($json,true);

        $secr           = new Secretary();
        $secr->name     = $data[0]['name'];
        $secr->boss     = $data[0]['boss'];
        $secr->email    = $data[0]['email'];
        $secr->password = Hash::make(str_random(32));
        $secr->save();

    }

    public function checkSecretary($email = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        @$json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/Secretary/"
            . $email
            , false
            , stream_context_create($arrContextOptions)
        );
        
        $data = json_decode($json,true);

        if($json ==  false)
        {
            return [];
        }
        else 
        {
            $data = json_decode($json,true);
            return $data;
        }

    }

    public function setNikBoss($id)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/".$id."/minManagerBoss"
            , false
            , stream_context_create($arrContextOptions)
        );
        $data = json_decode($json,true);
        if(!is_null($data))
        {
            return $data;
        }
    }

    public function getSecrBossSubordinates($personnel_no)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        
        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personnel_no
            . "/subordinates"
            , false
            , stream_context_create($arrContextOptions)
        );

        $bossjson = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personnel_no
            , false
            , stream_context_create($arrContextOptions)
        );

      
        $data = $bossdata = [];
        $data = json_decode($json, true);
        $bossdata = json_decode($bossjson, true);
        $customData = $datadet = $customdataboss = [];
      
        foreach($data as $result)
        {
            $datadet =  [
                            'label' => $result['personnel_no']." ".$result['name'], 'value' => $result['personnel_no']
                        ];

            array_push($customData,$datadet);
        }
       
        $customdataboss = [
            'label' => $bossdata['personnel_no']." ".$bossdata['name'], 'value' => $bossdata['personnel_no']
        ];

        array_push($customData,$customdataboss);

        return $customData;
    }
}
