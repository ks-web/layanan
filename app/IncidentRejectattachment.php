<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentRejectattachment extends Model
{
    protected $fillable = ['attachment','name','alias'];

    public function incident()
    {
        return $this->belongsTo('App\Incident');
    } 
}
