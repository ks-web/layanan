<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Difficulty;
use App\Priority;
use App\RequestChangeBps;
use App\RequestChange;
use App\Stage;
use App\Action;
use App\Status;
use App\RequestChangeBpsDuration;
use App\RequestChangeBpsAction;
use App\ServiceCoordinator;
use App\RequestChangeAction;
use App\RequestChangeBpsActionNote;
use App\Role;
use App\Service;
use App\RequestChangeRejectAttachment;
use App\RequestChangeBpsAttachment;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class RequestChangeBpsController extends Controller
{
    public function index()
    {
        $difficulties = Difficulty::all();
        $priorities = Priority::all();
        $bps = RequestChangeBps::all();

        $countMyRequest = RequestChange::ofLoggedUser()
            ->get()
            ->count();

        $countAllRequest = RequestChange::all()
            ->count();

        $countAllRequestBps = RequestChangeBps::all()
            ->count();

        // dd($requestchange);
        return view('bps.index', compact('bps', 'countMyRequest', 'countAllRequest', 'countAllRequestBps'));
    }


    public function showBps($id)
    {
        $difficulties = Difficulty::all();
        $priorities = Priority::all();
        $requestchange = RequestChangeBps::with('RequestChange')->where('id',$id)->first();
        // dd($requestchange);
        $view = view('request_changes._modal-bps', compact('requestchange','difficulties','priorities'))->render();
        return response()->json(['html' => $view]);
    }

    public function viewBps($id)
    {
        $difficulties = Difficulty::all();
        $priorities = Priority::all();
        $requestchange = RequestChangeBps::with('RequestChange')->where('id',$id)->first();
        // dd($requestchange);
        return view('request_changes._databps', compact('requestchange','difficulties','priorities'));
    }


/* 
   begin of function service owner
*/
    // show form edit bps
    public function SoEditBps($id)
    {
        // get data difficult
        $difficulties = Difficulty::all();
        // get data periority
        $priorities = Priority::all();
        // get data requestchange
        $requestchangeBps = RequestChangeBps::find($id);
        return view('bps.so.edit-bps', compact('requestchangeBps','difficulties','priorities'));
    }

    // show form edit bps
    public function SoUpdateBps(Request $request, $id)
    {
        try {
            // get data crf
            
            $requestchange = RequestChange::find($id);

            $deleteDuration = RequestChangeBpsDuration::where('request_change_bps_id',$id);
            $deleteDuration->delete();

            // dd($request->input('activity')[1]);
            if($request->input('activity') != null) 
            {
                $jumlah = count($request->input('activity'));
            }
           

            // update stage Request Change
            $rc = RequestChangeBps::find($id);

            // update stage bps
            $RchangeBps = RequestChangeBps::where('id', $id)
                ->update([
                    'stage_id' => Stage::waitingForCoordinatorSo()->first()->id,
                    'status_id'=> Status::approved()->first()->id,
                    'priority_id' => $request->input('priority'),
                    'difficulty_id' => $request->input('difficulty'),
                    'preparedby' => $request->input('preparedby'),
                    'module' => $request->input('module'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'bpj' => $request->input('bpj'),
                    'global_design' => $request->input('global_design'),
                    'detail_specification' => $request->input('detail_specification'),
                    'effort' => $request->input('effort'),
                    'start_plan_date' => $request->input('start_plan_date'),
                    'finish_plan_date' => $request->input('end_plan_date'),
            ]);
            
            // save to request change bps duration
            for($i=0; $i<$jumlah; $i++)
            {
                $RequestChangeBpsDuration =  RequestChangeBpsDuration::create([
                    'request_change_bps_id' => $id,
                    'activity' => $request->input('activity')[$i],
                    'mandays' => $request->input('mandays')[$i],
                    'pic' => $request->input('pic')[$i],
                    'level_id' => $request->input('level')[$i],
                    'start_date' => $request->input('start_date')[$i],
                    'finish_date' => $request->input('end_date')[$i]
                ]);
            }

             // insert data attachment 
             if(!empty($request->file('attachment')))
             {
                 foreach($request->file('attachment') as $files)
                 {
                     // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                     $dateNow    = Carbon::now()->toDateTimeString();
                     $date       = Carbon::parse($dateNow)->format('dmYHis');
                     $name       = $files->getClientOriginalName();
                     $username   = Auth::user()->id;
                     $filename   = $date.$username.$name;
 
                     // upload data
                     $item = $files->storeAs('attachments', $filename);
 
                     // input data file
                     $RchangeBps = RequestChangeBpsAttachment::create([
                        'request_change_bps_id' => $id,
                        'attachment' => $item, 
                        'name' => $filename, 
                        'alias' => $name
                    ]);
                 }
             }
            

            // delete attachment
            if(!empty($request->input('statusid')))
            {
                foreach($request->input('statusid') as $value)
                {   
                    if($value != null) 
                    {
                        // dd($value);
                        $attachDelete = RequestChangeBpsAttachment::find($value);
                        $attachDelete->delete();
                    }
                }

            }

            
            // input request change bps action
            $RchangeBpsaction =  RequestChangeBpsAction::create([
                'request_change_bps_id' => $id,
                'user_id'               => Auth::user()->id,
                'status_id'             => Status::approved()->first()->id,
                'action_type'           => Action::uploadBps()->first()->id,
                'stage_id'              => Stage::waitingForCoordinatorSo()->first()->id
            ]);
            
            // direct
            return redirect()
                ->route('approval.show', 'bps')
                ->with('success','BPS Berhasil dirubah.');

        } catch(Exception $e) {
            return redirect()
                ->route('crf.so.form.bps')
                ->with('success','Error '.$e);
        }
    }

    // show form create bps
    public function SoFormBps($id)
    {
        // get data difficult
        $difficulties = Difficulty::all();
        // get data periority
        $priorities = Priority::all();
        // get data requestchange
        $requestchange = RequestChange::find($id);
        return view('bps.so.form-bps', compact('requestchange','difficulties','priorities'));
    }

    public function soCreateBps(Request $request, $id)
    {
        try {
            // get data crf
            $requestchange = RequestChange::find($id);

            $rcbps = RequestChange::where('id', $id)
            ->update([
                'stage_id' => Stage::ticketCreated()->first()->id,
                'action_id' => Action::uploadBps()->first()->id
            ]);

            // input request change action
            $rchangeaction =  RequestChangeAction::create([
                'request_change_id' => $id,
                'user_id'           => Auth::user()->id,
                'status_id'         => Status::approved()->first()->id,
                'action_type'       => Action::uploadBps()->first()->id,
                'stage_id'          => Stage::ticketCreated()->first()->id,
                'role_id'           => Role::roleServicedesk()->first()->id
            ]);

            // dd($request->input('activity')[1]);
            $jumlah = count($request->input('activity'));
            $return = $jumlah;



            // update stage Request Change
            $rc = RequestChangeBps::find($id);

            // update stage bps
            $RchangeBps = RequestChangeBps::create([
                    'request_change_id' => $id,
                    'service_id' => $requestchange->service_id,
                    'stage_id' => Stage::waitingForCoordinatorSo()->first()->id,
                    'status_id'=> Status::approved()->first()->id,
                    'priority_id' => $request->input('priority'),
                    'difficulty_id' => $request->input('difficulty'),
                    'preparedby' => $request->input('preparedby'),
                    'module' => $request->input('module'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'bpj' => $request->input('bpj'),
                    'global_design' => $request->input('global_design'),
                    'detail_specification' => $request->input('detail_specification'),
                    'effort' => $request->input('effort'),
                    'start_plan_date' => $request->input('start_plan_date'),
                    'finish_plan_date' => $request->input('end_plan_date'),
            ]);

            if ($jumlah == 0){
                if(!empty($request->file('attachment')))
                {
                    foreach($request->file('attachment') as $files)
                    {
                        // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                        $dateNow    = Carbon::now()->toDateTimeString();
                        $date       = Carbon::parse($dateNow)->format('dmYHis');
                        $name       = $files->getClientOriginalName();
                        $username   = Auth::user()->id;
                        $filename   = $date.$username.$name;
    
                        // upload data
                        $item = $files->storeAs('attachments', $filename);
    
                        // input data file
                        $RchangeBps->requestChangeBpsAttachments()
                            ->save(new RequestChangeBpsAttachment([
                                'attachment' => $item, 
                                'name' => $filename, 
                                'alias' => $name
                            ]));
                    }
                }
            }

            else{

                if(!empty($request->file('attachment')))
                {
                    foreach($request->file('attachment') as $files)
                    {
                        // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                        $dateNow    = Carbon::now()->toDateTimeString();
                        $date       = Carbon::parse($dateNow)->format('dmYHis');
                        $name       = $files->getClientOriginalName();
                        $username   = Auth::user()->id;
                        $filename   = $date.$username.$name;
    
                        // upload data
                        $item = $files->storeAs('attachments', $filename);
    
                        // input data file
                        $RchangeBps->requestChangeBpsAttachments()
                            ->save(new RequestChangeBpsAttachment([
                                'attachment' => $item, 
                                'name' => $filename, 
                                'alias' => $name
                            ]));
                    }
                }

                

                for($i=0; $i<$jumlah; $i++)
                {
                    $RchangeBps->requestChangeBpsDurations()->save(new RequestChangeBpsDuration([
                        'activity' => $request->input('activity')[$i],
                        'mandays' => $request->input('mandays')[$i],
                        'pic' => $request->input('pic')[$i],
                        'level_id' => $request->input('level')[$i],
                        'start_date' => $request->input('start_date')[$i],
                        'finish_date' => $request->input('end_date')[$i]
                    ]));
                }


            }


            // insert data attachment 


            // // input request change action
            // $RchangeBpsaction =  RequestChangeBpsAction::create([
            //     'request_change_bps_id' => $id,
            //     'user_id'               => Auth::user()->id,
            //     'status_id'             => Status::approved()->first()->id,
            //     'action_type'           => Action::uploadBps()->first()->id,
            //     'stage_id'              => Stage::waitingForCoordinatorSo()->first()->id
            // ]);
            
            return redirect()
                ->route('approval.show', 'crf')
                ->with('success','BPS Berhasil dibuat.');

        } catch(Exception $e) {
            return redirect()
                ->route('crf.so.form.bps')
                ->with('success','Error '.$e);
        }
    }

/*
    end of service owner function
*/


/*
    begin of coordinator manager beict function
*/

    public function managerFormApprovalBps($id)
    {
        // get data difficult
        $difficulties = Difficulty::all();
        // get data periority
        $priorities = Priority::all();
        // get data requestchange
        $requestchangeBps = RequestChangeBps::find($id);
        return view('bps.manager.form-approval-bps', compact('requestchangeBps','difficulties','priorities'));
    }

    public function managerApprovalBps(Request $request, $id)
    {
        try {
            if($request->input('aksi') == 1)
            {
                // aksi jika menyetujui
                $rc = RequestChangeBps::find($id);
            
                if($rc->status_id == 2)
                {
                    $rcbps = RequestChangeBps::where('id', $id)
                        ->update([
                            'stage_id' => Stage::approveKaseya()->first()->id,
                        ]);

                    // // input request change action
                    // $rchangeaction =  RequestChangeAction::create([
                    //     'request_change_id' => $id,
                    //     'user_id'           => Auth::user()->id,
                    //     'status_id'         => Status::approved()->first()->id,
                    //     'action_type'       => Action::approve()->first()->id,
                    //     'stage_id'          => Stage::waitingForManagerBeict()->first()->id
                    // ]);

                    // input request change bps action
                    $rchangeaction =  RequestChangeBpsAction::create([
                        'request_change_bps_id' => $id,
                        'user_id'               => Auth::user()->id,
                        'status_id'             => Status::approved()->first()->id,
                        'action_type'           => Action::approve()->first()->id,
                        'stage_id'              => Stage::approveKaseya()->first()->id
                    ]);

                    // check note
                    if(!empty($request->input('note')))
                    {
                        // save note
                        $rchangeaction->RequestChangeBpsActionNotes()->save(new RequestChangeBpsActionNote(['note' => $request->input('note')]));
                    }

                    return redirect()
                        ->route('approval.show','bps')
                        ->with('success','Bps berhasil disetujui.');
                }
                elseif($rc->status_id == 3)
                {
                    // aksi jika memperbaiki
                    $rc = RequestChangeBps::find($id);
                
                    // update stage menjadi update bps
                    $rcbps = RequestChangeBps::where('id', $id)
                        ->update([
                            'stage_id' => Stage::updateBps()->first()->id,
                        ]);

                    // input request change bps action
                    $rchangeaction =  RequestChangeBpsAction::create([
                        'request_change_bps_id' => $id,
                        'user_id'               => Auth::user()->id,
                        'status_id'             => Status::approved()->first()->id,
                        'action_type'           => Action::approve()->first()->id,
                        'stage_id'              => Stage::updateBps()->first()->id
                    ]);

                    // check note
                    if(!empty($request->input('note')))
                    {
                        // save note
                        $rchangeaction->RequestChangeBpsActionNotes()->save(new RequestChangeBpsActionNote(['note' => $request->input('note')]));
                    }

                    return redirect()
                        ->route('approval.show','bps')
                        ->with('success','Bps berhasil dikembalikan ke service owner.');
                    }
            }
            elseif($request->input('aksi') == 2)
            {
                // aksi jika menolak
                $rbps = RequestChangeBps::find($id);
        
                $rcbps = RequestChangeBps::where('id', $id)
                ->update([
                    'stage_id' => Stage::requestRejected()->first()->id,
                ]);

                // input request change bps action
                $rchangeaction =  RequestChangeBpsAction::create([
                    'request_change_bps_id' => $id,
                    'user_id'               => Auth::user()->id,
                    'status_id'             => Status::rejected()->first()->id,
                    'action_type'           => Action::reject()->first()->id,
                    'stage_id'              => Stage::requestRejected()->first()->id
                ]);

                $rc = RequestChange::where('id', $rbps->request_change_id)
                ->update([
                    'stage_id' => Stage::requestRejected()->first()->id,
                    'action_id' => Action::reject()->first()->id
                ]);

                // input request change bps action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $rbps->request_change_id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::rejected()->first()->id,
                    'action_type'       => Action::reject()->first()->id,
                    'stage_id'          => Stage::requestRejected()->first()->id,
                    'role_id'           => Role::roleManagerBeict()->first()->id
                ]);

                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }

                // insert data attachment 
                if(!empty($request->file('attachment')))
                {
                    foreach($request->file('attachment') as $files)
                    {
                        // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                        $dateNow    = Carbon::now()->toDateTimeString();
                        $date       = Carbon::parse($dateNow)->format('dmYHis');
                        $name       = $files->getClientOriginalName();
                        $username   = Auth::user()->id;
                        $filename   = $date.$username.$name;

                        // upload data
                        $item = $files->storeAs('attachments', $filename);

                        // input data file
                        $requestchange = RequestChangeRejectAttachment::create([
                                'request_change_id' => $rbps->request_change_id,
                                'attachment'    => $item, 
                                'name'          => $filename, 
                                'alias'         => $name
                            ]);
                    }
                }

                return redirect()
                    ->route('approval.show','bps')
                    ->with('success','Permintaan Layanan Berhasil di Tolak.');
            }
        } catch(Exception $e) {
            return redirect()
                ->route('crf.index')
                ->with('success','Error '.$e.', persetujuan gagal dilakukan.');
        }
        
    }

/*
    end of coordinator manager beict function
*/

/*
    begin of coordinator service owner function
*/

    public function coFormApprovalBps($id)
    {
        // get data difficult
        $difficulties = Difficulty::all();
        // get data periority
        $priorities = Priority::all();
        // get data requestchange
        $requestchangeBps = RequestChangeBps::find($id);
        return view('bps.coordinator.form-approval-bps', compact('requestchangeBps','difficulties','priorities'));
    }


    public function coApprovalBps(Request $request, $id)
    {
        try {
            if($request->input('aksi') == "1")
            {
                // aksi jika setuju
                $rc = RequestChangeBps::find($id);
            
                $rcbps = RequestChangeBps::where('id', $id)
                ->update([
                    'stage_id' => Stage::waitingForManagerBeict()->first()->id,
                ]);

                // // input request change action
                // $rchangeaction =  RequestChangeAction::create([
                //     'request_change_id' => $id,
                //     'user_id'           => Auth::user()->id,
                //     'status_id'         => Status::approved()->first()->id,
                //     'action_type'       => Action::approve()->first()->id,
                //     'stage_id'          => Stage::waitingForManagerBeict()->first()->id
                // ]);

                // input request change bps action
                $rchangeaction =  RequestChangeBpsAction::create([
                    'request_change_bps_id' => $id,
                    'user_id'               => Auth::user()->id,
                    'status_id'             => Status::approved()->first()->id,
                    'action_type'           => Action::approve()->first()->id,
                    'stage_id'              => Stage::waitingForManagerBeict()->first()->id
                ]);

                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeBpsActionNotes()->save(new RequestChangeBpsActionNote(['note' => $request->input('note')]));
                }

                return redirect()
                    ->route('approval.show','bps')
                    ->with('success','Bps berhasil disetujui.');
            }
            elseif($request->input('aksi') == 2) 
            {
                // aksi jika menolak
                $rc = RequestChangeBps::find($id);
        
                $rcbps = RequestChangeBps::where('id', $id)
                ->update([
                    'stage_id' => Stage::waitingForManagerBeict()->first()->id,
                ]);

                // input request change bps action
                $rchangeaction =  RequestChangeBpsAction::create([
                    'request_change_bps_id' => $id,
                    'user_id'               => Auth::user()->id,
                    'status_id'             => Status::rejected()->first()->id,
                    'action_type'           => Action::reject()->first()->id,
                    'stage_id'              => Stage::waitingForManagerBeict()->first()->id
                ]);
 
                 // check note
                 if(!empty($request->input('note')))
                 {
                     // save note
                     $rchangeaction->RequestChangeBpsActionNotes()->save(new RequestChangeBpsActionNote(['note' => $request->input('note')]));
                 }

                 return redirect()
                    ->route('approval.show','bps')
                    ->with('success','Bps berhasil ditolak.');
 
            }
            elseif($request->input('aksi') == 3)
            {
                // aksi jika memperbaiki
                $rc = RequestChangeBps::find($id);
            
                $rcbps = RequestChangeBps::where('id', $id)
                ->update([
                    'stage_id' => Stage::updateBps()->first()->id,
                ]);

                // // input request change action
                // $rchangeaction =  RequestChangeAction::create([
                //     'request_change_id' => $id,
                //     'user_id'           => Auth::user()->id,
                //     'status_id'         => Status::approved()->first()->id,
                //     'action_type'       => Action::approve()->first()->id,
                //     'stage_id'          => Stage::waitingForManagerBeict()->first()->id
                // ]);

                // input request change bps action
                $rchangeaction =  RequestChangeBpsAction::create([
                    'request_change_bps_id' => $id,
                    'user_id'               => Auth::user()->id,
                    'status_id'             => Status::approved()->first()->id,
                    'action_type'           => Action::approve()->first()->id,
                    'stage_id'              => Stage::updateBps()->first()->id
                ]);

                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeBpsActionNotes()->save(new RequestChangeBpsActionNote(['note' => $request->input('note')]));
                }

                return redirect()
                    ->route('approval.show','bps')
                    ->with('success','Bps berhasil dikembalikan ke service owner.');
            }
            
        } catch(Exception $e) {
            return redirect()
                ->route('crf.index')
                ->with('success','Error '.$e.', persetujuan gagal dilakukan.');
        }
        
    }
    /*
        end of
    */
  
}
