<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use UrlSigner;
use App\ITRequest;
use App\Service;
use App\Stage;
use App\Status;
use App\Category;
use App\RequestApproval;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreITRequestRequest;
use App\Http\Requests\SpictActionRequest;
use App\Http\Requests\ApprovrsaveITRequestRequest;
use App\Http\Requests\SoActionRequest;
use Illuminate\Support\Facades\Storage;
use App\Notifications\RequestCreated;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\RequestReasonfile;
use App\RequestAttachment;
use App\RequestSo;
use App\RequestSoBps;
use App\ServiceCoordinator;
use App\User;
use App\Action;
use App\RequestAction;
use App\RequestActionNote;
use Carbon\Carbon;
use App\RequestRejectattachment;
use App\Incident;
use App\RequestChange;
use App\RequestChangeBps;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        $stage  = null;
        $file   = null;
        $urlpath = "approvals";
        $userId = Auth::user()->id;

        if(Auth::user()->hasAllRoles(['employee']))
        {
            // flow of boss (atasan)
            if(Auth::user()->hasAllRoles(['boss']))
            {
                // flow of manager beict
                if(Auth::user()->hasRole('manager beict'))
                {
                    //get count of data request
                    $countWaitingApproved = ITRequest::where('stage_id', '12')
                        ->where('boss', Auth::user()->id)  
                        ->where('stage_id', '1')
                        ->orWhere(function($q){
                        $q->ofBossSubordinates();
                        $q->where('stage_id', '1');
                    })
                    ->get()
                    ->count();

                    // new request change
                    $stage_1 = RequestChange::where('boss', Auth::user()->id)  
                            ->where('stage_id', '1')
                            ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '1');
                        })
                        ->get()
                        ->count();
                    
                    $stage_12 = RequestChange::where('stage_id','12')
                        ->get()
                        ->count();  

                    $countRequestChange = $stage_1 + $stage_12;
    
                    $countRequestChangeBps = RequestChangeBps::where('stage_id','12')
                        ->get()
                        ->count(); 

        

                    // end request change

                    if($id == 'request')
                    {
                        //get data request
                        $paginated  = ITRequest::where('stage_id', '12')
                        ->where('boss', Auth::user()->id)
                        ->where('stage_id', '1')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '1');
                        })->orderBy('updated_at', 'asc')->get();

                        $file = 'approvals.index-request';

                        // return data
                        return view($file,compact('paginated','userId','countWaitingApproved','countRequestChange','urlpath','countRequestChangeBps'));

                        // return view($file,compact('paginated','userId','countWaitingApproved','urlpath'));
                    }
                    elseif($id == 'crf')
                    {
                        $stage_1 = RequestChange::where('boss', Auth::user()->id)
                            ->where('stage_id', '1')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '1');
                            })->orderBy('updated_at', 'asc')->get();

                        $stage_12 = RequestChange::where('stage_id', '12')
                            ->get();

                        $requestchange = $stage_1->merge($stage_12);
                        
                        return view(
                            'approvals.index-crf', 
                            compact('requestchange','userId','countWaitingApproved','countRequestChange','urlpath','countRequestChangeBps'));
                    }
                    elseif($id == 'bps')
                    {
                        $bps =  RequestChangeBps::where('stage_id','12')
                            ->get();
                        
                        return view(
                            'approvals.index-bps', 
                            compact('bps','userId','countWaitingApproved','countRequestChange','urlpath','countRequestChangeBps'));
                    }
                   
                    // $paginated  = ITRequest::ofBossSubordinates()
                    //     ->whereIn('stage_id', ['1','12'])
                    //     ->get();

                    // $countWaitingApproved = $paginated->count();

                    // name of file view
                }
                else
                {
                    // get count of data request
                    $countWaitingApproved = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '1')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '1');
                        })
                        ->get()
                        ->count();

                    // get data request
                    // $paginated  = ITRequest::where('boss', Auth::user()->id)
                    //     ->where('stage_id', '1')
                    //     ->orWhere(function($q){
                    //         $q->ofBossSubordinates();
                    //         $q->where('stage_id', '1');
                    //     })
                    //     ->get();

                    // request change get waiting approve
                    $stage_1 = RequestChange::ofBossSubordinates()
                        ->where('boss', Auth::user()->id)
                        ->where('stage_id','1')
                        ->get()
                        ->count();    

                    // 

                    $countRequestChange = $stage_1;
                    
                    if($id == 'request')
                    {
                        //get data request
                        $paginated  = ITRequest::where('stage_id', '12')
                        ->where('boss', Auth::user()->id)
                        ->where('stage_id', '1')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '1');
                        })->orderBy('updated_at', 'asc')->get();

                        $file = 'approvals.index-request';

                        // return data
                        return view($file,compact('paginated','userId','countWaitingApproved','countRequestChange','urlpath','countRequestChangeBps'));

                        // return view($file,compact('paginated','userId','countWaitingApproved','urlpath'));
                    }
                    elseif($id == 'crf')
                    {
                        $requestchange = RequestChange::where('boss', Auth::user()->id)
                            ->where('stage_id', '1')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '1');
                            })->orderBy('updated_at', 'asc')->get();

                        $stage_12 = RequestChange::where('stage_id', '12')
                            ->get();

                        // $requestchange = $stage_1->merge($stage_12);
                        
                        return view(
                            'approvals.index-crf', 
                            compact('requestchange','userId','countWaitingApproved','countRequestChange','urlpath','countRequestChangeBps'));
                    }
                    elseif($id == 'bps')
                    {
                        $bps =  RequestChangeBps::where('stage_id','12')
                            ->get();
                        
                        return view(
                            'approvals.index-bps', 
                            compact('bps','userId','countWaitingApproved','countRequestChange','urlpath','countRequestChangeBps'));
                    }

                    // end
                    // name file of view 
                    // $file= 'approvals.index-request';

                    // return data
                    // return view($file,compact('paginated','userId','stage','urlpath','countWaitingApproved','countRequestChange'));
                    // return view($file,compact('paginated','userId','stage','urlpath','countWaitingApproved'));

                }
            }

            // flow manager BE & ICT


            if(Auth::user()->hasRole('operation sd'))
            {   
                // get count of data srf
                $countWaitingApproved   = ITRequest::where('stage_id', '2')
                    ->get()
                    ->count();
                
                // mengambil jumlah data incident yang dieskalasi serf
                $countEscalation    = Incident::where('stage_id','2')
                    ->get()
                    ->count();

                // mengambil jumlah data request change stage 2 (menunggu persetujuan specialist operation desk)
                $countRequestChange = RequestChange::where('stage_id', '2')
                    ->get()
                    ->count();
                

                // flow of specialist operation desk
                if($id == 'request')
                {
                    // get data request
                    $paginated  = ITRequest::where('stage_id', '2')
                        ->get();
                
                    // return data
                    return view('approvals.index-request',compact('paginated','userId','stage','urlpath','countWaitingApproved','countEscalation','countRequestChange'));
                    // return view('approvals.index-request',compact('paginated','userId','stage','urlpath','countWaitingApproved','countEscalation'));
                }
                elseif($id == 'incident')
                {
                    // mengambil data ayang dieskalasi
                    $paginated  = Incident::where('stage_id','2')
                        ->get();

                    // return data
                    return view('approvals.index-incident', compact(
                        'paginated'
                        ,'stage'
                        ,'urlpath'
                        ,'countEscalation','countWaitingApproved','countRequestChange')
                    );

                    // return view('approvals.index-incident', compact(
                    //     'paginated'
                    //     ,'stage'
                    //     ,'urlpath'
                    //     ,'countEscalation','countWaitingApproved')
                    // );
        
                }
                elseif($id == 'crf')
                {
                    // mengambil jumlah data request change stage 2 (menunggu persetujuan specialist operation desk)
                    $requestchange  = RequestChange::where('stage_id', '2')
                        ->get();

                    return view('approvals.index-crf', compact(
                        'requestchange'
                        ,'stage'
                        ,'urlpath'
                        ,'countEscalation','countWaitingApproved')
                    );
                }
                elseif($id == 'bps'){
                    // mengambil jumlah data request change stage 2 (menunggu persetujuan specialist operation desk)
                    $requestchange  = RequestChangeBps::where('stage_id', '2')
                    ->get();
                    return view('approvals.index-crf', compact(
                        'requestchange'
                        ,'stage'
                        ,'urlpath'
                        ,'countEscalation','countWaitingApproved')
                    );
                }
            }

            elseif (Auth::user()->hasRole(['boss'])) {
                
            }
            elseif( Auth::user()->hasAllRoles(['operation ict']))
            {
                // mengambil data count waiting approved
                $countWaitingApproved = ITRequest::where('stage_id','7')->get()->count();

                // mengambil data
                $paginated  = ITRequest::where('stage_id','7')->get();

                // file of view
                $file       = 'approvals.index-request';

                return view($file,compact('paginated','stage','urlpath','countWaitingApproved'));
            }

            // roles all service owners
            elseif(Auth::user()->hasAllRoles(['so web']) 
            or Auth::user()->hasAllRoles(['so mes flat']) 
            or Auth::user()->hasAllRoles(['so perangkat komputer']) 
            or Auth::user()->hasAllRoles(['so mes long']) 
            or Auth::user()->hasAllRoles(['so sap hr']) 
            or Auth::user()->hasAllRoles(['so sap ppqm']) 
            or Auth::user()->hasAllRoles(['so sap fico'])
            or Auth::user()->hasAllRoles(['so sap sd'])
            or Auth::user()->hasAllRoles(['so sap pm'])
            or Auth::user()->hasAllRoles(['so sap mm']) 
            or Auth::user()->hasAllRoles(['so sap psim'])             
            or Auth::user()->hasAllRoles(['so aplikasi office']) 
            or Auth::user()->hasAllRoles(['so vicon']) 
            or Auth::user()->hasAllRoles(['so printer']) 
            or Auth::user()->hasAllRoles(['so jaringan']) 
            or Auth::user()->hasAllRoles(['operation sd']) 
            or Auth::user()->hasAllRoles(['so messaging']))
            {
                // get count of data srf
                $countWaitingApproved   = ITRequest::where('stage_id', '10')
                ->get()
                ->count();
            
                // mengambil jumlah data incident yang dieskalasi serf
                // $countEscalation    = Incident::where('stage_id','2')
                //     ->get()
                //     ->count();

                // mengambil jumlah data request change stage 2 (menunggu persetujuan specialist operation desk)
                $countRequestChange = RequestChange::where('stage_id', '10')
                    ->get()
                    ->count();
                    
                if($id == 'request')
                {
                    $data = Auth::user()
                        ->roles()
                        ->get()
                        ->toArray();

                    $serv = array_column(
                        Service::whereIn('role_id',array_column($data,'id'))
                        ->get()
                        ->toArray(),'id'
                    );


                    // mengambil data request change (CRF) stage 10 waiting boss approve
                    $countRequestChange = RequestChange::whereIn('stage_id',['10','14'])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->count();

                    // mengambil data request change (BPS) stage 10 waiting boss approve
                    $countRequestChangeBps = RequestChangeBps::whereIn('stage_id',['4','40'])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->count();

                    $paginated = RequestSo::with(['request' => function ($query) {
                        $query->whereIn('stage_id', ['10','13','15','14']);
                    }])->whereIn('service_id',$serv)->get();

                    //dd($paginated);
                    $paginated1 = RequestSo::with('request')
                        ->whereIn('service_id',$serv)
                        ->get();
                    
                    $request_waiting_so_approved = RequestSo::withCount(['request' => function ($query) {
                        $query->whereIn('stage_id', ['10','13','15','14']);
                    }])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->toArray();

                    $arraydata_waiting_so_approved = array_column($request_waiting_so_approved, 'request_count');

                    $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);

                    // dd($counts__waiting_so_approved);
                    if(empty($counts__waiting_so_approved))
                    {
                        $countWaitingApproved = 0;
                    }
                    else
                    {
                        if(isset($counts__waiting_so_approved[1]))
                        {
                            $countWaitingApproved = $counts__waiting_so_approved[1];
                        }
                        else
                        {
                            $countWaitingApproved = 0;
                        }
                    }
                    // $countWaitingApproved               = $counts__waiting_so_approved[1];

                    if(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
                    {
                        $data = array_column(
                            Auth::user()
                                ->roles()
                                ->get()
                                ->toArray(), 'id'
                            );
                            

                        $serv  = array_column(
                            ServiceCoordinator::whereIn('role_id', $data)
                            ->get()
                            ->toArray(),'service_id'
                        );

                        $countRequestChangeBps = RequestChangeBps::where('stage_id','13')
                            ->whereIn('service_id',$serv)
                            ->get()
                            ->count();
                    }
                    $file = 'approvals.indexso-request';

                    return view($file,compact('paginated','stage','urlpath','countWaitingApproved','countRequestChange','countRequestChangeBps'));

                    // return view($file,compact('paginated','stage','urlpath','countWaitingApproved'));
                }
                elseif($id == 'crf')
                {
                    $data = Auth::user()
                        ->roles()
                        ->get()
                        ->toArray();

                    $serv = array_column(
                        Service::whereIn('role_id',array_column($data,'id'))
                        ->get()
                        ->toArray(),'id'
                    );

                    $countRequestChange = RequestChange::whereIn('stage_id',['10','14'])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->count();

                    // mengambil data request change (BPS) stage 10 waiting boss approve
                    $countRequestChangeBps = RequestChangeBps::whereIn('stage_id',['4','40'])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->count();
                    
                    // dd($countRc);

                    // $array = array_count_values(array_column($countRc,'request_change_count'));
                   
                    // if(empty($array))
                    // {
                    //     $countRequestChange = 0;
                    // }
                    // else
                    // {
                    //     if(isset($array[1]))
                    //     {
                    //         $countRequestChange = $array[1];
                    //     }
                    //     else
                    //     {
                    //         $countRequestChange = 0;
                    //     }
                    // }


                    $requestchange = RequestChange::whereIn('service_id',$serv)
                        ->whereIn('stage_id',['10','14'])
                        ->get();

                    // dd($requestchange);
                    $paginated1 = RequestSo::with('request')
                        ->whereIn('service_id',$serv)
                        ->get();
                    
                    $request_waiting_so_approved = RequestSo::withCount(['request' => function ($query) {
                        $query->whereIn('stage_id', ['10','13','15','14']);
                    }])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->toArray();

                    $arraydata_waiting_so_approved = array_column($request_waiting_so_approved, 'request_count');

                    $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);

                    // dd($counts__waiting_so_approved);
                    if(empty($counts__waiting_so_approved))
                    {
                        $countWaitingApproved = 0;
                    }
                    else
                    {
                        if(isset($counts__waiting_so_approved[1]))
                        {
                            $countWaitingApproved = $counts__waiting_so_approved[1];
                        }
                        else
                        {
                            $countWaitingApproved = 0;
                        }
                    }
                    // $countWaitingApproved               = $counts__waiting_so_approved[1];

                    if(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
                    {
                        $data = array_column(
                            Auth::user()
                                ->roles()
                                ->get()
                                ->toArray(), 'id'
                            );

                        $serv  = array_column(
                            ServiceCoordinator::whereIn('role_id', $data)
                            ->get()
                            ->toArray(),'service_id'
                        );

                        $countRequestChangeBps = RequestChangeBps::where('stage_id','13')
                            ->whereIn('service_id',$serv)
                            ->get()
                            ->count();
                    }

                    $file = 'approvals.index-crf-so';

                    return view($file,compact('requestchange','stage','urlpath','countWaitingApproved','countRequestChange','countRequestChangeBps'));

                    // return view($file,compact('requestchange','stage','urlpath','countWaitingApproved'));
                }
                elseif($id == 'bps')
                {
                    $data = Auth::user()
                        ->roles()
                        ->get()
                        ->toArray();

                    $serv = array_column(
                        Service::whereIn('role_id',array_column($data,'id'))
                        ->get()
                        ->toArray(),'id'
                    );

                    $service = $serv;

                    // dd($service);
                   
                    $countRequestChange = RequestChange::whereIn('stage_id',['10','14'])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->count();
                        
                    // mengambil jumlah data request change (BPS) stage 4 "waiting create bps"
                    $countRequestChangeBps = RequestChangeBps::whereIn('stage_id',['4','40'])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->count();

                     // mengambil data request change (BPS) stage 4 "waiting create bps"
                    $bps = RequestChangeBps::whereIn('stage_id',['4','40','13'])
                        ->whereIn('service_id',$serv)
                        ->get();
                        
                    // dd($countRc);

                    // $array = array_count_values(array_column($countRc,'request_change_count'));
                   
                    // if(empty($array))
                    // {
                    //     $countRequestChange = 0;
                    // }
                    // else
                    // {
                    //     if(isset($array[1]))
                    //     {
                    //         $countRequestChange = $array[1];
                    //     }
                    //     else
                    //     {
                    //         $countRequestChange = 0;
                    //     }
                    // }


                    // dd($requestchange);
                    $paginated1 = RequestSo::with('request')
                        ->whereIn('service_id',$serv)
                        ->get();
                    
                    $request_waiting_so_approved = RequestSo::withCount(['request' => function ($query) {
                        $query->whereIn('stage_id', ['10','13','15','14']);
                    }])
                        ->whereIn('service_id',$serv)
                        ->get()
                        ->toArray();

                    $arraydata_waiting_so_approved = array_column($request_waiting_so_approved, 'request_count');

                    $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);

                    // dd($counts__waiting_so_approved);
                    if(empty($counts__waiting_so_approved))
                    {
                        $countWaitingApproved = 0;
                    }
                    else
                    {
                        if(isset($counts__waiting_so_approved[1]))
                        {
                            $countWaitingApproved = $counts__waiting_so_approved[1];
                        }
                        else
                        {
                            $countWaitingApproved = 0;
                        }
                    }
                    // $countWaitingApproved               = $counts__waiting_so_approved[1];

                    $file = 'approvals.index-bps';

                    // if role coordinator
                    if(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
                    {
                        $data = array_column(
                            Auth::user()
                                ->roles()
                                ->get()
                                ->toArray(), 'id'
                            );
                            
                        $service = array_column(
                            Service::whereIn('role_id',($data))
                            ->get()
                            ->toArray(),'id'
                        );


                        $serv  = array_column(
                            ServiceCoordinator::whereIn('role_id', $data)
                            ->get()
                            ->toArray(),'service_id'
                        );

                        // dd($service);

                        $countRequestChangeBps = RequestChangeBps::where('stage_id','13')
                            ->whereIn('service_id',$serv)
                            ->get()
                            ->count();

                        $bps = RequestChangeBps::whereIn('service_id',$serv)
                            ->where('stage_id','13')
                            ->get();
                    }

                    return view($file,compact('bps','stage','urlpath','countWaitingApproved','countRequestChange','countRequestChangeBps','service'));

                    // return view($file,compact('requestchange','stage','urlpath','countWaitingApproved'));
                }
            
            
            }
            elseif(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
            {
                $data = array_column(
                    Auth::user()
                        ->roles()
                        ->get()
                        ->toArray(), 'id'
                    );               

                $serv  = array_column(
                    ServiceCoordinator::whereIn('role_id', $data)
                    ->get()
                    ->toArray(),'service_id'
                );

                $countRequestChangeBps = RequestChangeBps::where('stage_id','13')
                    ->whereIn('service_id',$serv)
                    ->get()
                    ->count();

                $bps = RequestChangeBps::whereIn('service_id',$serv)
                    ->where('stage_id','13')
                    ->get();

                $file = 'approvals.index-bps';

                return view($file,compact(
                    'bps',
                    'countRequestChangeBps'
                    )
                );
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
