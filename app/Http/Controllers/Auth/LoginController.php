<?php

namespace App\Http\Controllers\Auth;

use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Secretary;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers{
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo()
    {
        if (Auth::guard('secr')->check()) {
            return '/';
        } else 
            return '/';
    }

    public function logout(Request $request)
    {
        // do the normal logout
        $this->performLogout($request);
        
        // redirecto to sso
        return redirect()->away('https://sso.krakatausteel.com');
        //11504
    }

    public function programaticallyEmployeeLogin(Request $request, $personnel_no, $email)
    {
        $personnel_no   = base64_decode($personnel_no);
        $email          = base64_decode($email);
        $usercek        = new User();
    
        if(sizeof($usercek->employeeCheck($personnel_no)) != 0)
        {
            if(!is_null($usercek->personelNoChek($personnel_no)))
            {
                // new object user
                $userBoss = new User();
                // truct display user
                $strdisp = $userBoss->employeeCheck($personnel_no);
                // level jabatan
                if(substr($strdisp['esgrp'], 0, 1) == "A")
                {
                    $nikboss    = $personnel_no;
                }
                else 
                {
                    if(sizeof($userBoss->minManangerCheck($personnel_no)) != 0)
                    {
                        $nikboss    = $userBoss->setNikBoss($personnel_no);
                        $statusboss = "1";
                        
                    }
                    else 
                    {
                        if(sizeof($userBoss->minManangerDelegationCheck($personnel_no)) != 0)
                        {
                            $nikboss    = $userBoss->setNikDelegationBoss($personnel_no);
                            $statusboss = "2";
                        }
                        else
                        {
                            return "Atasan anda tidak terdaftar di struktur display silahkan hubungi 7 2743";
                        }
                    }
                    
                }

                //
                try {
                    // find all the details
                    $user = User::where('id', $personnel_no)->first();
                    $boss = User::where('id', $nikboss)->first();
                    
                    
                    // insert user
                    if (is_null($user)) {    
                        // create the new user
                        $saveuser = new User();
                        $saveuser->saveEmployee($personnel_no, $email);
                        $userrole = User::find($personnel_no);
                        $userrole->assignRole(['employee']);
                    }

                    // insert boss
                    // disini
                    if(is_null($boss))
                    {
                        // create the new boss
                        if($statusboss == "1")
                        {
                            $saveboss = new user();
                            $saveboss->saveMinManager($personnel_no);
                            $bossrole = User::find($nikboss);
                            $bossrole->assignRole(['employee','boss']);
                        }
                        else 
                        {
                            $saveboss = new user();
                            $saveboss->saveMinManagerDelegation($personnel_no);
                            $bossrole = User::find($nikboss);
                            $bossrole->assignRole(['employee','boss']);
                        }

                    }

                    // Programmatically login user
                    $userlogin = User::where('id', $personnel_no)->first();
                    if(Auth::check())
                    {
                        return redirect()
                            ->route('home.index');
                    }
                    else
                    {
                        Auth::loginUsingId($userlogin->id);
                        return redirect()
                            ->route('home.index');
                    }
                } catch (ModelNotFoundException $e) {
                    
                    return 'Employee not found!';
                }

                return $this->sendLoginResponse($request);
            }
            else
            {
                return redirect()->away('https://sso.krakatausteel.com');
            }
        }
        else {
            return redirect()->away('https://sso.krakatausteel.com');
        }
    }

    public function programaticallyServiceDeskLogin(Request $request, $email)
    {
        $email      = base64_decode($email);
        $userlogin  = User::where('email', $email)->first();
        if(!is_null($userlogin))
        {
            Auth::loginUsingId($userlogin->id);
            return redirect()
                ->route('home.index');
        }
        else
        {
            return redirect()->away('https://sso.krakatausteel.com');
        }
    }

    public function programaticallySecretaryLogin(Request $request, $email)
    {
        $newSecr    = new Secretary();
        $email      = base64_decode($email);

        // dd(sizeof($newSecr->checkSecretary($email)));
        // check data di api
        if(sizeof($newSecr->checkSecretary($email)) != 0)
        {
            $userlogin  = Secretary::where('email', $email)->first();
        
            if(!is_null($userlogin))
            {
                Auth::guard('secr')->loginUsingId($userlogin->id);
                return redirect()
                    ->route('secretary.index');
            }
            else 
            {
                // save user
                $newSecr->saveSecretary($email);
                $secr = Secretary::where('email',$email)->first();
                $secr->assignRole(['secretary']);
                
                // login
                $user  = Secretary::where('email', $email)->first();
                Auth::guard('secr')->loginUsingId($user->id);
                return redirect()
                    ->route('secretary.index');
            }
        }
        else
        {
            // return 'Tidak terdaftar sebagai secretaries';
            return redirect()->away('https://sso.krakatausteel.com');
        }
    }
}
