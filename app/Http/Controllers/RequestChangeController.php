<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestChange;
use App\Stage;
use App\RequestChangeAttachment;
use App\RequestChangeAction;
use App\RequestChangeActionNote;
use App\RequestChangeBpsActionNote;
use App\RequestChangeBpsDuration;
use App\RequestChangeRejectAttachment;
use App\Status;
use App\Action;
use App\Service;
use App\Difficulty;
use App\Role;
use App\Priority;
use App\RequestChangeBps;
use App\RequestChangeBpsAction;
use App\ServiceCoordinator;
use App\Http\Requests\StoreRequestChangeRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class RequestChangeController extends Controller
{
    // index request change
    public function index()
    {
        // emplye
        if(Auth::user()->hasAllRoles(['employee']))
        {
            // boss
            if(Auth::user()->hasAllRoles(['boss']))
            {
                // manager beict
                if(Auth::user()->hasRole('manager beict'))
                {
                    // $requestchange = RequestChangeBps::with('RequestChange')
                    //     ->get();

                    // //dd($requestchange);
                    // return view(
                    //     'request_changes.index-so', 
                    //     compact('requestchange')
                    // );

                    $requestchange = RequestChange::all();
                    $countMyRequest = RequestChange::ofLoggedUser()
                        ->get()
                        ->count();

                    $countAllRequest = RequestChange::all()
                        ->count();

                    $countAllRequestBps = RequestChangeBps::all()
                        ->count();

                    return view(
                        'request_changes.index', 
                        compact('requestchange', 'countMyRequest', 'countAllRequest','countAllRequestBps')
                    );
                }
                else
                {   
                    // boss
                    $requestchange = RequestChange::ofBossSubordinates()
                        ->get();

                    
                    $countMyRequest = RequestChange::ofLoggedUser()
                        ->get()
                        ->count();

                    $countAllRequest = RequestChange::ofBossSubordinates()
                        ->count();

                    return view(
                        'request_changes.index', 
                        compact('requestchange', 'countMyRequest', 'countAllRequest')
                    );
                }
            }
            elseif(Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so perangkat komputer']) or Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so jaringan']) or Auth::user()->hasAllRoles(['so messaging']))
            {
                if(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
                {
                    $data = array_column(
                        Auth::user()
                            ->roles()
                            ->get()
                            ->toArray(), 'id'
                        );

                    $serv  = array_column(
                        ServiceCoordinator::whereIn('role_id', $data)
                        ->get()
                        ->toArray(),'service_id'
                    );

                    $requestchange = RequestChange::whereIn('service_id',$serv)
                        ->get();

                    // $requestchange = RequestChangeBps::with('RequestChange')
                    //     ->whereIn('service_id',$serv)
                    //     ->get();
                    // dd($requestchange);

                    return view(
                        'request_changes.index-so', 
                        compact('requestchange')
                    );
                }
                else
                {

                    $data = Auth::user()
                        ->roles()
                        ->get()
                        ->toArray();

                    $serv = array_column(
                        Service::whereIn('role_id',
                        array_column($data,'id'))
                        ->get()
                        ->toArray(),'id'
                    );

                    $requestchange = RequestChange::whereIn('service_id',$serv)
                        ->get();
                        
                    // dd($requestchange);
                    return view(
                        'request_changes.index-so', 
                        compact('requestchange')
                    );
                }
            }
            elseif(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
            {
                $data = array_column(
                    Auth::user()
                        ->roles()
                        ->get()
                        ->toArray(), 'id'
                    );

                $serv  = array_column(
                    ServiceCoordinator::whereIn('role_id', $data)
                    ->get()
                    ->toArray(),'service_id'
                );

                $requestchange = RequestChange::whereIn('service_id',$serv)
                    ->get();

                // $requestchange = RequestChangeBps::with('RequestChange')
                //     ->whereIn('service_id',$serv)
                //     ->get();
                // dd($requestchange);

                return view(
                    'request_changes.index-so', 
                    compact('requestchange')
                );
            }
            elseif(Auth::user()->hasAllRoles(['operation sd']))
            {
                // specialist service desk
                $requestchange = RequestChange::all();
                return view(
                    'request_changes.index', 
                    compact('requestchange')
                );
            }
            elseif( Auth::user()->hasAllRoles(['operation ict']))
            {
                
            }
            elseif(Auth::user()->hasAllRoles(['chief']))
            {
                
            }
            else
            {  
                $requestchange = RequestChange::ofLoggedUser()
                    ->get();
                return view('request_changes.index', compact('requestchange')); 
            }

        }
        elseif(Auth::user()->hasAllRoles(['service desk']))
        {
            $requestchange = RequestChange::whereNotIn('stage_id',['9','11','24'])
                ->get();

            $waitingApprovals = RequestChange::where('stage_id', '3')
                ->get()
                ->count();

            $ticketClose = RequestChange::with('RequestChange')
                ->where('stage_id', '9')
                ->get()
                ->count();
            
            $countInprogress = $requestchange->count();

            return view(
                'request_changes.index-servicedesk', 
                compact('requestchange','countInprogress','waitingApprovals','ticketClose')
            );
        }

    }

    public function viewPerstage($id)
    {
         // employee
         if(Auth::user()->hasAllRoles(['employee']))
         {
                if(Auth::user()->hasAllRoles(['manager beict']))
                {
                // $requestchange = RequestChangeBps::with('RequestChange')
                    //     ->get();

                    // //dd($requestchange);
                    // return view(
                    //     'request_changes.index-so', 
                    //     compact('requestchange')
                    // );

                    $requestchange = RequestChange::ofLoggedUser()
                        ->get();

                    $countMyRequest = RequestChange::ofLoggedUser()
                        ->get()
                        ->count();

                    $countAllRequest = RequestChange::all()
                        ->count();

                    $countAllRequestBps = RequestChangeBps::all()
                        ->count();

                    return view(
                        'request_changes.index', 
                        compact('requestchange', 'countMyRequest', 'countAllRequest', 'countAllRequestBps')
                    );
                }
                elseif(Auth::user()->hasAllRoles(['boss']))
                {   
                    // boss
                    $requestchange = RequestChange::ofLoggedUser()
                        ->get();

                    $countMyRequest = RequestChange::ofLoggedUser()
                        ->get()
                        ->count();

                    $countAllRequest = RequestChange::ofBossSubordinates()
                        ->count();

                    return view(
                        'request_changes.index', 
                        compact('requestchange', 'countMyRequest', 'countAllRequest')
                    );
                }
                elseif(Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so perangkat komputer']) or Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so jaringan']) or Auth::user()->hasAllRoles(['so messaging']))
                {
                    $requestchange = RequestChange::ofLoggedUser()->get();

                    // dd($requestchange);
                    return view(
                        'request_changes.index-so', 
                        compact('requestchange')
                    );
                }
            
         }
        elseif(Auth::user()->hasAllRoles(['service desk']))
        {            
            $requestchange = RequestChange::where('stage_id',$id)
                ->get();

            $waitingApprovals = RequestChange::where('stage_id', '3')
                ->get()
                ->count();

            $ticketClose = RequestChange::with('RequestChange')
                ->where('stage_id', '9')
                ->get()
                ->count();
            
            $countInprogress = RequestChange::whereNotIn('stage_id',['9','11','24'])
                ->get()
                ->count();

            return view(
                'request_changes.index-servicedesk', 
                compact('requestchange','countInprogress','waitingApprovals','ticketClose')
            );
        }
    }

    // create data
    public function create()
    {
        if(Auth::user()->hasAllRoles(['employee'])){
            if(Auth::user()->hasAllRoles(['boss'])){
                $services = Service::all();
                return view('request_changes._createBoss', compact('services'));
            }else{    
                $services = Service::all();
                return view('request_changes.create', compact('services'));
            }
        }


    }

    // show detail data crf
    public function showDetailCrf($id)
    {
        $requestchange = RequestChange::where('id',$id)->first();
        // return view('request_changes._modal-detail-crf', compact('requestchange'));
        $view   = view('request_changes._modal-detail-crf', compact('requestchange'))->render();
        return response()->json(['html' => $view]);
    }

    // show data crf
    public function showCrf($id)
    {
        $requestchange = RequestChangeBps::where('request_change_id',$id)->get();
        $view   = view('request_changes._modal-crf', compact('requestchange'))->render();
        return response()->json(['html' => $view]);
    }

    
/*  begin of function boss
    // boss approval form
    // form approval boss
*/
    public function FormBossApproval($id)
    {
        $requestchange = RequestChange::find($id);
        $services = Service::all();
        return view('request_changes.boss.approval', compact('requestchange','services'));
    }

    // process boss approval
    public function BossApproval(RequestChange $requestchange, $id, Request $request)
    {
        try{
            if($request->input('aksi') == '1')
            {
                // input data request change

                $rchange = RequestChange::
                    where('id', $id)
                    ->update([
                        'stage_id' => Stage::waitingForSoApproval()->first()->id,
                        'action_id' => Action::approve()->first()->id
                ]);

                // input request change action
                $rchangeaction = new RequestChangeAction();
                $rchangeaction->request_change_id = $id;
                $rchangeaction->user_id = Auth::user()->id;
                $rchangeaction->status_id = Status::approved()->first()->id;
                $rchangeaction->action_type = Action::approve()->first()->id;
                $rchangeaction->role_id = Role::roleBoss()->first()->id;
                $rchangeaction->stage_id = Stage::waitingForSoApproval()->first()->id;
                $rchangeaction->save();
                
                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->requestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }

                return redirect()
                    ->route('approval.show','crf')
                    ->with('success','Permintaan / perubahan layanan  berhasil disetujui.');
            }
            elseif($request->input('aksi') == '2')
            {
                // input data request change

                $rchange = RequestChange::
                    where('id', $id)
                    ->update([
                        'stage_id' => Stage::requestRejected()->first()->id,
                        'action_id' => Action::reject()->first()->id
                ]);

                // input request change action
                $rchangeaction = new RequestChangeAction();
                $rchangeaction->request_change_id = $id;
                $rchangeaction->user_id = Auth::user()->id;
                $rchangeaction->status_id = Status::rejected()->first()->id;
                $rchangeaction->action_type = Action::reject()->first()->id;
                $rchangeaction->stage_id = Stage::requestRejected()->first()->id;
                $rchangeaction->role_id = Role::roleBoss()->first()->id;
                $rchangeaction->save();
                
                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->requestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }

                return redirect()
                    ->route('approval.show','crf')
                    ->with('success','Permintaan / perubahan layanan  berhasil ditolak.');
            }
        } catch(Exception $e) {
            return redirect()
                ->route('approval.show','crf')
                ->with('success','Error: '.$e.' gagal menolak permintaan / perubahan layanan ');
        }
        
    }
    // end of boss function

    // store data create
    public function store(StoreRequestChangeRequest $request) 
    {

        if(Auth::user()->hasAllRoles(['employee'])){
            if(Auth::user()->hasAllRoles(['boss'])){
                $requestchange = RequestChange::create([
                    'title'             => $request->input('title'),
                    'service_id'        => $request->input('service_id'),
                    'business_need'     => $request->input('business_need'),
                    'business_benefit'  => $request->input('business_benefit'),
                    'stage_id'          => Stage::WaitingForServiceDesk()->first()->id,
                    'action_id'         => Action::ccreate()->first()->id,
                    'boss'              => '',
                    'keterangan'        => $request->input('keterangan'),
                    'telp'              => $request->input('telp'),
                    'location'          => $request->input('location'),
                    'user_id'           => Auth::user()->id
                                            
                ]);
            }else{    
                // insert into request change
                $requestchange = RequestChange::create([
                    'title'             => $request->input('title'),
                    'service_id'        => $request->input('service_id'),
                    'business_need'     => $request->input('business_need'),
                    'business_benefit'  => $request->input('business_benefit'),
                    'stage_id'          => Stage::waitingBossApproval()->first()->id,
                    'action_id'         => Action::ccreate()->first()->id,
                    'keterangan'        => $request->input('keterangan'),
                    'telp'              => $request->input('telp'),
                    'location'          => $request->input('location'),
                    'user_id'           => Auth::user()->id,
                    'boss'              => Auth::user()->boss()->id
                                            
                ]);
            }
        }


        // insert data attachment 
        if(!empty($request->file('attachment')))
        {
            foreach($request->file('attachment') as $files)
            {
                // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                $dateNow    = Carbon::now()->toDateTimeString();
                $date       = Carbon::parse($dateNow)->format('dmYHis');
                $name       = $files->getClientOriginalName();
                $username   = Auth::user()->id;
                $filename   = $date.$username.$name;

                // upload data
                $item = $files->storeAs('attachments', $filename);

                // input data file
                $requestchange->requestChangeAttachments()
                    ->save(new RequestChangeAttachment([
                        'attachment' => $item, 
                        'name' => $filename, 
                        'alias' => $name
                    ]));
            }
        }

        // success redirect to
        return redirect()
            ->route('crf.index')
            ->with('success','Permintaan / perubahan layanan berhasil dibuat.');
    }

/*  begin of function specialist service desk
    // view from spsd escalation (SpsdFormEscalation)
    // process escalation (SpsdEscalation)
*/
    public function SpsdFormEscalation($id)
    {
        // get data request change by id
        $requestchange = RequestChange::find($id);
        // get data servie all
        $services = Service::all();
        // redirect to request_changes/spsd/escalation.blade.php
        return view('request_changes.spsd.escalation', compact('requestchange','services'));
    }

    // process escalation specialis service desk
    public function SpsdEscalation(RequestChange $requestchange, $id, Request $request)
    {
        // dd(!empty($request->input('so')));
        // try catch, handle error process
        try {
            // check modul applikasi
            if(!empty($request->input('so'))) 
            {
                 // dd($request);
                 // update stage 
                  // lopping data
                foreach($request->input('so') as $modul)
                {
                    // insert data to table RequestChangeBps
                    $rcbps = new RequestChangeBps();
                    $rcbps->request_change_id = $id;
                    $rcbps->service_id = $modul;    
                    $rcbps->stage_id = Stage::waitingForSoApproval()->first()->id;
                    $rcbps->save();               
                }

                 $rchange = RequestChange::
                 where('id', $id)
                 ->update([
                     'stage_id' => Stage::ticketCreated()->first()->id
                ]);

                // input request change action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::approved()->first()->id,
                    'action_type'       => Action::approve()->first()->id,
                    'stage_id'          => Stage::waitingForSoApproval()->first()->id
                ]);
                
                // redirect
                return redirect()
                    ->route('approval.show','crf')
                    ->with('success','Modul aplikasi berhasil dipilih.');
            }
            else
            {
                // empty back to the form
                return redirect()
                    ->route('crf.form.spsd.escalation', $id)
                    ->with('success','Error, gagal input data. tidak ada modul aplikasi yang dipilih.');
            }
        } catch(Exception $e) {
            // error back to the form
            return redirect()
                ->route('crf.form.spsd.escalation', $id)
                ->with('success','Error: '.$e.', gagal input data.');
        }
        
    }

//  end of function specialist service desk

/*  Begin of function service ownwer
    
*/

    public function soFormApprovalCrf($id)
    {
        $requestchange = RequestChange::where('id',$id)->first();
        return view('request_changes.so.approval', compact('requestchange'));
    }

    public function soApproval(Request $request, $id)
    {
        // test variabel request
        // dd($request->input('aksi'));
        // dd($id);
        // try 
        try {
            if($request->input('aksi') !== null and $request->input('aksi') == '1')
            {
                // dd($request->input('aksi'));
                // get data request change
                // get data service
                $rc = RequestChange::find($id);
                $service = Service::where('id', $rc->service_id)->first();
                // dd($rc);
                // update stage bps
                $rchange = RequestChange::
                where('id', $id)
                    ->update([
                        'stage_id' => Stage::waitingForManagerBeict()->first()->id,
                        'action_id' => Action::approve()->first()->id,
                ]);

                // input request change action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::approved()->first()->id,
                    'action_type'       => Action::approve()->first()->id,
                    'stage_id'          => Stage::waitingForManagerBeict()->first()->id,
                    'role_id'           => $service->role_id
                ]);
                // dd($rchangeaction);

                // insert data attachment 
                if(!empty($request->file('attachment')))
                {
                    foreach($request->file('attachment') as $files)
                    {
                        // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                        $dateNow    = Carbon::now()->toDateTimeString();
                        $date       = Carbon::parse($dateNow)->format('dmYHis');
                        $name       = $files->getClientOriginalName();
                        $username   = Auth::user()->id;
                        $filename   = $date.$username.$name;

                        // upload data
                        $item = $files->storeAs('attachments', $filename);

                        // input data file
                        $rchangeAttach = RequestChangeAttachment::create([
                                'request_change_id' => $id,
                                'attachment' => $item, 
                                'name' => $filename, 
                                'alias' => $name
                            ]);
                    }
                }

                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }

                // // input request change bps action
                // $rchangeaction =  RequestChangeBpsAction::create([
                //     'request_change_bps_id' => $id,
                //     'user_id'               => Auth::user()->id,
                //     'status_id'             => Status::approved()->first()->id,
                //     'action_type'           => Action::approve()->first()->id,
                //     'stage_id'              => Stage::waitingForServiceDesk()->first()->id
                // ]);


                // redirect after scuccess
                return redirect()
                    ->route('approval.show','crf')
                    ->with('success','Permintaan / perubahan berhasil disetujui.');
            }
            elseif($request->input('aksi') !== null and $request->input('aksi') == '2')
            {
                // dd($request->input('aksi'));
                // get data request change
                // get data service
                $rc = RequestChange::find($id);
                $service = Service::where('id', $rc->id)->first();
                

                // update stage bps
                $rchange = RequestChange::
                where('id', $id)
                    ->update([
                        'stage_id' => Stage::waitingForManagerBeict()->first()->id,
                        'action_id' => Action::reject()->first()->id,
                ]);
                

                // input request change action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::rejected()->first()->id,
                    'action_type'       => Action::reject()->first()->id,
                    'stage_id'          => Stage::requestRejected()->first()->id,
                    'role_id'           => $service->role_id
                ]);

                // insert data attachment 
                if(!empty($request->file('attachment')))
                {
                    foreach($request->file('attachment') as $files)
                    {
                        // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                        $dateNow    = Carbon::now()->toDateTimeString();
                        $date       = Carbon::parse($dateNow)->format('dmYHis');
                        $name       = $files->getClientOriginalName();
                        $username   = Auth::user()->id;
                        $filename   = $date.$username.$name;

                        // upload data
                        $item = $files->storeAs('attachments', $filename);

                        // input data file
                        $rchangeAttach = RequestChangeAttachment::create([
                                'request_change_id' => $id,
                                'attachment' => $item, 
                                'name' => $filename, 
                                'alias' => $name
                            ]);
                    }
                }

                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }

                // // input request change bps action
                // $rchangeaction =  RequestChangeBpsAction::create([
                //     'request_change_bps_id' => $id,
                //     'user_id'               => Auth::user()->id,
                //     'status_id'             => Status::approved()->first()->id,
                //     'action_type'           => Action::approve()->first()->id,
                //     'stage_id'              => Stage::waitingForServiceDesk()->first()->id
                // ]);
                
                // redirect setelah success
                return redirect()
                    ->route('approval.show','crf')
                    ->with('success','Permintaan / perubahan berhasil ditolak.');
            }

        } catch(Exception $e) {
            // handle error try catch
            return redirect()
                ->route('crf.index')
                ->with('success','Error '.$e.', persetujuan gagal dilakukan.');
        }
    }

   
/*
    End of service owner
*/

/*

    Begin of service desk function
*/

    public function servicedeskFormTicket($id)
    {
        // get data request change by id
        $requestchange = RequestChange::where('id',$id)->first();
        // redirect to request_changes/servicedek.ticket.blade.php
        return view('request_changes.servicedesk.ticket', compact('requestchange'));
    }

    public function servicedeskInputTicket(Request $request, $id)
    {
        try {
            $rc = RequestChangeBps::find($id);

            $rcbps = RequestChange::where('id', $id)
            ->update([
                'stage_id' => Stage::waitingForBps()->first()->id,
                'ticket' => $request->input('ticket'),
                'action_id' => Action::ticketInput()->first()->id
            ]);

            // input request change action
            $rchangeaction =  RequestChangeAction::create([
                'request_change_id' => $id,
                'user_id'           => Auth::user()->id,
                'status_id'         => Status::approved()->first()->id,
                'action_type'       => Action::ticketInput()->first()->id,
                'stage_id'          => Stage::waitingForBps()->first()->id,
                'role_id'           => Role::roleServicedesk()->first()->id
            ]);

            // // input request change bps action
            // $rchangeaction =  RequestChangeBpsAction::create([
            //     'request_change_bps_id' => $id,
            //     'user_id'               => Auth::user()->id,
            //     'status_id'             => Status::approved()->first()->id,
            //     'action_type'           => Action::ticketInput()->first()->id,
            //     'stage_id'              => Stage::waitingForBps()->first()->id
            // ]);
                        
            // redirec to
            return redirect()
                ->route('crf.index')
                ->with('success','Tiket berhasil diinput.');
        } catch(Exception $e) {
            // error handle
            return redirect()
                ->route('request_changes.servicedesk.ticket', $id)
                ->with('success','Error '.$e.', persetujuan gagal dilakukan.');
        }
         
    }

/*
    end of service desk function
*/

/*
    begin of manager beict function process
*/
    public function beictFormApproval($id)
    {
        // get data service
        $services = Service::all();
        // get data request change
        $requestchange = RequestChange::where('id',$id)->first();
        // redirect to view request_changes/managerbeict/form-approval.blade
        return view('request_changes.managerbeict.form-approval', compact('requestchange','services'));
    }

    public function beictFormReject($id)
    {
        // get data service
        $services = Service::all();
        // get data request change
        $requestchange = RequestChange::where('id',$id)->first();
        // redirect to view request_changes/managerbeict/form-approval.blade
        return view('request_changes.managerbeict.form-reject', compact('requestchange','services'));
    }

    public function beictCrfApproval(Request $request, $id)
    {
        try {
            if($request->input('aksi') == '1')
            {    
                $rcbps = RequestChange::where('id', $id)
                ->update([
                    'stage_id' => Stage::waitingForServiceDesk()->first()->id,
                    'action_id' => Action::approve()->first()->id
                ]);

                // input request change bps action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::approved()->first()->id,
                    'action_type'       => Action::approve()->first()->id,
                    'stage_id'          => Stage::waitingForServiceDesk()->first()->id,
                    'role_id'           => Role::roleManagerBeict()->first()->id
                ]);

                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }
    
                return redirect()
                    ->route('approval.show', 'crf')
                    ->with('success','Permintaan / perubahan layanan berhasil disetujui.');
            }
            elseif($request->input('aksi') == '2')
            {
                $rc = RequestChange::find($id);
    
                $rcbps = RequestChange::where('id', $id)
                    ->update([
                        'stage_id' => Stage::requestRejected()->first()->id,
                        'action_id' => Action::reject()->first()->id
                    ]);
    
                // input request change bps action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::rejected()->first()->id,
                    'action_type'       => Action::reject()->first()->id,
                    'stage_id'          => Stage::requestRejected()->first()->id,
                    'role_id'           => Role::roleManagerBeict()->first()->id
                ]);

                // insert data attachment 
                if(!empty($request->file('attachment')))
                {
                    foreach($request->file('attachment') as $files)
                    {
                        // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                        $dateNow    = Carbon::now()->toDateTimeString();
                        $date       = Carbon::parse($dateNow)->format('dmYHis');
                        $name       = $files->getClientOriginalName();
                        $username   = Auth::user()->id;
                        $filename   = $date.$username.$name;

                        // upload data
                        $item = $files->storeAs('attachments', $filename);

                        // input data file
                        $requestchange = RequestChangeRejectAttachment::create([
                                'request_change_id' => $id,
                                'attachment'    => $item, 
                                'name'          => $filename, 
                                'alias'         => $name
                            ]);
                    }
                }
               
                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }
    
                return redirect()
                    ->route('approval.show', 'crf')
                    ->with('success','Permintaan / perubahan layanan berhasil ditolak.');
            }
        } catch(Exception $e) {
            return redirect()
                ->route('approval.show', 'crf')
                ->with('success','Error '.$e.', approval gagal dilakukan.');
        }
        
    }


    public function beictCrfReject(Request $request, $id)
    {
        try {
            if($request->input('aksi') == '1')
            {    
                $rcbps = RequestChange::where('id', $id)
                ->update([
                    'stage_id' => Stage::waitingForSoApproval()->first()->id,
                    'action_id' => Action::approve()->first()->id
                ]);

                // input request change bps action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::approved()->first()->id,
                    'action_type'       => Action::approve()->first()->id,
                    'stage_id'          => Stage::waitingForSoApproval()->first()->id,
                    'role_id'           => Role::roleManagerBeict()->first()->id
                ]);

                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }
    
                return redirect()
                    ->route('approval.show', 'crf')
                    ->with('success','Permintaan / perubahan layanan berhasil disetujui.');
            }
            elseif($request->input('aksi') == '2')
            {
                $rc = RequestChange::find($id);
    
                $rcbps = RequestChange::where('id', $id)
                    ->update([
                        'stage_id' => Stage::requestRejected()->first()->id,
                        'action_id' => Action::reject()->first()->id
                    ]);
    
                // input request change bps action
                $rchangeaction =  RequestChangeAction::create([
                    'request_change_id' => $id,
                    'user_id'           => Auth::user()->id,
                    'status_id'         => Status::rejected()->first()->id,
                    'action_type'       => Action::reject()->first()->id,
                    'stage_id'          => Stage::requestRejected()->first()->id,
                    'role_id'           => Role::roleManagerBeict()->first()->id
                ]);

                // insert data attachment 
                if(!empty($request->file('attachment')))
                {
                    foreach($request->file('attachment') as $files)
                    {
                        // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                        $dateNow    = Carbon::now()->toDateTimeString();
                        $date       = Carbon::parse($dateNow)->format('dmYHis');
                        $name       = $files->getClientOriginalName();
                        $username   = Auth::user()->id;
                        $filename   = $date.$username.$name;

                        // upload data
                        $item = $files->storeAs('attachments', $filename);

                        // input data file
                        $requestchange = RequestChangeRejectAttachment::create([
                                'request_change_id' => $id,
                                'attachment'    => $item, 
                                'name'          => $filename, 
                                'alias'         => $name
                            ]);
                    }
                }
               
                // check note
                if(!empty($request->input('note')))
                {
                    // save note
                    $rchangeaction->RequestChangeActionNotes()->save(new RequestChangeActionNote(['note' => $request->input('note')]));
                }
    
                return redirect()
                    ->route('approval.show', 'crf')
                    ->with('success','Permintaan / perubahan layanan berhasil ditolak.');
            }
        } catch(Exception $e) {
            return redirect()
                ->route('approval.show', 'crf')
                ->with('success','Error '.$e.', approval gagal dilakukan.');
        }
        
    }
/*
    end of manager beict
*/

/* 
    action create pdf
*/
    public function createPdf($id)
    {
        $requestchange = RequestChange::where('id',$id)->first();
        $pdf = PDF::loadview('request_changes.pdf.crf-bps',['requestchange'=>$requestchange]);
        // return $pdf->download('crf');
        return $pdf->stream();
    }

/* 
    end action pdf
*/
}
