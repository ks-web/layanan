<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use UrlSigner;
use App\ITRequest;
use App\Service;
use App\Stage;
use App\Status;
use App\Category;
use App\RequestApproval;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreITRequestRequest;
use App\Http\Requests\ApprovrsaveITRequestRequest;
use Illuminate\Support\Facades\Storage;
use App\Notifications\RequestCreated;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\RequestReasonfile;
use App\RequestAttachment;
use App\RequestSo;
use App\RequestSoBps;
use App\ServiceCoordinator;
use App\User;
use App\Action;
use App\RequestAction;
use App\RequestActionNote;
use Carbon\Carbon;
use App\Incident;

class HomeController extends Controller
{
    public function index()
    {
        /*
            Data dasboard boss
            Data dasboard manager beict
            tes
        */
        // dd(Auth::user()->last_activity);
        // if(Auth::user()->last_activity == NULL)
        // {
            if(Auth::user()->hasAllRoles(['employee']))
            {
                $urlpath = "dasboard";
                if(Auth::user()->hasAllRoles(['boss']))
                {
                    if(Auth::user()->hasRole('manager beict'))
                    {
                        $a = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '1')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '1');
                            })
                            ->get()
                            ->count();

                        $b = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '12')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '12');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_waiting_boss_approved = $a+$b;

                        $jumlah_request_rejected = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '11')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '11');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_boss_approved = ITRequest::all()
                            ->count();

                        return view('dasboard.index', compact(
                            'jumlah_request_waiting_boss_approved',
                            'jumlah_request_rejected',
                            'jumlah_request_boss_approved',
                            'urlpath'
                            )
                        );
                    }
                    else
                    {
                        $jumlah_request_waiting_boss_approved = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '1')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '1');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_rejected = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '11')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '11');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_boss_approved = ITRequest::where('boss', Auth::user()->id)
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                            })
                            ->get()
                            ->count();

                        return view('dasboard.index', compact(
                            'jumlah_request_waiting_boss_approved',
                            'jumlah_request_rejected',
                            'jumlah_request_boss_approved',
                            'urlpath'
                            )
                        );
                    }
                }
                /*
                    Data dasboard specialist spsd
                */
                elseif(Auth::user()->hasAllRoles(['operation sd']))
                {
                    $jumlah_request_waiting_spsd_approved = ITRequest::where('stage_id','2')
                        ->get()
                        ->count();

                    $jumlah_request_rejected = ITRequest::where('stage_id', '11')
                        ->get()
                        ->count();

                    $jumlah_request_spsd_approved = ITRequest::all()
                        ->count();

                    $jumlah_request_eskalasi = ITRequest::where('category_id','2')
                        ->where('stage_id', '10')
                        ->get()
                        ->count();

                    return view('dasboard.index', compact(
                        'jumlah_request_waiting_spsd_approved',
                        'jumlah_request_rejected',
                        'jumlah_request_spsd_approved',
                        'jumlah_request_eskalasi',
                        'urlpath'
                        )
                    );
                }
                elseif(Auth::user()->hasAllRoles(['operation ict']))
                {
                    $jumlah_request_waiting_spict_approved  = ITRequest::where('stage_id', '7')
                        ->get()
                        ->count();

                    $jumlah_request_rejected = ITRequest::where('stage_id', '11')
                        ->get()
                        ->count();

                    $jumlah_request_all = ITRequest::all()
                        ->count();
                    return view('dasboard.index', compact(
                        'jumlah_request_waiting_spict_approved',
                        'jumlah_request_rejected',
                        'jumlah_request_all',
                        'urlpath'
                        )
                    );
                }
                elseif(Auth::user()->hasAllRoles(['chief']))
                {
                    $jumlah_request_waiting_chief_approved = ITRequest::where('stage_id', '19')
                        ->get()
                        ->count();

                    $jumlah_request_rejected = ITRequest::where('stage_id', '11')
                        ->get()
                        ->count();

                    $jumlah_request_all = ITRequest::all()
                        ->count();

                    return view('dasboard.index', compact(
                        'jumlah_request_waiting_chief_approved',
                        'jumlah_request_rejected',
                        'jumlah_request_all',
                        'urlpath'
                        )
                    );
                }
                // so start  dasboard
                // elseif(Auth::user()->hasAllRoles(['so mes flat']))
                // {
                //     // $data       = Auth::user()->roles()->get()->toArray();
                //     // $serv       = array_column(Service::whereIn('role_id',array_column($data,'id'))->get()->toArray(),'id');
                //     // $jumlah_request_waiting_so_approved = ITRequest::whereIn('stage_id', ['10','13','15','14'])->get()->count();
                //     // $jumlah_sedang_berjalan             = ITRequest::whereNotIn('stage_id', ['3','11'])->get()->count();
                //     // $jumlah_request_rejected            = ITRequest::where('stage_id', '11')->get()->count();
                //     // $jumlah_request_closed              = ITRequest::where('stage_id', '9')->get()->count();
                //     // return view('dasboard.index',
                //     //                 compact(
                //     //                     'jumlah_request_waiting_so_approved',
                //     //                     'jumlah_sedang_berjalan',
                //     //                     'jumlah_request_rejected',
                //     //                     'jumlah_request_closed',
                //     //                     'urlpath'
                //     //                     )
                //     //             );

                //     /*
                //         Data dasboard service owner 
                //     */
                //     $data = Auth::user()
                //     ->roles()
                //     ->get()
                //     ->toArray();

                //     $serv = array_column(Service::whereIn('role_id',array_column($data,'id'))
                //     ->get()
                //     ->toArray(),'id');

                //     $request_waiting_so_approved = RequestSo::withCount(['request' => function ($query) {
                //         $query->whereIn('stage_id', ['10','13','15','14']);
                //     }])->whereIn('service_id',$serv)->get()->toArray();

                    
                //     $arraydata_waiting_so_approved  = array_column($request_waiting_so_approved, 'request_count');
                //     $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);
                    
                //     if(isset($counts__waiting_so_approved[1]))
                //     {
                //         $jumlah_request_waiting_so_approved = $counts__waiting_so_approved[1];
                //     }
                //     else
                //     {
                //         $jumlah_request_waiting_so_approved = 0;
                //     }

                //     $jumlah_sedang_berjalan = RequestSo::with('request')
                //         ->whereIn('service_id',$serv)
                //         ->get()
                //         ->count();

                //     $request_closed = RequestSo::withCount(
                //         ['request' => function ($query) {
                //                 $query->whereIn('stage_id', ['9']);
                //             }
                //         ]
                //     )->whereIn('service_id',$serv)
                //         ->get()
                //         ->toArray();

                //     $arraydata_request_closed = array_column($request_closed, 'request_count');
                //     $counts_request_closed = array_count_values($arraydata_request_closed);

                //     if(isset($counts_request_closed[1]))
                //     {
                //         $jumlah_request_closed = $counts_request_closed[1];
                //     }
                //     else
                //     {
                //         $jumlah_request_closed = 0;
                //     }

                //     $jumlah_sedang_berjalan = ITRequest::where('service_id', '10')
                //     ->get()
                //     ->count();

                //     $jumlah_request_rejected = ITRequest::where('stage_id', '11')
                //     ->get()
                //     ->count();

                //     $jumlah_request_closed = ITRequest::where('stage_id', '9')
                //     ->get()
                //     ->count();

                //     return view('dasboard.index', compact(
                //         'jumlah_request_waiting_so_approved',
                //         'jumlah_sedang_berjalan',
                //         'jumlah_request_rejected',
                //         'jumlah_request_closed',
                //         'urlpath'
                //         )
                //     );
                    
                // }
                // elseif(Auth::user()->hasAllRoles(['so perangkat komputer']))
                // {
                //     $data = Auth::user()
                //     ->roles()
                //     ->get()
                //     ->toArray();

                //     $serv = array_column(Service::whereIn('role_id',array_column($data,'id'))
                //     ->get()
                //     ->toArray(),'id');

                //     $request_waiting_so_approved = RequestSo::withCount(['request' => function ($query) {
                //         $query->whereIn('stage_id', ['10','13','15','14']);
                //     }])->whereIn('service_id',$serv)->get()->toArray();

                    
                //     $arraydata_waiting_so_approved  = array_column($request_waiting_so_approved, 'request_count');
                //     $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);
                    
                //     if(isset($counts__waiting_so_approved[1]))
                //     {
                //         $jumlah_request_waiting_so_approved = $counts__waiting_so_approved[1];
                //     }
                //     else
                //     {
                //         $jumlah_request_waiting_so_approved = 0;
                //     }

                //     $jumlah_sedang_berjalan = RequestSo::with('request')
                //         ->whereIn('service_id',$serv)
                //         ->get()
                //         ->count();

                //     $request_closed = RequestSo::withCount(
                //         ['request' => function ($query) {
                //                 $query->whereIn('stage_id', ['9']);
                //             }
                //         ]
                //     )->whereIn('service_id',$serv)
                //         ->get()
                //         ->toArray();

                //     $arraydata_request_closed = array_column($request_closed, 'request_count');
                //     $counts_request_closed = array_count_values($arraydata_request_closed);

                //     if(isset($counts_request_closed[1]))
                //     {
                //         $jumlah_request_closed = $counts_request_closed[1];
                //     }
                //     else
                //     {
                //         $jumlah_request_closed = 0;
                //     }

                //     $jumlah_sedang_berjalan = ITRequest::where('service_id', '6')
                //     ->get()
                //     ->count();

                //     $jumlah_request_rejected = ITRequest::where('stage_id', '11')
                //     ->get()
                //     ->count();

                //     $jumlah_request_closed = ITRequest::where('stage_id', '9')
                //     ->get()
                //     ->count();

                   

                //     return view('dasboard.index', compact(
                //         'jumlah_request_waiting_so_approved',
                //         'jumlah_sedang_berjalan',
                //         'jumlah_request_rejected',
                //         'jumlah_request_closed',
                //         'urlpath'
                //         )
                //     );
                // }
                elseif(Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so jaringan'])  or Auth::user()->hasAllRoles(['so messaging']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so perangkat komputer']))
                {
                    // so aplikasi office start

                    if(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
                    {
                        $data = array_column(
                            Auth::user()
                            ->roles()
                            ->get()
                            ->toArray(), 'id'
                        );
    
                       
                        $serv = array_column(
                            ServiceCoordinator::whereIn('role_id', $data)
                            ->get()
                            ->toArray(),'service_id'
                        );
    
                        $all = RequestSo::with('request')
                            ->whereIn('service_id',$serv)
                            ->groupBy('request_id')
                            ->get()
                            ->count();
    
                        $waiting_co_approved = RequestSo::withCount(['request' => function($query){
                            $query->whereIn('stage_id', ['13']); 
                        }])
                        ->whereIn('service_id',$serv)
                        ->groupBy('request_id')->get()->toArray();
    
                        $arraydata = array_column($waiting_co_approved, 'request_count');
    
                        $counts = array_count_values($arraydata);
    
                        if(isset($counts[1]))
                        {
                            $waiting_co_approved_count = $counts[1];
                        }
                        else
                        {
                            $waiting_co_approved_count = 0;
                        }
                        //dd($arraydata);
                        return view('dasboard.index-co', compact(
                            'waiting_co_approved_count',
                            'all',
                            'urlpath'
                            )
                        );
                    }
                    else 
                    {                        
                        $data = Auth::user()
                            ->roles()
                            ->get()
                            ->toArray();

                        $serv = array_column(Service::whereIn('role_id',array_column($data,'id'))
                        ->get()
                        ->toArray(),'id');

                        $request_waiting_so_approved = RequestSo::withCount(['request' => function ($query) {
                            $query->whereIn('stage_id', ['10','13','15','14']);
                        }])->whereIn('service_id',$serv)->get()->toArray();

                        
                        $arraydata_waiting_so_approved  = array_column($request_waiting_so_approved, 'request_count');
                        $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);
                        
                        if(isset($counts__waiting_so_approved[1]))
                        {
                            $jumlah_request_waiting_so_approved = $counts__waiting_so_approved[1];
                        }
                        else
                        {
                            $jumlah_request_waiting_so_approved = 0;
                        }

                        $jumlah_sedang_berjalan = RequestSo::with('request')
                            ->whereIn('service_id',$serv)
                            ->get()
                            ->count();

                        $request_closed = RequestSo::withCount(
                            ['request' => function ($query) {
                                    $query->whereIn('stage_id', ['9']);
                                }
                            ]
                        )->whereIn('service_id',$serv)
                            ->get()
                            ->toArray();

                        $arraydata_request_closed = array_column($request_closed, 'request_count');
                        $counts_request_closed = array_count_values($arraydata_request_closed);

                        if(isset($counts_request_closed[1]))
                        {
                            $jumlah_request_closed = $counts_request_closed[1];
                        }
                        else
                        {
                            $jumlah_request_closed = 0;
                        }

                        $jumlah_sedang_berjalan = ITRequest::whereIn('service_id',$serv)
                            ->get()
                            ->count();

                        $jumlah_request_rejected = ITRequest::where('stage_id', '11')
                            ->get()
                            ->count();  

                        $jumlah_request_closed = ITRequest::where('stage_id', '9')
                            ->get()
                            ->count();

                        return view('dasboard.index', compact(
                            'jumlah_request_waiting_so_approved',
                            'jumlah_sedang_berjalan',
                            'jumlah_request_rejected',
                            'jumlah_request_closed',
                            'urlpath'
                            )
                        );
                    }
                }
                // end so dashboard
                // start coordinator dashboard
                // elseif(Auth::user()->hasAllRoles(['coordinator aplikasi']))
                // {
                //     $data = array_column(
                //         Auth::user()
                //         ->roles()
                //         ->get()
                //         ->toArray(), 'id'
                //     );

                //     $serv = array_column(
                //         ServiceCoordinator::whereIn('role_id', $data)
                //         ->get()
                //         ->toArray(),'service_id'
                //     );

                //     $all = RequestSo::with('request')
                //         ->whereIn('service_id',$serv)
                //         ->groupBy('request_id')
                //         ->get()
                //         ->count();

                //     $waiting_co_approved = RequestSo::withCount(['request' => function($query){
                //         $query->whereIn('stage_id', ['13']); 
                //     }])
                //         ->whereIn('service_id',$serv)
                //         ->groupBy('request_id')
                //         ->get()
                //         ->toArray();

                //     $arraydata = array_column($waiting_co_approved, 'request_count');

                //     $counts= array_count_values($arraydata);

                //     if(isset($counts[1]))
                //     {
                //         $waiting_co_approved_count = $counts[1];
                //     }
                //     else
                //     {
                //         $waiting_co_approved_count = 0;
                //     }
                //     //dd($arraydata);
                //     return view('dasboard.index', compact(
                //         'waiting_co_approved_count',
                //         'all',
                //         'urlpath'
                //         )
                //     );
                // }
                // elseif(Auth::user()->hasAllRoles(['coordinator infra']))
                // {
                //     $data = array_column(
                //         Auth::user()
                //         ->roles()
                //         ->get()
                //         ->toArray(), 'id'
                //     );

                   
                //     $serv = array_column(
                //         ServiceCoordinator::whereIn('role_id', $data)
                //         ->get()
                //         ->toArray(),'service_id'
                //     );

                //     $all = RequestSo::with('request')
                //         ->whereIn('service_id',$serv)
                //         ->groupBy('request_id')
                //         ->get()
                //         ->count();

                //     $waiting_co_approved = RequestSo::withCount(['request' => function($query){
                //         $query->whereIn('stage_id', ['13']); 
                //     }])
                //     ->whereIn('service_id',$serv)
                //     ->groupBy('request_id')->get()->toArray();

                //     $arraydata = array_column($waiting_co_approved, 'request_count');

                //     $counts = array_count_values($arraydata);

                //     if(isset($counts[1]))
                //     {
                //         $waiting_co_approved_count = $counts[1];
                //     }
                //     else
                //     {
                //         $waiting_co_approved_count = 0;
                //     }
                //     //dd($arraydata);
                //     return view('dasboard.index', compact(
                //         'waiting_co_approved_count',
                //         'all',
                //         'urlpath'
                //         )
                //     );
                // }
                else
                {
                    $jumlah_request_sedang_berjalan = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id', ['9','11','31'])
                        ->get()
                        ->count();

                    $jumlah_request_rejected = ITRequest::ofLoggedUser()
                        ->where('stage_id', '11')
                        ->get()
                        ->count();

                    $jumlah_request_closed = ITRequest::ofLoggedUser()
                        ->where('stage_id', '9')
                        ->get()
                        ->count();

                    $jumlah_incident_waiting_user_conf = Incident::ofLoggedUser()
                        ->where('stage_id','5')
                        ->get()
                        ->count();

                    $jumlah_sedang_berjalan = Incident::ofLoggedUser()
                        ->whereIn('stage_id',['3','4','6','29','5'])
                        ->get()
                        ->count();

                    $jumlah_sudah_diclose  = Incident::ofLoggedUser()
                        ->whereIn('stage_id',['9'])
                        ->get()
                        ->count();

                    return view('dasboard.indexemploye',
                                    compact(
                                        'jumlah_request_sedang_berjalan',
                                        'jumlah_request_rejected',
                                        'jumlah_request_closed',
                                        'jumlah_incident_waiting_user_conf',
                                        'jumlah_sedang_berjalan',
                                        'jumlah_sudah_diclose',
                                        'urlpath'
                                        )
                                );
                }
            }
            elseif(Auth::user()->hasAllRoles(['service desk']))
            {
                return redirect()
                    ->route('requests.index');
            }
            elseif(Auth::user()->hashRoles(['secretary']))
            {
                return redirect()
                    ->route('requests.index');
            }
        // }
        // else
        // {
        //     return redirect()->to(Auth::user()->last_activity);
        // }
    }

    public function dashboard()
    {
        /*
            Data dasboard boss
            Data dasboard manager beict
        */
            if(Auth::user()->hasAllRoles(['employee']))
            {
                $urlpath = "dashboard";
                if(Auth::user()->hasAllRoles(['boss']))
                {
                    if(Auth::user()->hasRole('manager beict'))
                    {
                        $a = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '1')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '1');
                            })
                            ->get()
                            ->count();

                        $b = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '12')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '12');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_waiting_boss_approved = $a+$b;

                        $jumlah_request_rejected = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '11')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '11');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_boss_approved = ITRequest::where('boss', Auth::user()->id)
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                            })
                            ->get()
                            ->count();

                        return view('dasboard.index', compact(
                            'jumlah_request_waiting_boss_approved',
                            'jumlah_request_rejected',
                            'jumlah_request_boss_approved',
                            'urlpath'
                            )
                        );
                    }
                    else
                    {
                        $jumlah_request_waiting_boss_approved = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '1')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '1');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_rejected = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', '11')
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', '11');
                            })
                            ->get()
                            ->count();

                        $jumlah_request_boss_approved = ITRequest::where('boss', Auth::user()->id)
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                            })
                            ->get()
                            ->count();

                        return view('dasboard.index', compact(
                            'jumlah_request_waiting_boss_approved',
                            'jumlah_request_rejected',
                            'jumlah_request_boss_approved',
                            'urlpath'
                            )
                        );
                    }
                }
                /*
                    Data dasboard specialist spsd
                */
                elseif(Auth::user()->hasAllRoles(['operation sd']))
                {
                    $jumlah_request_waiting_spsd_approved = ITRequest::where('stage_id', '2')
                        ->get()
                        ->count();

                    $jumlah_request_rejected  = ITRequest::where('stage_id', '11')
                        ->get()
                        ->count();

                    $jumlah_request_spsd_approved = ITRequest::all()
                        ->count();

                    $jumlah_request_eskalasi = ITRequest::where('category_id','2')
                        ->where('stage_id', '10')
                        ->get()
                        ->count();

                return view('dasboard.index', compact(
                        'jumlah_request_waiting_spsd_approved',
                        'jumlah_request_rejected',
                        'jumlah_request_spsd_approved',
                        'jumlah_request_eskalasi',
                        'urlpath'
                        )
                    );
                }
                elseif(Auth::user()->hasAllRoles(['operation ict']))
                {
                    $jumlah_request_waiting_spict_approved = ITRequest::where('stage_id', '7')
                        ->get()
                        ->count();

                    $jumlah_request_rejected = ITRequest::where('stage_id', '11')
                        ->get()
                        ->count();

                    $jumlah_request_all = ITRequest::all()
                        ->count();

                    return view('dasboard.index', compact(
                        'jumlah_request_waiting_spict_approved',
                        'jumlah_request_rejected',
                        'jumlah_request_all',
                        'urlpath'
                        )
                    );
                }
                elseif(Auth::user()->hasAllRoles(['chief']))
                {
                    $jumlah_request_waiting_chief_approved  = ITRequest::where('stage_id', '19')->get()->count();
                    $jumlah_request_rejected                = ITRequest::where('stage_id', '11')->get()->count();
                    $jumlah_request_all                     = ITRequest::all()->count();
                    return view('dasboard.index',
                                    compact(
                                        'jumlah_request_waiting_chief_approved',
                                        'jumlah_request_rejected',
                                        'jumlah_request_all',
                                        'urlpath'
                                        )
                                );
                }
                elseif(Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so perangkat komputer']) or Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so jaringan']) or Auth::user()->hasAllRoles(['so messaging']))
                {
                    /*
                        Data dasboard service owner 
                    */
                    $data                               = Auth::user()->roles()->get()->toArray();
                    $serv                               = array_column(Service::whereIn('role_id',array_column($data,'id'))->get()->toArray(),'id');
                    $request_waiting_so_approved        = RequestSo::withCount(['request' => function ($query) {
                                                                $query->whereIn('stage_id', ['10','13','15','14']);
                                                            }])->whereIn('service_id',$serv)->get()->toArray();

                    $arraydata_waiting_so_approved      = array_column($request_waiting_so_approved, 'request_count');
                    $counts__waiting_so_approved        = array_count_values($arraydata_waiting_so_approved);
                    if(isset($counts__waiting_so_approved[1]))
                    {
                        $jumlah_request_waiting_so_approved = $counts__waiting_so_approved[1];
                    }
                    else
                    {
                        $jumlah_request_waiting_so_approved = 0;
                    }

                    $jumlah_sedang_berjalan                 = RequestSo::with('request')->whereIn('service_id',$serv)->get()->count();

                    $request_closed                         = RequestSo::withCount(['request' => function ($query) {
                                                                    $query->whereIn('stage_id', ['9']);
                                                                }])->whereIn('service_id',$serv)->get()->toArray();

                    $arraydata_request_closed               = array_column($request_closed, 'request_count');
                    $counts_request_closed                  = array_count_values($arraydata_request_closed);

                    if(isset($counts_request_closed[1]))
                    {
                        $jumlah_request_closed = $counts_request_closed[1];
                    }
                    else
                    {
                        $jumlah_request_closed = 0;
                    }


                    $jumlah_request_waiting_so_approved = ITRequest::whereIn('stage_id', ['10','13','15','14'])->get()->count();
                    $jumlah_sedang_berjalan             = ITRequest::where('service_id', '10')->get()->count();
                    $jumlah_request_rejected            = ITRequest::where('stage_id', '11')->get()->count();
                    $jumlah_request_closed              = ITRequest::where('stage_id', '9')->get()->count();
                    return view('dasboard.index',
                                    compact(
                                        'jumlah_request_waiting_so_approved',
                                        'jumlah_sedang_berjalan',
                                        'jumlah_request_rejected',
                                        'jumlah_request_closed',
                                        'urlpath'
                                        )
                                );
                    
                }
                elseif(Auth::user()->hasAllRoles(['so mes flat']))
                {
                    // $data       = Auth::user()->roles()->get()->toArray();
                    // $serv       = array_column(Service::whereIn('role_id',array_column($data,'id'))->get()->toArray(),'id');
                    // $jumlah_request_waiting_so_approved = ITRequest::whereIn('stage_id', ['10','13','15','14'])->get()->count();
                    // $jumlah_sedang_berjalan             = ITRequest::whereNotIn('stage_id', ['3','11'])->get()->count();
                    // $jumlah_request_rejected            = ITRequest::where('stage_id', '11')->get()->count();
                    // $jumlah_request_closed              = ITRequest::where('stage_id', '9')->get()->count();
                    // return view('dasboard.index',
                    //                 compact(
                    //                     'jumlah_request_waiting_so_approved',
                    //                     'jumlah_sedang_berjalan',
                    //                     'jumlah_request_rejected',
                    //                     'jumlah_request_closed',
                    //                     'urlpath'
                    //                     )
                    //             );

                     /*
                        Data dasboard service owner 
                    */
                    $data                               = Auth::user()->roles()->get()->toArray();
                    $serv                               = array_column(Service::whereIn('role_id',array_column($data,'id'))->get()->toArray(),'id');
                    $request_waiting_so_approved        = RequestSo::withCount(['request' => function ($query) {
                                                                $query->whereIn('stage_id', ['10','13','15','14']);
                                                            }])->whereIn('service_id',$serv)->get()->toArray();

                    $arraydata_waiting_so_approved      = array_column($request_waiting_so_approved, 'request_count');
                    $counts__waiting_so_approved        = array_count_values($arraydata_waiting_so_approved);
                    
                    if(isset($counts__waiting_so_approved[1]))
                    {
                        $jumlah_request_waiting_so_approved = $counts__waiting_so_approved[1];
                    }
                    else
                    {
                        $jumlah_request_waiting_so_approved = 0;
                    }

                    $jumlah_sedang_berjalan                 = RequestSo::with('request')->whereIn('service_id',$serv)->get()->count();

                    $request_closed                         = RequestSo::withCount(['request' => function ($query) {
                                                                    $query->whereIn('stage_id', ['9']);
                                                                }])->whereIn('service_id',$serv)->get()->toArray();

                    $arraydata_request_closed               = array_column($request_closed, 'request_count');
                    $counts_request_closed                  = array_count_values($arraydata_request_closed);

                    if(isset($counts_request_closed[1]))
                    {
                        $jumlah_request_closed = $counts_request_closed[1];
                    }
                    else
                    {
                        $jumlah_request_closed = 0;
                    }


                    $jumlah_request_waiting_so_approved = ITRequest::whereIn('stage_id', ['10','13','15','14'])->get()->count();
                    $jumlah_sedang_berjalan             = ITRequest::where('service_id','3')->get()->count();
                    $jumlah_request_rejected            = ITRequest::where('stage_id', '11')->get()->count();
                    $jumlah_request_closed              = ITRequest::where('stage_id', '9')->get()->count();
                    return view('dasboard.index',
                                    compact(
                                        'jumlah_request_waiting_so_approved',
                                        'jumlah_sedang_berjalan',
                                        'jumlah_request_rejected',
                                        'jumlah_request_closed',
                                        'urlpath'
                                        )
                                );
                }
                elseif(Auth::user()->hasAllRoles(['so perangkat komputer']))
                {
                    $data                               = Auth::user()->roles()->get()->toArray();
                    $serv                               = array_column(Service::whereIn('role_id',array_column($data,'id'))->get()->toArray(),'id');
                    $jumlah_request_waiting_so_approved = ITRequest::whereIn('stage_id', ['10','13','15','14'])->get()->count();
                    $jumlah_sedang_berjalan             = ITRequest::whereNotIn('stage_id', ['10','11'])->get()->count();
                    $jumlah_request_rejected            = ITRequest::where('stage_id', '11')->get()->count();
                    $jumlah_request_closed              = ITRequest::where('stage_id', '9')->get()->count();
                    return view('dasboard.index',
                                    compact(
                                        'jumlah_request_waiting_so_approved',
                                        'jumlah_sedang_berjalan',
                                        'jumlah_request_rejected',
                                        'jumlah_request_closed',
                                        'urlpath'
                                        )
                                );
                }
                elseif(Auth::user()->hasAllRoles(['coordinator aplikasi']))
                {
                    $data                   = array_column(Auth::user()->roles()->get()->toArray(), 'id');
                    $serv                   = array_column(ServiceCoordinator::whereIn('role_id', $data)->get()->toArray(),'service_id');
                    $all                    = RequestSo::with('request')
                                                ->whereIn('service_id',$serv)
                                                ->groupBy('request_id')->get()->count();

                    $waiting_co_approved    = RequestSo::withCount(['request' => function($query){
                                                    $query->whereIn('stage_id', ['13']); 
                                                }])
                                                ->whereIn('service_id',$serv)
                                                ->groupBy('request_id')->get()->toArray();
                    $arraydata   = array_column($waiting_co_approved, 'request_count');
                    $counts      = array_count_values($arraydata);
                    if(isset($counts[1]))
                    {
                        $waiting_co_approved_count = $counts[1];
                    }
                    else
                    {
                        $waiting_co_approved_count = 0;
                    }
                    //dd($arraydata);
                    return view('dasboard.index',
                                    compact(
                                        'waiting_co_approved_count',
                                        'all',
                                        'urlpath'
                                        )
                                );
                }
                elseif(Auth::user()->hasAllRoles(['coordinator infra']))
                {
                    $data                   = array_column(Auth::user()->roles()->get()->toArray(), 'id');
                    $serv                   = array_column(ServiceCoordinator::whereIn('role_id', $data)->get()->toArray(),'service_id');
                    $all                    = RequestSo::with('request')
                                                ->whereIn('service_id',$serv)
                                                ->groupBy('request_id')->get()->count();

                    $waiting_co_approved    = RequestSo::withCount(['request' => function($query){
                                                    $query->whereIn('stage_id', ['13']); 
                                                }])
                                                ->whereIn('service_id',$serv)
                                                ->groupBy('request_id')->get()->toArray();
                    $arraydata   = array_column($waiting_co_approved, 'request_count');
                    $counts      = array_count_values($arraydata);
                    if(isset($counts[1]))
                    {
                        $waiting_co_approved_count = $counts[1];
                    }
                    else
                    {
                        $waiting_co_approved_count = 0;
                    }
                    //dd($arraydata);
                    return view('dasboard.index',
                                    compact(
                                        'waiting_co_approved_count',
                                        'all',
                                        'urlpath'
                                        )
                                );
                }
                else
                {
                    $jumlah_request_sedang_berjalan         = ITRequest::ofLoggedUser()->whereNotIn('stage_id', ['9','11','31'])->get()->count();
                    $jumlah_request_rejected                = ITRequest::ofLoggedUser()->where('stage_id', '11')->get()->count();
                    $jumlah_request_closed                  = ITRequest::ofLoggedUser()->where('stage_id', '9')->get()->count();

                    $jumlah_incident_waiting_user_conf      = Incident::ofLoggedUser()->where('stage_id','5')->get()->count();
                    $jumlah_sedang_berjalan                 = Incident::ofLoggedUser()->whereIn('stage_id',['3','4','6','29','5'])->get()->count();
                    $jumlah_sudah_diclose                   = Incident::ofLoggedUser()->whereIn('stage_id',['9'])->get()->count();
                    return view('dasboard.indexemploye',
                                    compact(
                                        'jumlah_request_sedang_berjalan',
                                        'jumlah_request_rejected',
                                        'jumlah_request_closed',
                                        'jumlah_incident_waiting_user_conf',
                                        'jumlah_sedang_berjalan',
                                        'jumlah_sudah_diclose',
                                        'urlpath'
                                        )
                                );
                }
            }
            /*
                Data dasboard service desk
            */
            elseif(Auth::user()->hasAllRoles(['service desk']))
            {

                return redirect()
                ->route('requests.index');
            }
    }

    public function dasboard()
    {
        $jumlah_request_waiting_sd_detail   = ITRequest::where('stage_id', '4')->get()->count();
        $jumlah_request_waiting_tiket       = ITRequest::where('stage_id', '3')->get()->count();
        // $jumlah_request_rejected            = ITRequest::All()->count();
        $jumlah_request_rejected            = ITRequest::whereNotIn('stage_id', ['11','31','9'])
                ->get()
                ->count();
        $urlpath = "dasboard";
        // incident 
        return view('dasboard.index',
                        compact(
                            'jumlah_request_waiting_tiket',
                            'jumlah_request_rejected',
                            'jumlah_request_waiting_sd_detail',                           
                            'urlpath'
                            )
                    );
    }
}
