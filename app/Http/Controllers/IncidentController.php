<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreIncidentRequest;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\IncidentAction;
use App\Action;
use App\Incident;
use App\Stage;
use App\Severity;
use App\Priority;
use App\IncidentAttachment;
use App\IncidentActionNote;
use App\IncidentRejectattachment;
use Carbon\Carbon;

class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stage = null;
        $urlpath = "incident";
        if(Auth::user()->hasAllRoles(['employee']))
        {
            if(Auth::user()->hasAllRoles(['operation sd']))
            {
                $countInprogress            = Incident::whereNotIn('stage_id',['11','9','31'])->get()->count();
                $countAll                   = Incident::all()->count();
                $countClose                 = Incident::where('stage_id','9')->get()->count();
                $countBreach                = Incident::whereIn('stage_id',['32','33','34'])->get()->count();
                $countEscalation    = Incident::where('stage_id','2')->get()->count();

                $paginated  = Incident::whereNotIn('stage_id',['11','9','31'])->get();
                return view('incidents.index', compact(
                    'paginated'
                    ,'stage'
                    ,'urlpath'
                    ,'countInprogress'
                    ,'countAll'
                    ,'countClose'
                    ,'countBreach'
                    ,'countEscalation')
                );
            }
            elseif(Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so perangkat komputer']) or Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so jaringan']) or Auth::user()->hasAllRoles(['so messaging']))
            
            {
                $countInprogress    = Incident::whereNotIn('stage_id',['11','9','31'])->get()->count();
                $countAll           = Incident::all()->count();
                $countClose         = Incident::where('stage_id','9')->get()->count();
                $countBreach        = Incident::whereIn('stage_id',['32','33','34'])->get()->count();

                $paginated  = Incident::whereNotIn('stage_id',['11','9','31'])->get();
                return view('incidents.index', compact(
                    'paginated'
                    ,'stage'
                    ,'urlpath'
                    ,'countInprogress'
                    ,'countAll'
                    ,'countClose'
                    ,'countBreach')
                );
            }
            else
            {
                $countInprogress = Incident::ofLoggedUser()->whereNotIn('stage_id',['11','9','31'])->get()->count();
                $countWaitingConfirmation = Incident::ofLoggedUser()->where('stage_id','5')->get()->count();
                $countAll = Incident::ofLoggedUser()->get()->count();
                $countClose = Incident::ofLoggedUser()->where('stage_id','9')->get()->count();

                $paginated  = Incident::ofLoggedUser()->whereNotIn('stage_id',['11','9','31'])->get();
                return view('incidents.indexemployee', compact('paginated','stage','urlpath','countInprogress','countWaitingConfirmation','countAll','countClose'));
            }
        }
        elseif(Auth::user()->hasAllRoles(['service desk']))
        {
            $countInprogress    = Incident::whereIn('stage_id',['3','4','6','30','32','33','34'])->get()->count();
            $countTicket        = Incident::where('stage_id','3')->get()->count();
            $countDeniedByUser  = Incident::where('stage_id','29')->get()->count();
            $countResolve       = Incident::where('stage_id','5')->get()->count();
            $countBreach        = Incident::whereIn('stage_id',['32','33','34'])->get()->count();
            $countClose         = Incident::where('stage_id','9')->get()->count();
            $countAll           = Incident::all()->count();

            $paginated  = Incident::whereIn('stage_id',['3','4','6','30','32','33','34'])->get();
            return view('incidents.index', compact('paginated','stage','urlpath','countInprogress','countTicket','countDeniedByUser','countResolve','countBreach','countClose','countAll'));
        }
        
    }

    /*
        show data incident perstage
    */

    public function showperstage($stage=null)
    {
        $urlpath = "incident";
        if(Auth::user()->hasAllRoles(['employee']))
        {
            if(Auth::user()->hasAllRoles(['operation sd']))
            {
                $countInprogress    = Incident::whereNotIn('stage_id',['11','9','31'])->get()->count();
                $countAll           = Incident::all()->count();
                $countClose         = Incident::where('stage_id','9')->get()->count();
                $countBreach        = Incident::whereIn('stage_id',['32','33','34'])->get()->count();
                $countEscalation    = Incident::where('stage_id','2')->get()->count();

                if($stage == "all")
                {
                    $paginated  = Incident::all();
                }
                elseif($stage == "breach")
                {
                    $paginated  = Incident::whereIn('stage_id', ['32','33','34'])->get();
                }
                else
                {
                    $paginated  = Incident::where('stage_id', $stage)->get();
                }
                
                return view('incidents.index', compact(
                    'paginated'
                    ,'stage'
                    ,'urlpath'
                    ,'countInprogress'
                    ,'countAll'
                    ,'countClose'
                    ,'countBreach'
                    ,'countEscalation')
                );
            }
            elseif(Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so perangkat komputer']) or Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so jaringan']) or Auth::user()->hasAllRoles(['so messaging']))
            {
                $countInprogress    = Incident::whereNotIn('stage_id',['11','9','31'])->get()->count();
                $countAll           = Incident::all()->count();
                $countClose         = Incident::where('stage_id','9')->get()->count();
                $countBreach        = Incident::whereIn('stage_id',['32','33','34'])->get()->count();

                if($stage == "all")
                {
                    $paginated  = Incident::all();
                }
                elseif($stage == "breach")
                {
                    $paginated  = Incident::whereIn('stage_id', ['32','33','34'])->get();
                }
                else
                {
                    $paginated  = Incident::where('stage_id', $stage)->get();
                }
                
                return view('incidents.index', compact(
                    'paginated'
                    ,'stage'
                    ,'urlpath'
                    ,'countInprogress'
                    ,'countAll'
                    ,'countClose'
                    ,'countBreach')
                );
            }
            else
            {
                $countInprogress    = Incident::ofLoggedUser()->whereNotIn('stage_id',['11','9','31'])->get()->count();
                $countWaitingConfirmation = Incident::ofLoggedUser()->where('stage_id','5')->get()->count();
                $countAll = Incident::ofLoggedUser()->get()->count();
                $countClose = Incident::ofLoggedUser()->where('stage_id','9')->get()->count();

                if($stage == 'all')
                {
                    $paginated  = Incident::ofLoggedUser()->get();
                    return view('incidents.indexemployee', compact('paginated','stage','urlpath','countInprogress','countWaitingConfirmation','countAll','countClose'));
                }
                else
                {
                
                    $paginated  = Incident::ofLoggedUser()->where('stage_id',$stage)->get();
                   
                    return view('incidents.indexemployee', compact('paginated','stage','urlpath','countInprogress','countWaitingConfirmation','countAll','countClose'));
                }
            }
        }
        elseif(Auth::user()->hasAllRoles(['service desk']))
        {
            $countInprogress    = Incident::whereIn('stage_id',['3','4','6'])->get()->count();
            $countTicket        = Incident::where('stage_id','3')->get()->count();
            $countDeniedByUser  = Incident::where('stage_id','29')->get()->count();
            $countResolve       = Incident::where('stage_id','5')->get()->count();
            $countBreach        = Incident::whereIn('stage_id',['32','33','34'])->get()->count();
            $countClose         = Incident::where('stage_id','9')->get()->count();
            $countAll           = Incident::all()->count();

            if($stage == 'all')
            {
                $paginated  = Incident::all();
                return view('incidents.index', compact('paginated','stage','urlpath','countInprogress','countTicket','countDeniedByUser','countResolve','countBreach','countBreach','countAll','countClose'));
            }
            elseif($stage == 'breach')
            {
                $paginated  = Incident::whereIn('stage_id',['32','33','34'])->get();
                return view('incidents.index', compact('paginated','stage','urlpath','countInprogress','countTicket','countDeniedByUser','countResolve','countBreach','countBreach','countAll','countClose'));
            }
            else
            {
                $paginated  = Incident::where('stage_id',$stage)->get(); 
               
                return view('incidents.index', compact('paginated','stage','urlpath','countInprogress','countTicket','countDeniedByUser','countResolve','countBreach','countBreach','countAll','countClose'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $urlpath = "incident";
        $priorities = Priority::all();
        $severities = Severity::all();
        return view('incidents.create', compact('severities','priorities','urlpath'));
    }

    /**
     * Store a newly created resource in storage.
     * insert data ke tabel incident
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIncidentRequest $request)
    {
        $incident = new Incident();
        $incident->description  = $request->input('description');
        $incident->impact       = $request->input('impact');
        $incident->priority_id  = $request->input('priority_id');
        $incident->severity_id  = $request->input('severity_id');
        $incident->location     = $request->input('location');
        $incident->user_id      = Auth::user()->id;
        $incident->stage_id     = Stage::waitingForServiceDesk()->first()->id;
        $incident->telp         = $request->input('telp');
        $incident->save();

        // insert data attachment 
        if(!empty($request->file('attachment')))
        {
            foreach($request->file('attachment') as $files)
            {
                // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                $dateNow    = Carbon::now()->toDateTimeString();
                $date       = Carbon::parse($dateNow)->format('dmYHis');
                $name       = $files->getClientOriginalName();
                $username   = Auth::user()->id;
                $filename   = $date.$username.$name;
                // upload data
                $item = $files->storeAs('attachments', $filename);
                // input data file
                $incident->incidentAttachments()->save(new IncidentAttachment(['attachment' => $item, 'name' => $filename, 'alias' => $name]));
            }
        }

        $incident->incidentActions()->save(new IncidentAction(['user' => Auth::user()->id, 'action_type' => Action::Ccreate()->first()->id]));
        
        return redirect()
            ->route('incidents.index')
            ->with('success','incident berhasil di buat');
    }

    /**
     * Display the specified resource.
     * dapatkan jumlah notifikasi
     * baca nottifikasi berdasarkan user dan stage nya
     * redirect ke view incidents/show.blade.php
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $user=null)
    {
        Auth::loginUsingId($user);
        $urlpath = "incident";

        $incident = Incident::find($id);
        $notification = Auth::user()->notifications->filter(function($item, $key) use($incident){
            return $item->data['id'] == $incident->id and  $item->data['stage_id'] == 4;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        $notification = Auth::user()->notifications->filter(function($item, $key) use($incident){
            return $item->data['id'] == $incident->id and  $item->data['stage_id'] == 6;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        $notification = Auth::user()->notifications->filter(function($item, $key) use($incident){
            return $item->data['id'] == $incident->id and  $item->data['stage_id'] == 30;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        
        $notification = Auth::user()->notifications->filter(function($item, $key) use($incident){
            return $item->data['id'] == $incident->id and  $item->data['stage_id'] == 9;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        
        $notifications = Auth::user()->notifications->filter(function($item, $key) use($incident){
            return $item->data['id'] == $incident->id and  $item->data['stage_id'] == 5;
        });
        foreach($notifications as $notification)
        {
            //dd($notification);
            if($notification->read_at == NULL)
            {
                $notification->markAsRead();
            }   
        }

        // return view('incidents.show', compact('incident','urlpath'));
        $incidentActions = $incident->incidentActions->map(function($value, $item){
            return IncidentAction::with(['incidentActionNotes','action','users'])
                ->find($value->id); 
        });
        return response()->json(collect([
            'incident' => $incident,
                [
                    $incident->stage,
                    $incident->priority,
                    $incident->severity,
                    $incident->user,
                ],
            'incidentActions' => $incidentActions,
            'incidentAttachments' => $incident->incidentAttachments,
        ]), 200);
    }

    /**
     * Show the form for editing the specified resource.
     * menampilkan form edit
     * letak file berada di direktori incidents/edit.blade.php
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Incident $incident)
    {
        $urlpath = "incident";
        return view('incidents.edit', compact('incident','urlpath'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incident $incidents)
    {
        $incidents->description  = $request->input('description');
        $incidents->impact       = $request->input('impact');
        $incidents->priority_id  = $request->input('priority_id');
        $incidents->severity_id  = $request->input('severity_id');
        $incidents->location     = $request->input('location');
        $incidents->user_id      = Auth::user()->id;
        $incidents->stage_id     = Stage::waitingForServiceDesk()->first()->id;
        $incidents->telp         = $request->input('telp');
        
        // insert data attachment 
        if(!empty($request->file('attachment')))
        {
            foreach($request->file('attachment') as $files)
            {
                // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                $dateNow    = Carbon::now()->toDateTimeString();
                $date       = Carbon::parse($dateNow)->format('dmYHis');
                $name       = $files->getClientOriginalName();
                $username   = Auth::user()->id;
                $filename   = $date.$username.$name;
                // upload data
                $item = $files->storeAs('attachments', $filename);
                // input data file
                $incidents->incidentAttachments()->save(new IncidentAttachment(['attachment' => $item, 'name' => $filename, 'alias' => $name]));
            }
        }
        return redirect()
            ->route('incidents.index')
            ->with('success','incident berhasil dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incident $incident)
    {
        $incident->delete();
        return redirect()
            ->route('incidents.index')
            ->with('success','incident berhasil di delete');
    }

    public function detailsave(Request $request, Incident $incidents, $id)
    {
        $incidents = Incident::find($id);
        $incidents->detail = $request->input('detail');
        $incidents->stage_id = Stage::waitingUserConf()->first()->id;
        $incidents->save();

        $incAction = new IncidentAction();
        $incAction->user = Auth::user()->id;
        $incAction->incident_id = $id;
        $incAction->action_type = Action::detailInput()->first()->id;
        $incAction->save();

        $incAction->incidentActionNotes()->save(new IncidentActionNote(['note' => $request->input('detail')]));

        $notification = Auth::user()->notifications->filter(function($item, $key) use($incidents){
            return $item->data['id'] == $incidents->id and  $item->data['stage_id'] == 4;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        return redirect()
            ->route('incidents.index')
            ->with('success','Detail berhasil di input, Terimkasih');
    }

    public function detailshow(Incident $incident, $user=null)
    {
        Auth::loginUsingId($user);
        $urlpath = "incident";

        $priority = Priority::find($incident->priority_id)->name;
        $severity = Severity::find($incident->priority_id)->name;
        return view('incidentapprovals.edit', compact('incident','priority','severity','urlpath'));
    }

    public function noteshow(Incident $incident, $user=null)
    {
        Auth::loginUsingId($user);
        $urlpath = "incident";

        $priority = Priority::find($incident->priority_id)->name;
        $severity = Severity::find($incident->priority_id)->name;
        return view('incidents.shownote', compact('incident','priority','severity','urlpath'));
    }

    public function approveshow(Request $request, $id, $user=null)
    {
        Auth::loginUsingId($user);
        $incidents = Incident::find($id);
        $incidents->stage_id = Stage::closed()->first()->id;
        $incidents->save();

        $incAction = new IncidentAction();
        $incAction->user = Auth::user()->id;
        $incAction->incident_id = $id;
        $incAction->action_type = Action::approve()->first()->id;
        $incAction->save();

        $notification = Auth::user()->notifications->filter(function($item, $key) use($incidents){
            return $item->data['id'] == $incidents->id and  $item->data['stage_id'] == 6;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        return redirect()
            ->route('incidents.index')
            ->with('success','Incident disetujui');
    }

    public function arsip(Request $request, $id, $user=null)
    {
        $incidents = Incident::find($id);
        $incidents->stage_id = Stage::arsip()->first()->id;
        $incidents->save();

        $incAction = new IncidentAction();
        $incAction->user = Auth::user()->id;
        $incAction->incident_id = $id;
        $incAction->action_type = Action::arsip()->first()->id;
        $incAction->save();

        $notification = Auth::user()->notifications->filter(function($item, $key) use($incidents){
            return $item->data['id'] == $incidents->id and  $item->data['stage_id'] == 9;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        return redirect()
            ->route('incidents.index')
            ->with('success','Incident berhasil diarsipkan.');
    }

    public function escalation(Request $request)
    {
        $incidents = Incident::find($request->input('id1'));
        $incidents->stage_id = Stage::waitingForOperationDesk()->first()->id;
        $incidents->save();

        $incAction = new IncidentAction();
        $incAction->user = Auth::user()->id;
        $incAction->incident_id = $request->input('id1');
        $incAction->action_type = Action::escalation()->first()->id;
        $incAction->save();

        $incAction->incidentActionNotes()->save(new IncidentActionNote(['note' => $request->input('note1')]));
        
        $notification = Auth::user()->notifications->filter(function($item, $key) use($incidents){
            return $item->data['id'] == $incidents->id and  $item->data['stage_id'] == 3;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        return redirect()
            ->route('incidents.showperstage',3)
            ->with('success','Incident berhasil dieskalasi ke specialist servicedesk.');
    }

    public function fail(Request $request)
    {
        //dd($request->input('id2'));

        $incidents = Incident::find($request->input('id2'));
        $incidents->stage_id = Stage::failedStage()->first()->id;
        $incidents->save();

        $incAction = new IncidentAction();
        $incAction->user = Auth::user()->id;
        $incAction->incident_id = $request->input('id2');
        $incAction->action_type = Action::failAction()->first()->id;
        $incAction->save();

        $incAction->incidentActionNotes()->save(new IncidentActionNote(['note' => $request->input('note2')]));
        
        $notification = Auth::user()->notifications->filter(function($item, $key) use($incidents){
            return $item->data['id'] == $incidents->id and  $item->data['stage_id'] == 3;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        return redirect()
            ->route('approval.show','incident')
            ->with('success','Incident berhasil di Fail.');
    }

    public function rejectform(Incident $incident, $user=null)
    {
        Auth::loginUsingId($user);
        $urlpath = "incident";

        $priority = Priority::find($incident->priority_id)->name;
        $severity = Severity::find($incident->priority_id)->name;
        return view('incidentapprovals.reject', compact('incident','priority','severity','urlpath'));
    }

    public function rejectshow(Request $request, Incident $incidents, $id)
    {
        $incidents = Incident::find($id);
        $incidents->stage_id = Stage::deniedByUser()->first()->id;
        $incidents->save();

        // insert data attachment 
        if(!empty($request->file('attachment')))
        {
            foreach($request->file('attachment') as $files)
            {
                // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                $dateNow    = Carbon::now()->toDateTimeString();
                $date       = Carbon::parse($dateNow)->format('dmYHis');
                $name       = $files->getClientOriginalName();
                $username   = Auth::user()->id;
                $filename   = $date.$username.$name;
                // upload data
                $item = $files->storeAs('attachments', $filename);
                // input data file
                $incidents->incidentRejectattachments()->save(new IncidentRejectattachment(['attachment' => $item, 'name' => $filename, 'alias' => $name]));
            }
        }

        $incAction = new IncidentAction();
        $incAction->user = Auth::user()->id;
        $incAction->incident_id = $id;
        $incAction->action_type = Action::reject()->first()->id;
        $incAction->save();

        $incAction->incidentActionNotes()->save(new IncidentActionNote(['note' => $request->input('note')]));

        $notification = Auth::user()->notifications->filter(function($item, $key) use($incidents){
            return $item->data['id'] == $incidents->id and  $item->data['stage_id'] == 6;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }

        return redirect()
            ->route('incidents.index')
            ->with('success','Incident berhasil ditolak.');
    }

    public function ticketshow(Incident $incident, $user=null)
    {
        Auth::loginUsingId($user);
        $urlpath = "incident";

        $priority = Priority::find($incident->priority_id)->name;
        $severity = Severity::find($incident->priority_id)->name;
        return view('incidentapprovals.ticket', compact('incident','priority','severity','urlpath'));
    }

    public function ticketcreated(Request $request, Incident $incidents, $id)
    {
        $incidents = Incident::find($id);
        $incidents->stage_id = Stage::ticketCreated()->first()->id;
        $incidents->ticket = $request->input('ticket');
        $incidents->save();

        $incAction = new IncidentAction();
        $incAction->user = Auth::user()->id;
        $incAction->incident_id = $id;
        $incAction->action_type = Action::ticketInput()->first()->id;
        $incAction->save();

        return redirect()
            ->route('incidents.index')
            ->with('success','Nomor ticket berhasil di input');
    }
    
}
