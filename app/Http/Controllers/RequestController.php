<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use UrlSigner;
use App\ITRequest;
use App\Service;
use App\Stage;
use App\Status;
use App\Category;
use App\RequestApproval;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreITRequestRequest;
use App\Http\Requests\SpictActionRequest;
use App\Http\Requests\ApprovrsaveITRequestRequest;
use App\Http\Requests\SoActionRequest;
use App\Http\Requests\TicketInputITRequest;
use Illuminate\Support\Facades\Storage;
use App\Notifications\RequestCreated;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\RequestReasonfile;
use App\RequestAttachment;
use App\RequestSo;
use App\RequestSoBps;
use App\ServiceCoordinator;
use App\User;
use App\Action;
use App\RequestAction;
use App\RequestActionNote;
use Carbon\Carbon;
use App\RequestRejectattachment;
use App\Secretary;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user = null)
    {    
        Auth::loginUsingId($user);
        $userId = Auth::user()->id;
        $stage  = null;
        $file   = null;
        $urlpath = "request";

        if(Auth::user()->hasAllRoles(['employee']))
        {
            if(Auth::user()->hasAllRoles(['boss']))
            {
                if(Auth::user()->hasRole('manager beict'))
                {
                    // $countWaitingApproved = ITRequest::where('stage_id', '12')->orWhere(function($q){
                    //     $q->ofBossSubordinates();
                    //     $q->where('stage_id', '1');
                    // })->count();

                    // $paginated  = ITRequest::where('stage_id', '12')->orWhere(function($q){
                    //     $q->ofBossSubordinates();
                    //     $q->where('stage_id', '1');
                    // })->orderBy('updated_at', 'asc')->get();

                    $countWaitingApproved  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get()
                        ->count();

                    $countSrfAprroved = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '2')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '2');
                        })
                        ->get()
                        ->count();

                    $countCrfAprroved = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '3')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '3');
                        })
                        ->get()
                        ->count();

                    $countRejected = ITRequest::where('stage_id', '11')
                        ->get()
                        ->count();
                    $countAll = ITRequest::all()->count();

                    $file       = 'requests.index';

                    $paginated  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get();
                    
                    return view($file,compact(
                            'paginated',
                            'userId',
                            'stage',
                            'urlpath',
                            'countWaitingApproved',
                            'countSrfAprroved',
                            'countCrfAprroved',
                            'countRejected',
                            'countAll')
                        );
                }
                else
                {
                    
                    //dd($countSrfAprroved);
                    $countWaitingApproved  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get()
                        ->count();

                    $countSrfAprroved = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '2')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '2');
                        })
                        ->get()
                        ->count();

                    $countCrfAprroved = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '3')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '3');
                        })
                        ->get()
                        ->count();

                    $countRejected = ITRequest::where('boss', Auth::user()->id)
                        ->where('boss', Auth::user()->id)
                        ->where('stage_id', '11')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '11');
                        })
                        ->get()
                        ->count();

                    $countAll = ITRequest::where('boss', Auth::user()->id)
                      ->orWhere(function($q){
                            $q->ofBossSubordinates();
                        })
                        ->get()
                        ->count();

                    $paginated  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get();

                    $file       = 'requests.index';

                    return view($file,compact(
                        'paginated',
                        'userId',
                        'stage',
                        'urlpath',
                        'countWaitingApproved',
                        'countSrfAprroved',
                        'countCrfAprroved',
                        'countRejected',
                        'countAll')
                    );
                }
            }
            elseif(Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so perangkat komputer']) or Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so jaringan']) or Auth::user()->hasAllRoles(['so messaging']))
            {
                if(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
                {
                    $data = array_column(
                        Auth::user()
                            ->roles()
                            ->get()
                            ->toArray(), 'id'
                        );
    
                    $serv  = array_column(
                        ServiceCoordinator::whereIn('role_id', $data)
                        ->get()
                        ->toArray(),'service_id'
                    );
    
                    $all = RequestSo::with('request')
                        ->whereIn('service_id',$serv)
                        ->groupBy('request_id')
                        ->get()
                        ->count();
    
                    // $paginated  = RequestSo::with(
                    //     ['request' => function($query) {
                    //             $query->whereIn('stage_id', ['13']);
                    //         }
                    //     ]
                    // )->whereIn('service_id',$serv)
                    // ->groupBy('request_id')->get();
                        
                    $paginated = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get();
    
                    $countWaitingApproved = $paginated->count();
    
                    $file = 'requests.indexco';
    
                    return view($file,compact(
                        'paginated',
                        'countWaitingApproved',
                        'urlpath',
                        'all',
                        'stage'
                        )
                    );
                }
                else
                {
                    $data = Auth::user()
                        ->roles()
                        ->get()
                        ->toArray();

                    $serv = array_column(
                        Service::whereIn('role_id',
                        array_column($data,'id'))
                        ->get()
                        ->toArray(),'id'
                    );

                    // $paginated  = RequestSo::with(['request' => function ($query) {
                    //                     $query->whereIn('stage_id', ['10','13','15','14']);
                    //                 }])->whereIn('service_id',$serv)->get();
                    //dd($paginated);
                    
                    $paginated  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get();

                    $paginated1 = RequestSo::with('request')
                        ->whereIn('service_id',$serv)
                        ->get();

                    $countWaitingApproved = $paginated->count();
                    
                    $request_waiting_so_approved = RequestSo::withCount(
                        ['request' => function ($query) 
                            {
                                $query->whereIn('stage_id', ['10','13','15','14']);
                            }
                        ])->whereIn('service_id',$serv)
                        ->get()
                        ->toArray();

                    $arraydata_waiting_so_approved = array_column($request_waiting_so_approved, 'request_count');

                    $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);

                    // dd($counts__waiting_so_approved);
                    // if(empty($counts__waiting_so_approved))
                    // {
                    //     $countWaitingApproved           = 0;
                    // }
                    // else
                    // {
                    //     if(isset($counts__waiting_so_approved[1]))
                    //     {
                    //         $countWaitingApproved       = $counts__waiting_so_approved[1];
                    //     }
                    //     else
                    //     {
                    //         $countWaitingApproved       = 0;
                    //     }
                    // }
                    // $countWaitingApproved               = $counts__waiting_so_approved[1];


                    $countAll  = $paginated1->count();
                    
                    $countBreach  = ITRequest::whereIn('stage_id', ['32','33','34'])
                        ->get()
                        ->count();

                    $file = 'requests.index';

                    return view($file,compact(
                        'paginated',
                        'userId',
                        'stage',
                        'urlpath',
                        'countWaitingApproved',
                        'countAll',
                        'countBreach'
                        )
                    );
                }
            }
            // elseif(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
            // {
            //     $data = array_column(
            //         Auth::user()
            //             ->roles()
            //             ->get()
            //             ->toArray(), 'id'
            //         );

            //     $serv  = array_column(
            //         ServiceCoordinator::whereIn('role_id', $data)
            //         ->get()
            //         ->toArray(),'service_id'
            //     );

            //     $all = RequestSo::with('request')
            //         ->whereIn('service_id',$serv)
            //         ->groupBy('request_id')
            //         ->get()
            //         ->count();

            //     // $paginated  = RequestSo::with(
            //     //     ['request' => function($query) {
            //     //             $query->whereIn('stage_id', ['13']);
            //     //         }
            //     //     ]
            //     // )->whereIn('service_id',$serv)
            //     // ->groupBy('request_id')->get();
                    
            //     $paginated = ITRequest::ofLoggedUser()
            //         ->whereNotIn('stage_id',['9','11','31'])
            //         ->get();

            //     $countWaitingApproved = $paginated->count();

            //     $file = 'requests.index';

            //     return view($file,compact(
            //         'paginated',
            //         'countWaitingApproved',
            //         'urlpath',
            //         'all',
            //         'stage'
            //         )
            //     );
            // }
            elseif(Auth::user()->hasAllRoles(['operation sd']))
            {
                // $countWaitingApproved   = ITRequest::where('stage_id', '2')->get()->count();

                $countWaitingApproved = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get()
                    ->count();

                $countApproved = ITRequest::where('stage_id', '3')
                    ->get()
                    ->count();

                $countEscalate = ITRequest::where('stage_id', '10')
                    ->get()
                    ->count();

                $countAll = ITRequest::all()
                    ->count();

                $countBreach = ITRequest::whereIn('stage_id', ['32','33','34'])
                    ->get()
                    ->count();

                $paginated  = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get();

                $file = 'requests.index';

                return view($file,compact(
                    'paginated',
                    'userId',
                    'stage',
                    'urlpath',
                    'countWaitingApproved',
                    'countApproved',
                    'countEscalate',
                    'countAll',
                    'countBreach')
                );
            }
            elseif( Auth::user()->hasAllRoles(['operation ict']))
            {
                // $countWaitingApproved = ITRequest::where('stage_id','7')
                //     ->get()
                //     ->count();

                $countWaitingApproved = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get()
                    ->count();

                $countAll = ITRequest::all()
                    ->count();

                $paginated =  ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get();

                $file = 'requests.index';

                return view($file, compact(
                    'paginated',
                    'userId',
                    'stage',
                    'urlpath',
                    'countWaitingApproved',
                    'countAll'
                    )
                );
            }
            elseif(Auth::user()->hasAllRoles(['chief']))
            {
                $countWaitingApproved = ITRequest::where('stage_id','19')
                    ->get()
                    ->count();

                $countAll = ITRequest::all()
                    ->count();

                $paginated = ITRequest::where('stage_id','19')
                    ->get();

                $file = 'requests.index';

                return view($file, compact(
                    'paginated',
                    'userId',
                    'stage',
                    'urlpath',
                    'countWaitingApproved',
                    'countAll'
                    )
                );
            }
            else
            {
                $countInprogressEmployee = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get()
                    ->count();

                $countCloseEmployee  = ITRequest::ofLoggedUser()
                    ->where('stage_id','9')
                    ->get()
                    ->count();

                $countRejectedEmployee = ITRequest::ofLoggedUser()
                    ->where('stage_id','11')
                    ->get()
                    ->count();

                $paginated = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get();

                $file = 'requests.indexemploye';

                return view($file, compact(
                    'paginated',
                    'userId',
                    'stage',
                    'urlpath',
                    'countInprogressEmployee',
                    'countCloseEmployee',
                    'countRejectedEmployee'
                    )
                );
            }

        }
        elseif(Auth::user()->hasAllRoles(['service desk']))
        {
            $countInprogress = ITRequest::whereNotIn('stage_id', ['11','31','9'])
                ->get()
                ->count();

            $countWaitingInputTicket = ITRequest::where('stage_id', '3')
                ->get()
                ->count();

            $countSolved = ITRequest::where('stage_id', '5')
                ->get()
                ->count();

            $countBreach = ITRequest::whereIn('stage_id', ['32','33','34'])
                ->get()
                ->count();

            $countAll = ITRequest::all()
                ->count();

            $countClose = ITRequest::where('stage_id', '9')
                ->get()
                ->count();

            $paginated = ITRequest::whereNotIn('stage_id', ['11','31','9'])
                ->get();

            $file = 'requests.index';

            return view($file, compact(
                'paginated',
                'userId',
                'stage',
                'urlpath',
                'countWaitingInputTicket',
                
                'countAll',
                'countClose',
                'countSolved',
                'countBreach',
                'countInprogress'
                )
            );
        }

        //return view($file,compact('paginated','userId','stage','urlpath'));
    }

    public function viewperstage($stage=null)
    {    
        $userId = Auth::user()->id;
        $urlpath = "request";

        if(Auth::user()->hasAllRoles(['employee']))
        {
            if(Auth::user()->hasAllRoles(['boss']))
            {
                if(Auth::user()->hasAllRoles(['manager beict']))
                {
                    // $countWaitingApproved = ITRequest::where('stage_id', '12')->orWhere(function($q){
                    //     $q->ofBossSubordinates();
                    //     $q->where('stage_id', '1');
                    // })->count();

                    $countWaitingApproved  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get()
                        ->count();

                    //dd($countWaitingApproved);
                   
                    $countSrfAprroved   = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '2')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '2');
                        })
                        ->get()
                        ->count();

                    $countCrfAprroved   = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '3')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '3');
                        })
                        ->get()
                        ->count();

                    $countRejected      = ITRequest::where('stage_id', '11')
                        ->get()
                        ->count();

                    $countAll           = ITRequest::all()->count();

                    if($stage == 'all')
                    {
                        $paginated  = ITRequest::all();
                        $file       = 'requests.index';
                    }
                    else
                    {
                        $file       = null;
                        $stage      = $stage;
                        $paginated  = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', $stage)
                            ->orWhere(function($q)use($stage){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', $stage);
                            })
                            ->get();
                        $file       = 'requests.indexperstage';
                    }

                    return view($file,compact(
                        'paginated',
                        'stage',
                        'userId',
                        'urlpath',
                        'countWaitingApproved',
                        'countSrfAprroved',
                        'countCrfAprroved',
                        'countRejected',
                        'countAll'
                        )
                    );
                }
                else
                {
                    $countWaitingApproved  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get()
                        ->count();

                    $countSrfAprroved = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '2')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '2');
                        })
                        ->get()
                        ->count();

                    $countCrfAprroved = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '3')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '3');
                        })
                        ->get()
                        ->count();

                    $countRejected = ITRequest::where('boss', Auth::user()->id)
                        ->where('stage_id', '11')
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                            $q->where('stage_id', '11');
                        })
                        ->get()
                        ->count();

                    $countAll = ITRequest::where('boss', Auth::user()->id)
                        ->orWhere(function($q){
                            $q->ofBossSubordinates();
                        })
                        ->get()
                        ->count();

                    if($stage == 'all')
                    {
                        $paginated  = ITRequest::where('boss', Auth::user()->id)
                            ->orWhere(function($q){
                                $q->ofBossSubordinates();
                            })
                            ->get();
                        $file       = 'requests.index';
                    }
                    else
                    {
                        $paginated  = ITRequest::where('boss', Auth::user()->id)
                            ->where('stage_id', $stage)
                            ->orWhere(function($q)use($stage){
                                $q->ofBossSubordinates();
                                $q->where('stage_id', $stage);
                            })
                            ->get();

                        $file       = 'requests.indexperstage';
                    }

                    return view($file,compact(
                        'paginated',
                        'stage',
                        'userId',
                        'urlpath',
                        'countWaitingApproved',
                        'countSrfAprroved',
                        'countCrfAprroved',
                        'countRejected',
                        'countAll'
                        )
                    );
                }
            }
            elseif(Auth::user()->hasAllRoles(['operation sd']))
            {
                // $countWaitingApproved = ITRequest::where('stage_id', '2')
                //     ->get()
                //     ->count();

                $countWaitingApproved  = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get()
                    ->count();

                $countApproved = ITRequest::where('stage_id', '3')
                    ->get()
                    ->count();

                $countEscalate = ITRequest::where('stage_id', '10')
                    ->get()
                    ->count();

                $countAll = ITRequest::all()
                    ->count();

                $countBreach = ITRequest::whereIn('stage_id', ['32','33','34'])
                    ->get()
                    ->count();


                if($stage == 'all')
                {
                    $paginated  = ITRequest::all();
                    $file       = 'requests.index';
                }
                elseif($stage == "breach")
                {
                    $file       = null;
                    $stage      = $stage;
                    $stageall   = Stage::all();
                    $paginated  = ITRequest::whereIn('stage_id', ['32','33','34'])->get();
                    $file       = 'requests.indexperstage';
                }
                else
                {
                    $file       = null;
                    $stage      = $stage;
                    $stageall   = Stage::find($stage);
                    $paginated  = ITRequest::where('stage_id', $stage)->get();
                    $file       = 'requests.indexperstage';
                }

                return view($file,compact(
                    'paginated',
                    'stage',
                    'userId',
                    'urlpath',
                    'countWaitingApproved',
                    'countApproved',
                    'countEscalate',
                    'countAll',
                    'countBreach'
                    )
                );
            }
            elseif(Auth::user()->hasAllRoles(['operation ict']))
            {
                // $countWaitingApproved = ITRequest::where('stage_id','7')
                // ->get()
                // ->count();

                $countWaitingApproved  = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get()
                    ->count();

                $countAll = ITRequest::all()
                ->count();

                if($stage == 'all')
                {
                    $paginated  = ITRequest::all();
                    $file       = 'requests.index';
                }
                else
                {
                    $file       = null;
                    $stage      = $stage;
                    $stageall   = Stage::find($stage);
                    $paginated  = ITRequest::where('stage_id', $stage)->get();
                    $file       = 'requests.indexperstage';
                }

                return view($file,compact(
                    'paginated',
                    'stage',
                    'userId',
                    'urlpath',
                    'countWaitingApproved',
                    'countAll'
                    )
                );
            }
            elseif(Auth::user()->hasAllRoles(['chief']))
            {
                $countWaitingApproved = ITRequest::where('stage_id','19')
                    ->get()
                    ->count();
                $countAll = ITRequest::all()
                    ->count();
                if($stage == 'all')
                {
                    $paginated  = ITRequest::all();
                    $file       = 'requests.index';
                }
                else
                {
                    $file       = null;
                    $stage      = $stage;
                    $stageall   = Stage::find($stage);
                    $paginated  = ITRequest::where('stage_id', $stage)->get();
                    $file       = 'requests.indexperstage';
                }
                return view($file,compact(
                    'paginated',
                    'stageall',
                    'stage',
                    'userId',
                    'urlpath',
                    'countWaitingApproved',
                    'countAll'
                    )
                );
            }
            elseif(Auth::user()->hasAllRoles(['so web']) or Auth::user()->hasAllRoles(['so mes flat']) or Auth::user()->hasAllRoles(['so mes long']) or Auth::user()->hasAllRoles(['so sap hr']) or Auth::user()->hasAllRoles(['so sap ppqm']) or Auth::user()->hasAllRoles(['so sap fico']) or Auth::user()->hasAllRoles(['so sap sd']) or Auth::user()->hasAllRoles(['so sap pm']) or Auth::user()->hasAllRoles(['so sap mm']) or Auth::user()->hasAllRoles(['so sap psim']) or Auth::user()->hasAllRoles(['so perangkat komputer']) or Auth::user()->hasAllRoles(['so aplikasi office']) or Auth::user()->hasAllRoles(['so vicon']) or Auth::user()->hasAllRoles(['so printer']) or Auth::user()->hasAllRoles(['so jaringan']) or Auth::user()->hasAllRoles(['so messaging']))
            {
                if(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
                {
                    if($stage == 'all')
                    {
                        $data = array_column(
                            Auth::user()
                            ->roles()
                            ->get()
                            ->toArray(), 'id'
                        );
                        $serv = array_column(
                            ServiceCoordinator::whereIn('role_id', $data)
                            ->get()
                            ->toArray(),'service_id'
                        );
                        $paginated = RequestSo::with('request')
                            ->whereIn('service_id',$serv)
                            ->groupBy('request_id')
                            ->get();
                        
                        $all = $paginated->count();
                        $countWaitingApproved = ITRequest::ofLoggedUser()
                            ->whereNotIn('stage_id',['9','11','31'])
                            ->get()
                            ->count();
                        $file = 'requests.indexco';
                        return view($file,compact(
                            'paginated',
                            'urlpath',
                            'stage',
                            'countWaitingApproved',
                            'all'
                            )
                        );
                    }
                    else
                    {
                        $data = array_column(
                            Auth::user()
                            ->roles()
                            ->get()
                            ->toArray(), 'id'
                        );
                        $serv = array_column(
                            ServiceCoordinator::whereIn('role_id', $data)
                            ->get()
                            ->toArray(),'service_id'
                        );
                        $paginated = RequestSo::with(['request' => function($query){
                            $query->where('stage_id', 'like', $stage);
                        }])
                        ->whereIn('service_id',$serv)
                        ->groupBy('request_id')
                        ->get();
                        $file       = 'requests.indexco';
                        return view($file,compact('paginated','urlpath','stage'));
                    }
                }
                else
                {
                    if($stage == 'all')
                    {
                        $data = Auth::user()
                            ->roles()
                            ->get()
                            ->toArray();
                        $stage = $stage;
                        $serv = array_column(
                            Service::whereIn('role_id',array_column($data,'id'))
                            ->get()
                            ->toArray(),'id'
                        );
                        $paginated = RequestSo::with('request')
                            ->whereIn('service_id',$serv)
                            ->get();
                        $file = 'requests.indexso';
                    }
                    else
                    {
                        $data = Auth::user()
                            ->roles()
                            ->get()
                            ->toArray();
                        $stage = $stage;
                        $serv = array_column(
                            Service::whereIn('role_id',array_column($data,'id'))
                            ->get()
                            ->toArray(),'id'
                        );
                        $paginated = RequestSo::with(['request' => function ($query) use ($stage) {
                                            $query->where('stage_id', 'like', $stage);
                                        }])->get();
                    
                    }
                    $paginatedoo  = ITRequest::ofLoggedUser()
                        ->whereNotIn('stage_id',['9','11','31'])
                        ->get();
                    $countWaitingApproved = $paginatedoo->count();
                    $file = 'requests.indexso';
                    $paginated1 = RequestSo::with('request')
                        ->whereIn('service_id',$serv)
                        ->get();
                    $request_waiting_so_approved = RequestSo::withCount(['request' => function ($query) {
                        $query->whereIn('stage_id', ['10','13','15','14']);
                    }])->whereIn('service_id',$serv)->get()->toArray();
                    $arraydata_waiting_so_approved  = array_column($request_waiting_so_approved, 'request_count');
                    $counts__waiting_so_approved = array_count_values($arraydata_waiting_so_approved);
                    // if(empty($counts__waiting_so_approved))
                    // {
                    //     $countWaitingApproved = 0;
                    // }
                    // else
                    // {
                    //     if(isset($counts__waiting_so_approved[1]))
                    //     {
                    //         $countWaitingApproved = $counts__waiting_so_approved[1];
                    //     }
                    //     else
                    //     {
                    //         $countWaitingApproved = 0;
                    //     }
                    // }
                
                    $countAll = $paginated1->count();
                    $countBreach = ITRequest::whereIn('stage_id', ['32','33','34'])
                        ->get()
                        ->count();
                    return view($file, compact(
                        'paginated',
                        'stage',
                        'userId',
                        'urlpath',
                        'countWaitingApproved',
                        'countAll',
                        'countBreach'
                        )
                    );
                }
            }
            // elseif(Auth::user()->hasAllRoles(['coordinator aplikasi']) or Auth::user()->hasAllRoles(['coordinator infra']))
            // {
            //     if($stage == 'all')
            //     {
            //         $data = array_column(
            //             Auth::user()
            //             ->roles()
            //             ->get()
            //             ->toArray(), 'id'
            //         );
            //         $serv = array_column(
            //             ServiceCoordinator::whereIn('role_id', $data)
            //             ->get()
            //             ->toArray(),'service_id'
            //         );
            //         $paginated = RequestSo::with('request')
            //             ->whereIn('service_id',$serv)
            //             ->groupBy('request_id')
            //             ->get();
                    
            //         $all = $paginated->count();
            //         $countWaitingApproved = ITRequest::ofLoggedUser()
            //             ->whereNotIn('stage_id',['9','11','31'])
            //             ->get()
            //             ->count();
            //         $file = 'requests.indexso';
            //         return view($file,compact(
            //             'paginated',
            //             'urlpath',
            //             'stage',
            //             'countWaitingApproved',
            //             'all'
            //             )
            //         );
            //     }
            //     else
            //     {
            //         $data = array_column(
            //             Auth::user()
            //             ->roles()
            //             ->get()
            //             ->toArray(), 'id'
            //         );
            //         $serv = array_column(
            //             ServiceCoordinator::whereIn('role_id', $data)
            //             ->get()
            //             ->toArray(),'service_id'
            //         );
            //         $paginated = RequestSo::with(['request' => function($query){
            //             $query->where('stage_id', 'like', $stage);
            //         }])
            //         ->whereIn('service_id',$serv)
            //         ->groupBy('request_id')
            //         ->get();
            //         $file       = 'requests.indexso';
            //         return view($file,compact('paginated','urlpath','stage'));
            //     }
            // }
            else
            {
                $file = null;
                $stage = $stage;
                $stageall = Stage::find($stage);
                $countInprogressEmployee = ITRequest::ofLoggedUser()
                    ->whereNotIn('stage_id',['9','11','31'])
                    ->get()
                    ->count();
                $countCloseEmployee  = ITRequest::ofLoggedUser()
                    ->where('stage_id','9')
                    ->get()
                    ->count();
                $countRejectedEmployee  = ITRequest::ofLoggedUser()
                    ->where('stage_id','11')
                    ->get()
                    ->count();
                $paginated  = ITRequest::ofLoggedUser()
                    ->where('stage_id', $stage)
                    ->get();
                $file = 'requests.indexemploye';
                return view($file, compact(
                    'paginated',
                    'stageall',
                    'stage',
                    'userId',
                    'urlpath',
                    'countInprogressEmployee',
                    'countCloseEmployee',
                    'countRejectedEmployee'
                    )
                );
            }
        }
        elseif(Auth::user()->hasAllRoles(['service desk']))
        {
            
            $countInprogress = ITRequest::whereNotIn('stage_id', ['11','31','9'])
                ->get()
                ->count();
            $countWaitingInputTicket = ITRequest::where('stage_id', '3')
                ->get()
                ->count();
            $countSolved = ITRequest::where('stage_id', '5')
                ->get()
                ->count();
            $countBreach = ITRequest::whereIn('stage_id', ['32','33','34'])
                ->get()
                ->count();
            $countAll = ITRequest::all()
                ->count();
            $countClose = ITRequest::where('stage_id', '9')
                ->get()
                ->count();
            if($stage == 'all')
            {
                $paginated  = ITRequest::all();
                $file       = 'requests.index';
            }
            elseif($stage == 'breach')
            {
                $stage      = $stage;
                $file       = null;
                $paginated  = ITRequest::whereIn('stage_id', ['32','33','34'])->get();
                $file       = 'requests.indexperstage';
            }
            else
            {
                $file       = null;
                $stage      = $stage;
                $stageall   = Stage::find($stage);
                $paginated  = ITRequest::where('stage_id', $stage)->get();
                $file       = 'requests.indexperstage';
            }
            return view($file,compact('paginated','stage','userId','urlpath','countWaitingInputTicket','countAll','countClose','countSolved','countBreach','countInprogress'));
        }
        return view($file,compact('paginated','stageall','stage','userId','urlpath'));
    }
    public function viewnotin($stage=null)
    {
        $countInprogresEmployee = ITRequest::ofLoggedUser()
        ->whereNotIn('stage_id', ['9','11','31'])
        ->get()
        ->count();
        $paginated = ITRequest::ofLoggedUser()
        ->whereNotIn('stage_id', ['9','11','31'])
        ->get();
        $file = 'requests.indexemploye';
        $urlpath = "request";
        return view($file, compact(
            'paginated',
            'stage',
            'urlpath'
            )
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasAllRoles(['employee'])){
            if(Auth::user()->hasAllRoles(['boss'])){
                $user       = new User();
                $golongan   = $user->esCheck(Auth::user()->id);  
                $urlpath    = "request";
                $services   = Service::all();
                return view('requests.create_boss', compact('services','urlpath', 'golongan'));
            }
            else{
                $user       = new User();
                $golongan   = $user->esCheck(Auth::user()->id);  
                $urlpath    = "request";
                $services   = Service::all();
                return view('requests.create', compact('services','urlpath', 'golongan'));
            }
            
        }

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreITRequestRequest $request)
    {
        if($request->input('golongan') == 'A')
        {
            // input data ke table request
            // $r->attachment          = $request->file('attachment')->store('attachments');
            if(!empty($request->input('nda')))
            {
                $nda = $request->input('nda');
            }
            else
            {
                $nda = '0';
            }
            if(Auth::user()->hasAllRoles(['employee'])){
                if(Auth::user()->hasAllRoles(['boss'])){
                    $roleso = Service::find($request->service_id)->role_id;
                    $r = new ITRequest();
                    $r->service_id          = $request->input('service_id');
                    $r->title               = $request->input('title');
                    $r->business_need       = $request->input('business_need');
                    $r->business_benefit    = $request->input('business_benefit');
                    $r->stage_id            = Stage::WaitingForServiceDesk()->first()->id;
                    $r->category_id         = $request->input('category');
                    $r->keterangan          = $request->input('keterangan');
                    $r->telp                = $request->input('telp');
                    $r->location            = $request->input('location');
                    $r->nda                 = $request->input('nda');
                    $r->user_id             = Auth::user()->id;
                    $r->nda_date            = Carbon::now();
                    $r->so                  = $roleso;
                    $r->boss                = '';
                    $r->save();
                }
                else{
                    $roleso = Service::find($request->service_id)->role_id;
                    $r = new ITRequest();
                    $r->service_id          = $request->input('service_id');
                    $r->title               = $request->input('title');
                    $r->business_need       = $request->input('business_need');
                    $r->business_benefit    = $request->input('business_benefit');
                    $r->stage_id            = Stage::waitingForOperationDesk()->first()->id;
                    $r->category_id         = $request->input('category');
                    $r->keterangan          = $request->input('keterangan');
                    $r->telp                = $request->input('telp');
                    $r->location            = $request->input('location');
                    $r->nda                 = $request->input('nda');
                    $r->user_id             = Auth::user()->id;
                    $r->nda_date            = Carbon::now();
                    $r->so                  = $roleso;
                    $r->boss                = Auth::user()->boss()->id;

                }
            }
            // insert data attachment 
            if(!empty($request->file('attachment')))
            {
                foreach($request->file('attachment') as $files)
                {
                    // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                    $dateNow    = Carbon::now()->toDateTimeString();
                    $date       = Carbon::parse($dateNow)->format('dmYHis');
                    $name       = $files->getClientOriginalName();
                    $username   = Auth::user()->id;
                    $filename   = $date.$username.$name;
                    // upload data
                    $item = $files->storeAs('attachments', $filename);
                    // input data file
                    $r->requestAttachments()->save(new RequestAttachment(['attachment' => $item, 'name' => $filename, 'alias' => $name]));
                }
            }
            // insert data action
            $r->requestActions()->save(new RequestAction(['user' => Auth::user()->id, 'action_type' => Action::Ccreate()->first()->id]));
            // insert data so
            $r->requestSos()->save(new RequestSo(['service_id' => $request->input('service_id'), 'status' => 0]));
            // return redirect
            return redirect()
                ->route('requests.index')
                ->with('success','Permintaan layanan berhasil dibuat.');
        }
        else
        {
            // input data ke table request
            // $r->attachment          = $request->file('attachment')->store('attachments');
            if(!empty($request->input('nda')))
            {
                $nda = $request->input('nda');
            }
            else
            {
                $nda = '0';
            }
            $roleso = Service::find($request->service_id)->role_id;
            $r = new ITRequest();
            $r->service_id          = $request->input('service_id');
            $r->title               = $request->input('title');
            $r->business_need       = $request->input('business_need');
            $r->business_benefit    = $request->input('business_benefit');
            $r->stage_id            = Stage::waitingBossApproval()->first()->id;
            $r->category_id         = $request->input('category');
            $r->keterangan          = $request->input('keterangan');
            $r->telp                = $request->input('telp');
            $r->location            = $request->input('location');
            $r->nda                 = $request->input('nda');
            $r->user_id             = Auth::user()->id;
            $r->nda_date            = Carbon::now();
            $r->so                  = $roleso;
            $r->boss                = Auth::user()->boss()->id;
            $r->save();
            // insert data attachment 
            if(!empty($request->file('attachment')))
            {
                foreach($request->file('attachment') as $files)
                {
                    // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                    $dateNow    = Carbon::now()->toDateTimeString();
                    $date       = Carbon::parse($dateNow)->format('dmYHis');
                    $name       = $files->getClientOriginalName();
                    $username   = Auth::user()->id;
                    $filename   = $date.$username.$name;
                    // upload data
                    $item = $files->storeAs('attachments', $filename);
                    // input data file
                    $r->requestAttachments()->save(new RequestAttachment(['attachment' => $item, 'name' => $filename, 'alias' => $name]));
                }
            }
            // insert data action
            $r->requestActions()->save(new RequestAction(['user' => Auth::user()->id, 'action_type' => Action::Ccreate()->first()->id]));
            // insert data so
            $r->requestSos()->save(new RequestSo(['service_id' => $request->input('service_id'), 'status' => 0]));
            // return redirect
            return redirect()
                ->route('requests.index')
                ->with('success','Permintaan layanan berhasil dibuat.');
        }
    }
    public function uploaddocument(Request $r, ITRequest $request, $user=NULL)
    {
        // update tabel request
        $request->stage_id = Stage::waitingForSoApproval()->first()->id;
        $request->save();
        // input file attachment
        if(!empty('attachment'))
        {
            foreach( $r->file('attachment') as $files)
            {
                //storeAs
                //getClientOriginalName();
                $dateNow    = Carbon::now()->toDateTimeString();
                $date       = Carbon::parse($dateNow)->format('dmYHis');
                $name       = $files->getClientOriginalName();
                $username   = Auth::user()->id;
                $filename   = $date.$username.$name;
                // upload data
                $item = $files->storeAs('attachments', $filename);
                $rfiles             = new RequestAttachment();
                $rfiles->attachment = $files->storeAs('attachments', $filename);
                $rfiles->name       = $filename;
                $rfiles->request_id = $request->id;
                $rfiles->alias      = $name;
                $rfiles->save();
            }
        }
        // input request
        $ra             = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::waitingForSoApproval()->first()->id;
        $ra->save();
        // request action
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::uploadFile()->first()->id;
        $ract->save();
        // notification read
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 28;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Upload File berhasil.');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show1(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        $urlpath    = "request";
        $raction    = ITRequest::find($request->id)->requestActions;
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 2;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 3;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 6;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 4;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 9;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 11;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 16;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 30;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 5;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        // return view('requests.show',compact('request','urlpath'));
        $requestActions = $request->requestActions->map(function($value, $item){
            return RequestAction::with(['RequestActionNotes','users','action'])
                ->find($value->id); 
        });
        $requestSos = $request->requestSos->map(function($value, $item){
            return RequestSo::with(['requestSoBps','service'])
                ->find($value->id); 
        });
        return response()->json(collect([
            'request' => $request,
                [
                    $request->service,
                    $request->category,
                    $request->stage,
                    $request->user
                ],
            'requestActions' => $requestActions,
            'requestAttachments' => $request->requestAttachments,
            'requestSos' => $requestSos,
        ]), 200);
    }
    public function show(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        $urlpath    = "request";
        $raction    = ITRequest::find($request->id)->requestActions;
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 1;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 2;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 3;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 4;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 5;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 6;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 7;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification1 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 8;
        })->first();
        if(isset($notification1))
        {
            $notification1->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 10;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 9;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 11;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 12;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 13;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 14;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 15;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 16;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 17;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 18;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 19;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 20;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 21;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 22;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 23;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 24;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 25;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 26;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 27;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 28;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 29;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 31;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        $notification2 = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 30;
        })->first();
        if(isset($notification2))
        {
            $notification2->markAsRead();
        }
        return view('requests.show',compact('request','urlpath'));
       
    }
    public function noteshow(Request $r, ITRequest $request, $user=NULL)
    {
        $urlpath    = "request";
        return view('requests.shownote',compact('request','urlpath'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ITRequest $request)
    {
        $services = Service::all();
        $urlpath    = "request";
        return view('requests.edit', compact('request','services','urlpath'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, ITRequest $request)
    {
        if($r->hasFile('attachment')){
            Storage::delete($request->attachment);
            $request ->attachment   = $r->file('attachment')->store('attachments');
        }
        $request ->service_id       = $r->input('service_id');
        $request ->business_need    = $r->input('business_need');
        $request ->business_benefit = $r->input('business_benefit');
        $request ->user_id          = Auth::user()->id;
        $request ->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan berhasil diubah.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ITRequest $request)
    {
        $request->delete();
        Storage::delete($request->attachment);
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil dihapus.');
    }
    public function approvesave(Request $r, ITRequest $request)
    {
        if($r->input('aksi') == "1")
        {
            // update stage
            if($r->input('categories') == 2)
            {
                $request->stage_id          = Stage::waitingForOperationDesk()->first()->id;
                $request->service_id        = $r->input('service_id');
                $request->title             = $r->input('title');
                $request->business_need     = $r->input('business_need');
                $request->business_benefit  = $r->input('business_benefit');
                $request->category_id       = $r->input('categories');
                $request->keterangan        = $r->input('keterangan');
                $request->save();
            }
            elseif($r->input('categories') == 1)
            {
                if($r->has('so'))
                {
                    $roleso = $r->input('so');
                }
                else
                {
                    $roleso     = Service::find($request->service_id)->role_id;
                    $roleso1    = Role::find($roleso)->name;
                }
                //$request->stage_id          = Stage::waitingForSoApproval()->first()->id;
                $request->stage_id          = Stage::waitingForServiceDesk()->first()->id;
                $request->service_id        = $r->input('service_id');
                $request->title             = $r->input('title');
                $request->business_need     = $r->input('business_need');
                $request->business_benefit  = $r->input('business_benefit');
                $request->category_id       = $r->input('categories');
                $request->keterangan        = $r->input('keterangan');
                $request->save();
            }
            // save request approvals
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForOperationDesk()->first()->id;
            $ra->save();
            // insert action
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::approve()->first()->id;
            $ract->save();
            // insert action note
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            // read notifikasi
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 1;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            // redirect
            return redirect()
                ->route('approval.show','request')
                ->with('success','Permintaan layanan berhasil disetujui.');
        }
        elseif($r->input('aksi') == "2")
        {
            // update stage
            $request->stage_id = Stage::requestRejected()->first()->id;
            $request->save();
            
            // insert request approval
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::rejected()->first()->id;
            $ra->stage_id   = Stage::requestRejected()->first()->id;
            $ra->save();
            // insert action
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::reject()->first()->id;
            $ract->save();
            // insert action note
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            
            // read notification
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 1;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            // redirec
            return redirect()
                ->route('approval.show','request')
                ->with('success','Permintaan layanan berhasil ditolak.');
        }
    }
    public function soapprove(Request $r, ITRequest $request)
    {
        if($r->input('aksi') == "1")
        {
            // update stage
            // $request->stage_id = Stage::waitingForServiceDesk()->first()->id;
            $request->stage_id = Stage::waitingForOperationIct()->first()->id;
            $request->reason = $r->input('reason');
            $request->save();
            // save request approvals
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 10;
            })->first();
            $notification->markAsRead();
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::approve()->first()->id;
            $ract->save();
            return redirect()
                ->route('requests.index')
                ->with('success','Permintaan layanan berhasil disetujui.');
        }
        elseif($r->input('aksi') == "2")
        {
            $request->stage_id  = Stage::requestDenied()->first()->id;
            $request->reason    = $r->input('reason');
            $request->save();
    
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::rejected()->first()->id;
            $ra->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 10;
            })->first();
            $notification->markAsRead();
            // action 
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::reject()->first()->id;
            $ract->save();
    
            return redirect()
                ->route('requests.index')
                ->with('success','Permintaan layanan berhasil ditolak.');
        }
    }
    public function spsdapprove(Request $r, ITRequest $request)
    {
        //dd($r->input('service_id')." ".$r->input('service_idold'));
        if($r->input('aksi') == "1")
        {
            // update stage
            // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
            $request->stage_id = Stage::waitingForServiceDesk()->first()->id;
            $request->service_id = $r->input('service_id');
            $request->save();
            $rso = RequestSo::where('request_id',$request->id)->where('service_id', $r->input('service_idold'))->first();
            $rso->service_id = $r->input('service_id');
            $rso->save();
            // save request approvals
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForServiceDesk()->first()->id;
            $ra->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 2;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::approve()->first()->id;
            $ract->save();
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            return redirect()
                ->route('approval.show', 'request')
                ->with('success','Permintaan layanan berhasil disetujui.');
        }
        else
        {
            if($r->has('so'))
            {
                $roleso = $r->input('so');
            }
            else
            {
                $roleso = Service::find($request->service_id)->role_id;
            }
            $request->stage_id  = Stage::waitingForSoApproval()->first()->id;
            $request->so        = $roleso;
            $request->save();
            
            if(!empty($r->input('service_id')))
            {
                $rso = RequestSo::where('request_id',$request->id)
                    ->where('service_id', $r->input('service_idold'))->first();
                $rso->service_id = $r->input('service_id');
                $rso->save();
            }
            // save  request approval
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForSoApproval()->first()->id;
            $ra->save();
            // so action
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::escalation()->first()->id;
            $ract->save();
            // so note action
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            // read notification
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == ( $request->id and $item->data['stage_id'] == 2 or $request->id and $item->data['stage_id'] == 7);
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
    
            return redirect()
                ->route('approval.show', 'request')
                ->with('success','Permintaan layanan berhasil dieskalasi ke Service Owner.');
        }
    }
    public function managerbeictapprove(Request $r, ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        if($r->input('aksi') == "1")
        {
            // update stage
            // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
            // $request->stage_id = Stage::waitingForServiceDesk()->first()->id;
            $request->stage_id = Stage::waitingForOperationDesk()->first()->id;
            $request->save();
            // save request approvals
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForServiceDesk()->first()->id;
            $ra->save();
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::approve()->first()->id;
            $ract->save();
            if(!empty($r->input('note')))
            {
                // save note
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 12;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil disetujui.');
        }
        else
        {
            $request->stage_id = Stage::requestRejected()->first()->id;
            $request->save();
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id = Auth::user()->id;
            $ra->status_id = Status::rejected()->first()->id;
            $request->stage_id = Stage::requestRejected()->first()->id;
            $ra->save();
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::approve()->first()->id;
            $ract->save();
            if(!empty($r->input('note')))
            {
                // save note
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 12;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            return redirect()
                ->route('requests.index')
                ->with('success','Permintaan layanan berhasil ditolak.');
        }
    }
    public function chiefupdate(Request $r, ITRequest $request)
    {
        // update stage
        // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
        $request->stage_id = Stage::waitingForManagerBeict()->first()->id;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::waitingForManagerBeict()->first()->id;
        $ra->save();
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::approve()->first()->id;
        $ract->save();
        if(!empty($r->input('note')))
        {
            // save note
            $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
        }
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 19;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil disetujui.');
    }
    public function spsdcrfapprove(Request $r, ITRequest $request)
    {
        // update stage
        // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
        $request->stage_id = Stage::approved()->first()->id;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::approved()->first()->id;
        $ra->save();
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::approve()->first()->id;
        $ract->save();
        if(!empty($r->input('note')))
        {
            // save note
            $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
        }
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 2;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil disetujui.');
    }
    public function coordinatorapprove(Request $r, ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        // update stage
        // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
        if($request->crf_category == 3)
        {
            $data       = array_column(Auth::user()->roles()->get()->toArray(), 'id');
            $serv       = array_column(ServiceCoordinator::whereIn('role_id', $data)->get()->toArray(),'service_id');
            $rso        = RequestSo::where('request_id',$request->id)->whereIn('service_id', $serv)->get();
            foreach($rso as $item)
            {
                $item->status= 2;
                $item->save();
            }
            $rsocek     = RequestSo::where('request_id',$request->id)->where('status','1')->first();
            // dd($rsocek);
            if(!empty($rsocek))
            {
                $request->stage_id = Stage::waitingForCoordinatorSo()->first()->id;
                $request->save();
                // save request approvals
                $ra = new RequestApproval();
                $ra->request_id     = $request->id;
                $ra->user_id        = Auth::user()->id;
                $ra->status_id      = Status::approved()->first()->id;
                $ra->stage_id       = Stage::waitingForCoordinatorSo()->first()->id;
                $ra->save();
            }
            else
            {
                $request->stage_id  = Stage::waitingForChiefApproval()->first()->id;
                $request->save();
                // save request approvals
                $ra = new RequestApproval();
                $ra->request_id     = $request->id;
                $ra->user_id        = Auth::user()->id;
                $ra->status_id      = Status::approved()->first()->id;
                $ra->stage_id       = Stage::waitingForChiefApproval()->first()->id;
                $ra->save();
            }
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 13;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
        }
        else
        {
            $request->stage_id      = Stage::waitingForManagerBeict()->first()->id;
            $request->description   = $r->input('description');
            $request->save();
            // save request approvals
            $ra = new RequestApproval();
            $ra->request_id         = $request->id;
            $ra->user_id            = Auth::user()->id;
            $ra->status_id          = Status::approved()->first()->id;
            $ra->stage_id           = Stage::waitingForManagerBeict()->first()->id;
            $ra->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 13;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
        }
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::approve()->first()->id;
        $ract->save();
        if(!empty($r->input('note')))
        {
            $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil disetujui.');
    }
    
    public function waitingdocument(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        // update stage
        // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
        // $request->ticket    = $r->input('ticket');
        $request->stage_id  = Stage::waitingForDocument()->first()->id;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::waitingForDocument()->first()->id;
        $ra->save();
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::waitingDocument()->first()->id;
        $ract->save();
        if(!empty($r->input('note')))
        {
            $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
        }
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 4;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Menunggu Dokumen Rekomendasi');
    }
    public function trasportupdate(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        // update stage
        // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
        // $request->ticket    = $r->input('ticket');
        $request->stage_id  = Stage::Transport()->first()->id;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::Transport()->first()->id;
        $ra->save();
        // save action
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::confirmationTransport()->first()->id;
        $ract->save();
        // save action note
        if(!empty($r->input('note')))
        {
            $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
        }
        // read notifikasi
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 4;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        // redirect
        return redirect()
            ->route('requests.index')
            ->with('success','Konfirmasi berhasil dilakukan.');
    }
    
    public function ticketcrfupdate(Request $r, ITRequest $request)
    {
        // update stage
        // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
        $request->stage_id  = Stage::waitingForSoApproval()->first()->id;
        $request->ticket    = $r->input('ticket');
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id     = $request->id;
        $ra->user_id        = Auth::user()->id;
        $ra->status_id      = Status::approved()->first()->id;
        $ra->stage_id       = Stage::ticketCreated()->first()->id;
        $ra->save();
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 3;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::ticketInput()->first()->id;
        $ract->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Nomor ticket berhasil diinput.');
    }
    public function srfnotes(Request $r, ITRequest $request)
    {
        $ract = new RequestAction();
        $ract->request_id = $request->id;
        $ract->user = Auth::user()->id;
        $ract->action_type = Action::inputNote()->first()->id;
        $ract->save();
        if(!empty($r->input('note')))
        {
            // save note
            $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Note berhasil disimpan.');
    }
    public function detailcrfupdate(Request $r, ITRequest $request)
    {
       
        // update stage
        $request->stage_id  = Stage::waitingUserConf()->first()->id;
        $request->nda       = $r->input('nda');
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id     = $request->id;
        $ra->user_id        = Auth::user()->id;
        $ra->status_id      = Status::approved()->first()->id;
        $ra->stage_id       = Stage::waitingUserConf()->first()->id;
        $ra->save();
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 3;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $ract = new RequestAction();
        $ract->request_id = $request->id;
        $ract->user = Auth::user()->id;
        $ract->action_type = Action::detailInput()->first()->id;
        $ract->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil disetujui.');
    }
    public function trasportconfirm(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        // update stage
        $request->stage_id  = Stage::transportSuccess()->first()->id;
        $request->nda       = 3;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id = Auth::user()->id;
        $ra->status_id = Status::approved()->first()->id;
        //$ra->stage_id = Stage::transportSuccess()->first()->id;
        $ra->stage_id = Stage::waitingForInformationDetail()->first()->id;
        $ra->save();
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 18;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $ract = new RequestAction();
        $ract->request_id = $request->id;
        $ract->user = Auth::user()->id;
        $ract->action_type = Action::transport()->first()->id;
        $ract->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Transport berhasil dilakukan.');
    }
    public function ndaapprovecrf(Request $r, ITRequest $request)
    {
        // update stage
        $request->stage_id  = Stage::waitingForSoTransport()->first()->id;
        $request->nda       = 2;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id = Auth::user()->id;
        $ra->status_id = Status::approved()->first()->id;
        $ra->stage_id = Stage::waitingForSoTransport()->first()->id;
        $ra->save();
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 16;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $ract = new RequestAction();
        $ract->request_id = $request->id;
        $ract->user = Auth::user()->id;
        $ract->action_type = Action::ndaApprove()->first()->id;
        $ract->save();
        return redirect()
            ->route('requests.index')
            ->with('success','NDA Berhasil disetujui .');
    }
    public function arsip(Request $r, ITRequest $request, Service $service)
    {
        // update 
        $request->stage_id = Stage::arsip()->first()->id;
        $request->save();
        // save request approvals
        // $ra = new RequestApproval();
        // $ra->request_id = $request->id;
        // $ra->user_id    = Auth::user()->id;
        // $ra->status_id  = Status::approved()->first()->id;
        // $ra->stage_id   = Stage::arsip()->first()->id;
        // $ra->save();
       
        // save request action
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::arsip()->first()->id;
        $ract->save();
        
    
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil diarsipkan.');
    }
    public function unrelease(Request $r, ITRequest $request, Service $service)
    {
        // update 
        $request->stage_id = Stage::waitingForSoRelease()->first()->id;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::waitingForSoRelease()->first()->id;
        $ra->save();
       
        // save request action
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::unrelease()->first()->id;
        $ract->save();
        
        // read notification
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 15;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        // redirect
        return redirect()
            ->route('requests.index')
            ->with('success','Bps berhasil diunrelease.');
    }
    /*
        Release
    */
    public function release(Request $r, ITRequest $request, Service $service)
    {
        // update status 1
        $idrso          = RequestSo::where('request_id',$request->id)->first();
        $idrso->status  = 1;
        $idrso->save();
        // cek status = 0 di request_sos
        // $cek = RequestSo::where('request_id',0)->first();
        // if(isset($cek))
        // {
        //     // update stage
        //     $request->stage_id = Stage::waitingForSoApproval()->first()->id;
        //     $request->save();
        //     // save request approvals
        //     $ra = new RequestApproval();
        //     $ra->request_id = $request->id;
        //     $ra->user_id    = Auth::user()->id;
        //     $ra->status_id  = Status::approved()->first()->id;
        //     $ra->stage_id   = Stage::waitingForSoApproval()->first()->id;
        //     $ra->save();
        // }
        // else
        // {
        $request->stage_id = Stage::waitingForCoordinatorSo()->first()->id;
        $request->save();
        // save request approvals
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::waitingForCoordinatorSo()->first()->id;
        $ra->save();
        //}
        // save request action
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::bpsRelease()->first()->id;
        $ract->save();
        // read notification
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 10;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        // redirect
        return redirect()
            ->route('requests.index')
            ->with('success','Bps berhasil direlease.');
    }
    public function chooseso(Request $r, ITRequest $request)
    {
        if(count($r->input('so')) > 1)
        {
            // update request
            $serviceid = $r->input('serviceid');
            $idrso = RequestSo::where('service_id', $serviceid)->where('request_id',$request->id)->first();
            $idrso->status = 1;
            $idrso->save();
            // 
            foreach($r->input('so') as $item)
            {
                $servcoordinator1 = ServiceCoordinator::where('service_id', $request->service_id)->first()->role_id;
                $servcoordinator2 = ServiceCoordinator::where('service_id', $item)->first()->role_id;
                $crfcategorynow   = ITRequest::where('id', $request->id)->first()->crf_category;
                //dd($servcoordinator1);
                if($item != $request->service_id)
                {
                    if($servcoordinator1 == $servcoordinator2 and $crfcategorynow != 3)
                    {
                        // update stage
                        $request->stage_id      = Stage::waitingForBps()->first()->id;
                        $request->crf_category  =  2;
                        $request->save();
                    }
                    if($servcoordinator1 != $servcoordinator2)
                    {
                        $request->stage_id      = Stage::waitingForBps()->first()->id;
                        $request->crf_category  =  3;
                        $request->save();
                    }
                }
                if($item != $request->service_id)
                {
                    $cekso = RequestSo::where('service_id', $item)->where('request_id',$request->id)->first();
                    if(!isset($cekso))
                    {
                        $rso = new RequestSo();
                        $rso->request_id    = $request->id;
                        $rso->service_id    = $item;
                        $rso->status        = 0;
                        $rso->save();   
                    } 
                }
            }
            // save request approvals
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForBps()->first()->id;
            $ra->save();
        }
        else
        {
            // update stage
            $request->stage_id      = Stage::waitingForCoordinatorSo()->first()->id;
            $request->crf_category  = 1;
            $request->save();
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForCoordinatorSo()->first()->id;
            $ra->save();
            foreach($request->requestSos()->get() as $item)
            {
                $rsobps         = RequestSoBps::where('request_sos_id', $item->id)->first();
                $rsobps->status = 3;
                $rsobps->save();
            }
        }
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::chooseSo()->first()->id;
        $ract->save();
        
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 15;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        return redirect()
            ->route('requests.index')
            ->with('success','SO berhasil dipilih.');
    }
    public function formnotes(Request $r, ITRequest $request, $user = null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requests.formnote', compact('request','services','categories','roles','urlpath'));
    }
    public function detailcrfshow(Request $r, ITRequest $request, $user = null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requests.editdetailcrf', compact('request','services','categories','roles','urlpath'));
    }
    public function transportshow(Request $r, ITRequest $request)
    {
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requests.transport', compact('request','services','categories','roles','urlpath'));
    }
    public function spsdshow(Request $r, ITRequest $request, $user = null)
    {
        Auth::loginUsingId($user); 
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.escalation', compact('request','services','categories','roles','urlpath'));
    }
    public function coshow(Request $r, ITRequest $request, $user = null)
    {
        Auth::loginUsingId($user); 
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.co', compact('request','services','categories','roles','urlpath'));
    }
    public function uploaddocumentshow(Request $r, ITRequest $request, $user = null)
    {
        Auth::loginUsingId($user); 
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.upload', compact('request','services','categories','roles','urlpath'));
    }
    public function konfirmasitrshow(Request $r, ITRequest $request, $user = null)
    {
        Auth::loginUsingId($user); 
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.konfirmasitr', compact('request','services','categories','roles','urlpath'));
    }
    
    public function coordinatorshow(Request $r, ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.coordinator', compact('request','services','categories','roles','urlpath'));
    }
    public function waitingdocumentshow(Request $r, ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requests.waitingdocument', compact('request','services','categories','roles','urlpath'));
    }
    public function ndaapproveshow(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.ndacrf', compact('request','services','categories','roles','urlpath'));
    }
    public function choosesoshow(Request $r, ITRequest $request, Service $service)
    {
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $requestSos = RequestSo::with('request')
            ->where('service_id',$service->id)
        ->where('request_id',$request->id)->first();
        $urlpath    = "request";
        return view('requestapprovals.chooseso', compact('request','services','categories','roles','requestSos','urlpath'));
    }
    public function managerbeictshow(Request $r, ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $urlpath    = "request";
        return view('requestapprovals.manager', compact('request','services','categories','urlpath'));
    }
    public function chiefshow(Request $r, ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $urlpath    = "request";
        return view('requestapprovals.chief', compact('request','services','categories','urlpath'));
    }
    public function spsdcrfshow(Request $r, ITRequest $request)
    {
        $services   = Service::all();
        $categories = Category::all();
        $urlpath    = "request";
        return view('requestapprovals.spsd', compact('request','services','categories','urlpath'));
    }
    public function approveshow(ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $urlpath    = "request";
        return view('requestapprovals.edit', compact('request','services','categories','urlpath'));
    }
    public function spictshow(ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $urlpath    = "request";
        return view('requestapprovals.spict', compact('request','services','categories','urlpath'));
    }
    public function escalationshow(ITRequest $request)
    {
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.escalation', compact('request','services','categories','roles','urlpath'));
    }
    public function bpsshow(ITRequest $request, Service $service, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $requestSos = RequestSo::with('request')
            ->where('service_id',$service->id)
            ->where('request_id',$request->id)->first();
        $urlpath    = "request";
        return view('requestapprovals.bps', compact('request','services','categories','roles','requestSos','urlpath'));
    }
    public function srfrejectshow(ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.rejectsrf', compact('request','services','categories','roles','urlpath'));
    }
    public function srfapproveshow(ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.approvesrf', compact('request','services','categories','roles','urlpath'));
    }
    public function editrecomedation(ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requestapprovals.recomendation', compact('request','services','categories','roles','urlpath'));
    }
    public function editdetail(ITRequest $request)
    {
        $services   = Service::all();
        $categories = Category::all();
        $urlpath    = "request";
        return view('requests.editdetail', compact('request','services','categories','urlpath'));
    }
    public function editticket(ITRequest $request, $user=null)
    {
        Auth::loginUsingId($user);
        $services   = Service::all();
        $categories = Category::all();
        $urlpath    = "request";
        return view('requests.editticket', compact('request','services','categories','urlpath'));
    }
    public function showvalidasi(ITRequest $request)
    {
        $services   = Service::all();
        $urlpath    = "request";
        return view('requestapprovals.recomendation', compact('request','services','urlpath'));
    }
    public function editreject(ITRequest $request)
    {
        $services   = Service::all();
        $urlpath    = "request";
        return view('requestapprovals.editreject', compact('request','services','urlpath'));
    }
    
    public function ticketcrfshow(Request $r, ITRequest $request)
    {
        $services   = Service::all();
        $categories = Category::all();
        $roles      = Role::where('name','like','%so%')->get();
        $urlpath    = "request";
        return view('requests.editticketcrf', compact('request','services','categories','roles','urlpath'));
    }
    /*
     ** upload bps
    */
    public function updatebps(Request $r, ITRequest $request)
    {
        // get service id
        $serviceid          = $r->input('serviceid');
        // update rso
        $updateRso          = RequestSo::where('service_id', $serviceid)->where('request_id',$request->id)->first();
        $updateRso->status  = 1;
        $updateRso->save();
        // get id request so
        $idrso      = RequestSo::where('service_id', $serviceid)->where('request_id',$request->id)->first()->id;
        // cek status so
        $cekso      = RequestSo::where('request_id',$request->id)->where('status','0')->first();
        if(isset($cekso))
        {
            // update stage
            $request->stage_id  = Stage::waitingForSoApproval()->first()->id;
            $request->reason    = $r->input('reason');
            $request->save();
        }
        else
        {
            // update stage
            $request->stage_id  = Stage::waitingForSoRelease()->first()->id;
            $request->reason    = $r->input('reason');
            $request->save();
        }
        // upload file and save data upload
        foreach( $r->file('attachment') as $files)
        {
            $dateNow    = Carbon::now()->toDateTimeString();
            $date       = Carbon::parse($dateNow)->format('dmYHis');
            $name       = $files->getClientOriginalName();
            $username   = Auth::user()->id;
            $filename   = $date.$username.$name;
            
            // upload
            $item = $files->storeAs('attachments', $filename);
            // $rso->requestSoBps()->save(new RequestSoBps(['attachment' => $item, 'status' => '1']));
            // save data upload
            $rso = new requestSoBps();
            $rso->request_sos_id    = $idrso;
            $rso->attachment        = $item;
            $rso->name              = $filename;
            $rso->alias             = $name;
            $rso->status            = 1;
            $rso->save();
        }
        // save request approval
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::waitingForSoRelease()->first()->id;
        $ra->save();
        // save request action
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::uploadBps()->first()->id;
        $ract->save();
        // save note action
        // cek note available
        if(!empty($r->input('note')))
        {
            // save note
            $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
        }
        // read notification
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 10;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        
        // redirect
        return redirect()
            ->route('requests.index')
            ->with('success','Bps  berhasil diunggah.');
    }
    public function updaterecomendation(SoActionRequest $r, ITRequest $request)
    {
        if($r->input('document_status') == "l1")
        {
            // update tabel request
            $request->stage_id              = Stage::waitingForOperationIct()->first()->id;
            $request->reason                = $r->input('reason');
            $request->recomendation_status  = $r->input('recomendation_status');
            $request->save();
            // input file attachment
            if(!empty($r->file('attachment')))
            {
                foreach( $r->file('attachment') as $files)
                {
                    //storeAs
                    //getClientOriginalName();
                    $dateNow    = Carbon::now()->toDateTimeString();
                    $date       = Carbon::parse($dateNow)->format('dmYHis');
                    $name       = $files->getClientOriginalName();
                    $username   = Auth::user()->id;
                    $filename   = $date.$username.$name;
                    $rfiles = new RequestReasonfile();
                    $rfiles->attachment = $files->storeAs('attachments', $filename);
                    $rfiles->name       = $filename;
                    $rfiles->request_id = $request->id;
                    $rfiles->alias      = $name;
                    $rfiles->save();
                }
            }
            // input request
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForOperationIct()->first()->id;
            $ra->save();
            // cek recomendation or not recomendation
            if($r->input('recomendation_status') == 1)
            {
                $actioneText = Action::recomendation()->first()->id;
            }
            else
            {
                $actioneText = Action::notRecomendation()->first()->id;
            }
            // request action
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = $actioneText;
            $ract->save();
            // action notes
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('reason')]));
            }
            // notification read
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 10;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            return redirect()
                ->route('approval.show', 'request')
                ->with('success','Rekomendasi berhasil dikirim.');
        }
        elseif($r->input('document_status') == "l2")
        {
            // update stage
            // $request->stage_id = Stage::waitingForOperationIct()->first()->id;
            // $request->ticket    = $r->input('ticket');
            $request->stage_id  = Stage::waitingForDocument()->first()->id;
            $request->save();
            // save request approvals
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->stage_id   = Stage::waitingForDocument()->first()->id;
            $ra->save();
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::waitingDocument()->first()->id;
            $ract->save();
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 4;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            return redirect()
                ->route('approval.show', 'request')
                ->with('success','Menunggu Dokumen Rekomendasi');
        }
    }
    public function updatedetail(Request $r, ITRequest $request)
    {
        $request->stage_id  = Stage::resolve()->first()->id;
        $request->detail    = $r->input('detail');
        $request->save();
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        //$ra->stage_id   = Stage::waitingUserConf()->first()->id;
        $ra->stage_id   = Stage::resolve()->first()->id;
        $ra->save();
        $ract = new RequestAction();
        $ract->request_id = $request->id;
        $ract->user = Auth::user()->id;
        $ract->action_type = Action::detailInput()->first()->id;
        $ract->save();
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 4;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 17;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        
        return redirect()
            ->route('requests.index')
            ->with('success','Detil layanan berhasil di-input.');
    }
    public function updateticket(TicketInputITRequest $r, ITRequest $request)
    {
        $request->stage_id = Stage::ticketCreated()->first()->id;
        $request->ticket = $r->input('ticket');
        $request->save();
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::ticketCreated()->first()->id;
        $ra->save();
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == $request->id and $item->data['stage_id'] == 3;
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        $ract = new RequestAction();
        $ract->request_id = $request->id;
        $ract->user = Auth::user()->id;
        $ract->action_type = Action::ticketInput()->first()->id;
        $ract->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Nomor tiket kaseya berhasil di-input.');
    }
    public function managerreject(ITRequest $request)
    {
        $request->stage_id = Stage::requestRejected()->first()->id;
        $request->save();
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id = Auth::user()->id;
        $ra->status_id = Status::rejected()->first()->id;
        $request->stage_id = Stage::requestRejected()->first()->id;
        $ra->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil ditolak.');
    }
    public function soreject(ITRequest $request)
    {
        $request->stage_id = Stage::requestDenied()->first()->id;
        $request->save();
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id = Auth::user()->id;
        $ra->status_id = Status::rejected()->first()->id;
        $ra->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil ditolak.');
    }
    public function bossreject(Request $r, ITRequest $request)
    {
        $request->stage_id = Stage::requestDenied()->first()->id;
        $request->save();
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id = Auth::user()->id;
        $ra->status_id = Status::rejected()->first()->id;
        $ra->save();
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil ditolak.');
    }
    public function employeeapprove(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        if($r->input('aksi') == 1)
        {
            $request->stage_id = Stage::closed()->first()->id;
            $request->save();
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id = Auth::user()->id;
            $ra->status_id = Status::approved()->first()->id;
            $ra->stage_id = Stage::closed()->first()->id;
            $ra->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 6;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            $ract = new RequestAction();
            $ract->request_id = $request->id;
            $ract->user = Auth::user()->id;
            $ract->action_type = Action::approve()->first()->id;
            $ract->save();
            return redirect()
                ->route('requests.index')
                ->with('success','Permintaan layanan disetujui.');
        }
        elseif($r->input('aksi') == 2)
        {
            $request->stage_id = Stage::deniedByUser()->first()->id;
            $request->save();
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id = Auth::user()->id;
            $ra->status_id = Status::rejected()->first()->id;
            $ra->stage_id = Stage::deniedByUser()->first()->id;
            $ra->save();
            if(!empty($r->file('attachment')))
            {
                foreach($r->file('attachment') as $files)
                {
                    // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                    $dateNow    = Carbon::now()->toDateTimeString();
                    $date       = Carbon::parse($dateNow)->format('dmYHis');
                    $name       = $files->getClientOriginalName();
                    $username   = Auth::user()->id;
                    $filename   = $date.$username.$name;
                    // upload data
                    $item = $files->storeAs('attachments', $filename);
                    // input data file
                    $request->requestRejectattachments()->save(new RequestRejectattachment(['attachment' => $item, 'name' => $filename, 'alias' => $name]));
                }
            }
            $ract = new RequestAction();
            $ract->request_id = $request->id;
            $ract->user = Auth::user()->id;
            $ract->action_type = Action::reject()->first()->id;
            $ract->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 6;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            return redirect()
                ->route('requests.index')
                ->with('success','Permintaan layanan/akses berhasil dikonfirmasi.');
        }
    }
    public function spictapprove(Request $r, ITRequest $request)
    {
        if($r->input('aksi') == "1")
        {
            $request->stage_id = Stage::waitingForServiceDesk()->first()->id;
            $request->save();
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->stage_id   = Stage::waitingForServiceDesk()->first()->id;
            $ra->status_id  = Status::approved()->first()->id;
            $ra->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 7;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::approve()->first()->id;
            $ract->save();
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            return redirect()
                ->route('approval.show','request')
                ->with('success','Permintaan layanan berhasil disetujui.');
        }
        else
        {
            $request->stage_id = Stage::requestRejected()->first()->id;
            $request->save();
            $ra = new RequestApproval();
            $ra->request_id = $request->id;
            $ra->user_id    = Auth::user()->id;
            $ra->stage_id   = Stage::requestRejected()->first()->id;
            $ra->status_id  = Status::rejected()->first()->id;
            $ra->save();
            $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
                return $item->data['id'] == $request->id and $item->data['stage_id'] == 7;
            })->first();
            if(isset($notification))
            {
                $notification->markAsRead();
            }
            $ract = new RequestAction();
            $ract->request_id   = $request->id;
            $ract->user         = Auth::user()->id;
            $ract->action_type  = Action::reject()->first()->id;
            $ract->save();
            if(!empty($r->input('note')))
            {
                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $r->input('note')]));
            }
            return redirect()
                ->route('approval.show','request')
                ->with('success','Permintaan layanan berhasil ditolak.');
        }
    }
    public function eskalasiso(Request $r, ITRequest $request)
    {
        if($r->has('so'))
        {
            $roleso = $r->input('so');
        }
        else
        {
            $roleso = Service::find($request->service_id)->role_id;
        }
        // update stage
        $request->stage_id  = Stage::waitingForSoApproval()->first()->id;
        $request->so        = $roleso;
        $request->save();
        // save  request approval
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id    = Auth::user()->id;
        $ra->status_id  = Status::approved()->first()->id;
        $ra->stage_id   = Stage::waitingForSoApproval()->first()->id;
        $ra->save();
        // so action
        $ract = new RequestAction();
        $ract->request_id   = $request->id;
        $ract->user         = Auth::user()->id;
        $ract->action_type  = Action::escalation()->first()->id;
        $ract->save();
        // read notification
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == ( $request->id and $item->data['stage_id'] == 2 or $request->id and $item->data['stage_id'] == 7);
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil dieskalasi ke Service Owner.');
    }
    public function udatateeskalasiso(Request $r, ITRequest $request)
    {
        if($r->has('so'))
        {
            $roleso = $r->input('so');
        }
        else
        {
            $roleso = Service::find($request->service_id)->role_id;
        }
        $request->stage_id = Stage::waitingForSoApproval()->first()->id;
        $request->so = $roleso;
        $request->save();
        // save  request approval
        $ra = new RequestApproval();
        $ra->request_id = $request->id;
        $ra->user_id = Auth::user()->id;
        $ra->status_id = Status::approved()->first()->id;
        $ra->stage_id = Stage::waitingForSoApproval()->first()->id;
        $ra->save();
        $ract = new RequestAction();
        $ract->request_id = $request->id;
        $ract->user = Auth::user()->id;
        $ract->action_type = Action::escalation()->first()->id;
        $ract->save();
        // read notification
        $notification = Auth::user()->notifications->filter(function($item, $key) use($request){
            return $item->data['id'] == ( $request->id and $item->data['stage_id'] == 2 or $request->id and $item->data['stage_id'] == 7);
        })->first();
        if(isset($notification))
        {
            $notification->markAsRead();
        }
        return redirect()
            ->route('requests.index')
            ->with('success','Permintaan layanan berhasil dieskalasi ke Service Owner.');
    }
    
}