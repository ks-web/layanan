<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Kaseya\Client;
use GuzzleHttp\Psr7\Request as Request;
use function GuzzleHttp\json_decode;
use App\User;
use Illuminate\Routing\Route;

class ITRequest extends Model
{
    protected $table = 'requests';

    public function requestActions()
    {
        return $this->hasMany('App\RequestAction', 'request_id')->orderBy('id', 'DESC');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function serviceSos()
    {
        return $this->belongsToMany('App\Service', 'request_sos', 'request_id', 'service_id')
            ->using('App\RequestSo');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }

    public function severity()
    {
        return $this->belongsTo('App\Severity');
    }

    public function priority()
    {
        return $this->belongsTo('App\Priority');
    }

    public function requestApprovals()
    {
        return $this->hasMany('App\RequestApproval', 'request_id');
    }

    public function requestRejectattachments()
    {
        return $this->hasMany('App\RequestRejectattachment', 'request_id');
    }

    public function requestReasonfiles()
    {
        return $this->hasMany('App\RequestReasonfile', 'request_id');
    }

    public function requestAttachments()
    {
        return $this->hasMany('App\RequestAttachment', 'request_id');
    }

    public function requestBps()
    {
        return $this->hasMany('App\RequestBps', 'request_id');
    }

    public function requestSos()
    {
        return $this->hasMany('App\RequestSo', 'request_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function scopeOfLoggedUser($query)
    {
        $query->where('user_id', Auth::user()->id);
    }

    public function scopeOfBossSubordinates($query)
    {
        $query->whereIn('user_id', Auth::user()->getPersonnelNoSubordinates());
    }

    public function scopeSecrOfBossSubordinates($query)
    {
        $query->whereIn('user_id', Auth::user()->getBossSubordinates());
    }

    public function requestChange($stage = NULL)
    {
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/87132147890095585806341772/tickets?$top=50&$filter=Stage eq \''.$stage.'\'';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        //$responsedata   = $response->getBody(); 
        return $responsedata["Result"];
    }

    public function closeTicket($ticketId = NULL, $statusId = NULL)
    {
        //automation/servicedesktickets/{ticketId}/status/{statusId}
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'PUT';
        $actionUri  = '/automation/servicedesktickets/'.$ticketId.'/status/'.$statusId;
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        return "Tiket Close";
        //$responsedata   = $response->getBody(); 
    }

    public function showTiket($reference = NULL)
    {
        // 
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/87132147890095585806341772/tickets?$top=50&$filter=TicketRef eq \''.$reference.'\'';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true); 
        return $responsedata["Result"];
    }

    public function requestStatus($status = NULL)
    {
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/87132147890095585806341772/status?$filter=StatusName eq \''.$status.'\'';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        return $responsedata["Result"];
    }

    public function requestMasterStatus()
    {
        // CH003300
        // automation/servicedesks/{serviceDeskId}/categories
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/87132147890095585806341772/status';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response   = $client->execute($request);
        echo $response->getBody();
    }
}
