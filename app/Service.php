<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;


class Service extends Model
{
    use HasRoles;

    protected $fillable = ['name'];

    public function requests()
    {
        return $this->hasMany('App\ITRequest');
    }

    public function requestSoss()
    {
        return $this->hasMany('App\RequestSo');
    }
    
    public function serviceCoordinator()
    {
        return $this->belongsTo('App\ServiceCoordinator');
    }

    public function requestChanges()
    {
        return $this->hasMany('App\RequestChange');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function requestSos()
    {
        return $this->belongsToMany('App\ITRequest', 'request_sos', 'service_id', 'request_id')
            ->using('App\RequestSo');
    }

    public function requestChangeBps()
    {
        return $this->hasMany('App\RequestChangeBps');
    }

    public function scopeSrf($query)
    {   
        $query->where('id',2);
    }

    public function scopeCrf($query)
    {
        $query->where('id', 1);
    }
}
