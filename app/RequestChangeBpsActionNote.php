<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChangeBpsActionNote extends Model
{
    protected $fillable = ['note'];

    public function requestChangeBpsAction()
    {
        return $this->belongsTo('App\RequestChangeBpsAction');
    }
}
