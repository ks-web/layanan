<?php

namespace App\Observers;

use App\RequestChange;
use App\Status;
use App\User;
use App\Stage;
use App\Action;
use App\RequestChangeAction;
use App\Service;
use App\ServiceCoordinator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\RequestChangeCreated;
use Spatie\Permission\Models\Role;

class RequestChangeActionObserver
{
    /**
     * Handle the request "created" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function created(RequestChangeAction $rchangeaction)
    {  
        if($rchangeaction->requestChange->stage_id == Stage::waitingBossApproval()->first()->id)
        {
            $boss   = Auth::user()->boss();
            $boss->notify(new RequestChangeCreated($rchangeaction));
        }
        elseif($rchangeaction->requestChange->stage_id == Stage::waitingForOperationDesk()->first()->id)
        {
            // notifikasi operation sd
            $role       = Role::findByName('operation sd');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($rchangeaction) {
                $item->notify(new RequestChangeCreated($rchangeaction));
            });

            $user1  = User::find($rchangeaction->requestChange->user_id); 
            $user1->notify(new RequestChangeCreated($rchangeaction));
        }
        elseif($rchangeaction->requestChange->stage_id == Stage::requestRejected()->first()->id)
        {
            // notifikasi operation 
            // notification to user
            $user1  = User::find($rchangeaction->requestChange->user_id); 
            $user1->notify(new RequestChangeCreated($rchangeaction));

            // notification to big boss
            $user       = new User();
            $boss_id    = $user->setNikBoss($rchangeaction->requestChange->user_id);
            $boss       = User::find($boss_id);
            $boss->notify(new RequestChangeCreated($rchangeaction));
        }
        // elseif($rchangeaction->requestChange->stage_id == Stage::ticketCreated()->first()->id)
        // {
        //     // notifikasi so
        //     // dd($rchangeaction->requestChange->requestChangeBps);
        //     $service_so = $rchangeaction->requestChange;
        //     $roleid     = Service::where('id',$service_so->service_id)->first();
        //     $roles      = Role::where('id', $roleid->role_id)->first();
        //     $role       = Role::findByName($roles->name);
            
        //     $collection = $role->users;
        //     $collection->each(function ($item, $key) use ($rchangeaction) {
        //         $item->notify(new RequestChangeCreated($rchangeaction));
        //     });
        // }
        elseif($rchangeaction->requestChange->stage_id == Stage::waitingForSoApproval()->first()->id)
        {
            $roleid     = Service::where('id',$rchangeaction->requestChange->service_id)->first();
            $roles      = Role::where('id', $roleid->role_id)->get();
            foreach($roles as $role)
            {
                $role       = Role::findByName($role->name);
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rchangeaction) {
                    $item->notify(new RequestChangeCreated($rchangeaction));
                });
            }
        }
        elseif($rchangeaction->requestChange->stage_id == Stage::waitingForServiceDesk()->first()->id)
        {
            // service desk
            // dd($rchangeaction->requeastChange->requestChangeBps);
            $role       = Role::findByName('service desk');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($rchangeaction) {
                $item->notify(new RequestChangeCreated($rchangeaction));
            });
        }
        elseif($rchangeaction->requestChange->stage_id == Stage::waitingForManagerBeict()->first()->id)
        {
            // manager beict
            // dd($rchangeaction->requeastChange->requestChangeBps);
            $role       = Role::findByName('manager beict');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($rchangeaction) {
                $item->notify(new RequestChangeCreated($rchangeaction));
            });

            // notifikasi 
            $service_so = $rchangeaction->requestChange;
            $servid     = $service_so->service_id;
            $serco      = ServiceCoordinator::where('service_id', $servid)->first();
            $role       = Role::where('id', $serco->role_id)->first();
            $role       = Role::findByName($role->name);
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($rchangeaction) {
                $item->notify(new RequestChangeCreated($rchangeaction));
            });
        }
        elseif($rchangeaction->requestChange->stage_id == Stage::waitingForBps()->first()->id)
        {
            $roleid     = Service::where('id',$rchangeaction->requestChange->service_id)->first();
            $roles      = Role::where('id', $roleid->role_id)->get();
            foreach($roles as $role)
            {
                $role       = Role::findByName($role->name);
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rchangeaction) {
                    $item->notify(new RequestChangeCreated($rchangeaction));
                });
            }
        }
       
    }

    /**
     * Handle the request "updated" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function updated(RequestChange $requestchange, Request $request)
    {
       
    }

    /**
     * Handle the request "deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function deleted(ITRequest $request)
    {
        //
    }

    /**
     * Handle the request "restored" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function restored(ITRequest $request)
    {
        //
    }

    /**
     * Handle the request "force deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function forceDeleted(ITRequest $request)
    {
        //
    }
}
