<?php

namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Auth;
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    public $nikBoss;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function requests()
    {
        return $this->hasMany('App\ITRequest');
    }

    public function requestChanges()
    {
        return $this->hasMany('App\RequestChange');
    }

    public function requestActions()
    {
        return $this->hasMany('App\RequestAction');
    }

    public function incidents()
    {
        return $this->hasMany('App\Incident');
    }

    public function RequestChangeActions()
    {
        return $this->hasMany('App\RequestChangeAction');
    }

    public function RequestChangeBpsActions()
    {
        return $this->hasMany('App\RequestChangeBpsAction');
    }

    public function requestApprovals()
    {
        return $this->hasMany('App\RequestApproval', 'user_id');
    }
    
    public function getIdWithNameAttribute()
    {
        return $this->id .' '. $this->name;
    }

    public function getId()
    {
        if((Auth::id() !== null))
        {
            return Auth::id();
        }
    }

    public function getPersonnelNoSubordinates()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        
        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $this->id
            . "/clossestSubordinates"
            , false
            , stream_context_create($arrContextOptions)
        );

        // dd($json);

        $data = json_decode($json, true);
        $datanik = array_column($data,'personnel_no');
        return $datanik;   
    }

    public function boss()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $this->id 
            . "/minManagerBoss"
            , false
            , stream_context_create($arrContextOptions)
        );

        $data = json_decode($json,true);

       if(sizeof($data) != 0)
        {
            $var =  User::find($data['personnel_no']);
            return $var;
        }
        elseif(sizeof($data) == 0){
            return '';
        }
        else
        {
            $json = file_get_contents(
                "https://portal.krakatausteel.com/eos/api/structdisp/"
                . $this->id 
                . "/minManagerBossWithDelegation"
                , false
                , stream_context_create($arrContextOptions)
            );

            $data1 = json_decode($json,true);
            return User::find($data1['personnel_no']);
        }

        
    }

    public function setNikBoss($id)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/".$id."/minManagerBoss"
            , false
            , stream_context_create($arrContextOptions)
        );
        
        $data = json_decode($json,true);
        if(!is_null($data))
        {
            
            if(isset($data['personnel_no'])) {
                return $data['personnel_no'];
            }
            else {
                return "Data atasan anda tidak ditemukan di structur display";
                exit;
            };
        }
    }

    public function setNikDelegationBoss($id)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/".$id."/minManagerBossWithDelegation"
            , false
            , stream_context_create($arrContextOptions)
        );
        
        $data = json_decode($json,true);
        if(!is_null($data))
        {
            
            if(isset($data['personnel_no'])) {
                return $data['personnel_no'];
            }
            else {
                return "Data atasan anda tidak ditemukan di structur display";
                exit;
            };
        }
    }

    public function saveEmployee($personelno = NULL, $email = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno
            , false
            , stream_context_create($arrContextOptions)
        );
        $data = json_decode($json,true);

        $user           = new User();
        $user->name     = $data['name'];
        $user->id       = $data['personnel_no'];
        $user->email    = $email;
        $user->password = Hash::make(str_random(32));
        $user->save();

    }

    public function saveMinManager($personelno = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno ."/minManagerBoss"
            , false
            , stream_context_create($arrContextOptions)
        );

        $data = json_decode($json,true);

        $jsonemail = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/personalEmail/"
            . $data['personnel_no']
            , false
            , stream_context_create($arrContextOptions)
        );

        $dataemail = json_decode($jsonemail,true);

        $user           = new User();
        $user->name     = $data['name'];
        $user->id       = $data['personnel_no'];
        $user->email    = $dataemail[0]['email'];
        $user->password = Hash::make(str_random(32));
        $user->save();  
    }

    public function saveMinManagerDelegation($personelno = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno ."/minManagerBossWithDelegation"
            , false
            , stream_context_create($arrContextOptions)
        );

        $data = json_decode($json,true);

        $jsonemail = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/personalEmail/"
            . $data['personnel_no']
            , false
            , stream_context_create($arrContextOptions)
        );

        $dataemail = json_decode($jsonemail,true);

        $user           = new User();
        $user->name     = $data['name'];
        $user->id       = $data['personnel_no'];
        $user->email    = $dataemail[0]['email'];
        $user->password = Hash::make(str_random(32));
        $user->save();  
    }
    

    public function saveUserSecr($personelno = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno
            , false
            , stream_context_create($arrContextOptions)
        );

        $data = json_decode($json,true);

        $jsonemail = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/personalEmail/"
            . $data['personnel_no']
            , false
            , stream_context_create($arrContextOptions)
        );

        $dataemail = json_decode($jsonemail,true);

        $user           = new User();
        $user->name     = $data['name'];
        $user->id       = $data['personnel_no'];
        $user->email    = $dataemail[0]['email'];
        $user->password = Hash::make(str_random(32));
        $user->save();  
    }

    public function personelNoChek($personelno = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno
            , false
            , stream_context_create($arrContextOptions)
        );
        
        $data = json_decode($json,true);
        if($data)
        {
            return $data['personnel_no'];
        }
    }

    public function employeeCheck($personelno = NULL)
    {
        try 
        {
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            @$json = file_get_contents(
                "https://portal.krakatausteel.com/eos/api/structdisp/"
                . urlencode($personelno)
                , false
                , stream_context_create($arrContextOptions)
            );
            
            if($json ==  false)
            {
                return [];
            }
            else 
            {
                $data = json_decode($json,true);
                return $data;
            }
        } 
        catch(Exception $ex) {
            return $ex;
        }
    }

    public function minManangerCheck($personelno = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        @$json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno
            . "/minManagerBoss"
            , false
            , stream_context_create($arrContextOptions)
        );
        
        $data = json_decode($json,true);

        if($json ==  false)
        {
            return [];
        }
        else 
        {
            $data = json_decode($json,true);
            return $data;
        }
    }

    public function minManangerDelegationCheck($personelno = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        @$json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno
            . "/minManagerBossWithDelegation"
            , false
            , stream_context_create($arrContextOptions)
        );
        
        $data = json_decode($json,true);

        if($json ==  false)
        {
            return [];
        }
        else 
        {
            $data = json_decode($json,true);
            return $data;
        }
    
    }

    public function esCheck($personelno = NULL)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = file_get_contents(
            "https://portal.krakatausteel.com/eos/api/structdisp/"
            . $personelno
            , false
            , stream_context_create($arrContextOptions)
        );

        
        $data = json_decode($json,true);
        return substr($data['esgrp'],0,1);
    }
}
