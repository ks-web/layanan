<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function requestChangeActions()
    {
        return $this->hasMany('App\RequestChangeAction');
    }

    public function scopeRoleEmployee($query)
    {
        $query->where('id', 1);
    }

    public function scopeRoleServicedesk($query)
    {
        $query->where('id', 2);
    }

    public function scopeRoleBoss($query)
    {
        $query->where('id', 3);
    }

    public function scopeRoleOperationDesk($query)
    {
        $query->where('id', 4);
    }

    public function scopeRoleManagerBeict($query)
    {
        $query->where('id', 17);
    }
}
