<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RequestChange extends Model
{
    protected $fillable = ['service_id','title','business_need','business_benefit','ticket','telp','location','user_id','stage_id','action_id','boss'];

    // relation with RequestChangeAttachments
    public function requestChangeAttachments()
    {
        return $this->hasMany('App\RequestChangeAttachment', 'request_change_id');
    }

    public function RequestChangeRejectAttachments()
    {
        return $this->hasMany('App\RequestChangeRejectAttachment', 'request_change_id');
    }

    public function requestChangeBps()
    {
        return $this->hasOne('App\RequestChangeBps', 'request_change_id');
    }

    public function requestChangeActions()
    {
        return $this->hasMany('App\RequestChangeAction');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }

    public function action()
    {
        return $this->belongsTo('App\Action');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeOfLoggedUser($query)
    {
        $query->where('user_id', Auth::user()->id);
    }

    public function scopeOfBossSubordinates($query)
    {
        $query->whereIn('user_id', Auth::user()->getPersonnelNoSubordinates());
    }
    
    public function scopeSecrOfBossSubordinates($query)
    {
        $query->whereIn('user_id', Auth::user()->getBossSubordinates());
    }


}
