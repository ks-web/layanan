<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::resource('services', 'ServiceController');
    Route::resource('statuses', 'StatusController');
    Route::resource('stages', 'StageController');
    Route::resource('requests', 'RequestController');
    Route::resource('requestApprovals', 'RequestApprovalController');
    Route::resource('incidents', 'IncidentController');
    Route::resource('incidentApprovals', 'IncidentApprovalController');
    Route::put('/incident/{incident}/detailsave', 'IncidentController@detailsave')->name('incidents.detailsave');
    Route::get('/incident/{incident}/detailshow', 'IncidentController@detailshow')->name('incidents.detailshow');
    Route::put('/incident/{incident}/approve/show', 'IncidentController@approveshow')->name('incidents.approveshow');
    Route::put('/incident/{incident}/reject/show', 'IncidentController@rejectshow')->name('incidents.rejectshow');
    Route::get('/incident/{incident}/reject/form', 'IncidentController@rejectform')->name('incidents.rejectform');
    Route::get('/incident/{incident}/ticket/show', 'IncidentController@ticketshow')->name('incidents.ticketshow');
    Route::put('/incident/{incident}/ticket/create', 'IncidentController@ticketcreated')->name('incidents.ticketcreated');
    Route::get('/incident/{incident}/show', 'IncidentController@show')->name('incidents.show');
    Route::get('/incident/show/{incident}', 'IncidentController@show')->name('incidents.show1');
    Route::get('/incident/stage/{incident}', 'IncidentController@showperstage')->name('incidents.showperstage');
    Route::put('/requests/{request}/approvesave', 'RequestController@approvesave')->name('requests.approvesave');
    Route::get('/requests/{request}/approveshow', 'RequestController@approveshow')->name('requests.approveshow');
    Route::put('/requests/{request}/soapprove', 'RequestController@soapprove')->name('requests.soapprove');
    Route::get('/requests/{request}/edit/detail', 'RequestController@editdetail')->name('requests.editdetail');
    Route::put('/requests/{request}/updatedetail', 'RequestController@updatedetail')->name('requests.updatedetail');
    Route::get('/requests/{request}/edit/reject', 'RequestController@editreject')->name('requests.editreject');
    Route::put('/requests/{request}/so/update/reject', 'RequestController@soreject')->name('requests.soreject');
    Route::put('/requests/{request}/boss/update/reject', 'RequestController@bossreject')->name('requests.bossreject');
    Route::put('/requests/{request}/spict/approve', 'RequestController@spictapprove')->name('requests.spictapprove');
    Route::get('/requests/{request}/spict/show', 'RequestController@spictshow')->name('requests.spictshow');
    Route::put('/requests/{request}/spsd/approve', 'RequestController@spsdapprove')->name('requests.spsdapprove');
    Route::get('/requests/{request}/spsd/show', 'RequestController@spsdshow')->name('requests.spsdshow');
    Route::put('/requests/{request}/spsd/crfapprove', 'RequestController@spsdcrfapprove')->name('requests.spsdcrfapprove');
    Route::get('/requests/{request}/spsd/crfshow', 'RequestController@spsdcrfshow')->name('requests.spsdcrfshow');
    Route::get('/requests/{request}/esklasi/so', 'RequestController@eskalasiso')->name('requests.eskalasiso');
    Route::put('/requests/{request}/esklasi/so/update', 'RequestController@udatateeskalasiso')->name('requests.udatateeskalasiso');
    Route::put('/requests/{request}/update/ticket', 'RequestController@updateticket')->name('requests.updateticket');
    Route::get('/requests/{request}/edit/ticket', 'RequestController@editticket')->name('requests.editticket');
    Route::get('/requests/{request}/validasi/show', 'RequestController@showvalidasi')->name('requests.showvalidasi');
    Route::get('/requests/{request}/eskalasi/show', 'RequestController@escalationshow')->name('requests.showeskalasi');
    Route::get('/requests/{request}/form/rekomendasi', 'RequestController@editrecomedation')->name('requests.editrecomedation');
    Route::put('/requests/{request}/update/rekomendasi', 'RequestController@updaterecomendation')->name('requests.updaterecomendation');
    Route::get('/requests/{request}/show', 'RequestController@show')->name('requests.show');
    Route::get('/requests/show/{request}', 'RequestController@show1')->name('requests.show1');
    Route::get('/requests/{request}/{service}/form/bps', 'RequestController@bpsshow')->name('requests.bpsshow');
    Route::put('/requests/{request}/update/bps', 'RequestController@updatebps')->name('requests.updatebps');
    Route::get('/requests/{request}/{service}/choose/so/show', 'RequestController@choosesoshow')->name('requests.choosesoshow');
    Route::put('/requests/{request}/choose/so/create', 'RequestController@chooseso')->name('requests.chooseso');
    Route::put('/requests/{request}/{service}/release/bps', 'RequestController@release')->name('requests.release');
    Route::put('/requests/{request}/{service}/unrelease/bps', 'RequestController@unrelease')->name('requests.unrelease');
    Route::put('/requests/{request}/coo/approve', 'RequestController@coordinatorapprove')->name('requests.coordinatorapprove');
    Route::get('/requests/{request}/coo/show', 'RequestController@coordinatorshow')->name('requests.coordinatorshow');
    Route::get('/requests/{request}/co/show', 'RequestController@coshow')->name('requests.coshow');
    Route::put('/requests/{request}/manager/approve', 'RequestController@managerbeictapprove')->name('requests.managerbeictapprove');
    Route::get('/requests/{request}/manager/show', 'RequestController@managerbeictshow')->name('requests.managerbeictshow');
    Route::get('/requests/{request}/input/ticket/crf', 'RequestController@ticketcrfshow')->name('requests.ticketcrfshow');
    Route::put('/requests/{request}/update/ticket/crf', 'RequestController@ticketcrfupdate')->name('requests.ticketcrfupdate');
    Route::get('/requests/{request}/input/transport/crf', 'RequestController@transportshow')->name('requests.transportshow');
    Route::get('/requests/{request}/input/konfirmasi/transport', 'RequestController@konfirmasitrshow')->name('requests.konfirmasitrshow');
    Route::put('/requests/{request}/update/transport/crf', 'RequestController@trasportupdate')->name('requests.trasportupdate');
    Route::get('/requests/{request}/nda/crf/show', 'RequestController@ndaapproveshow')->name('requests.ndaapproveshow');
    Route::put('/requests/{request}/nda/crf/approve', 'RequestController@ndaapprovecrf')->name('requests.ndaapprovecrf');
    Route::get('/requests/{request}/detail/crf/show/', 'RequestController@detailcrfshow')->name('requests.detailcrfshow');
    Route::get('/requests/{request}/detail/crf/update', 'RequestController@detailcrfupdate')->name('requests.detailcrfupdate');
    Route::get('/requests/{request}/transport/crf/update', 'RequestController@trasportconfirm')->name('requests.trasportconfirm');
    Route::put('/requests/{request}/manager/reject', 'RequestController@managerreject')->name('requests.managerreject');
    Route::put('/requests/{request}/chief/update', 'RequestController@chiefupdate')->name('requests.chiefupdate');
    Route::get('/requests/{request}/chief/show', 'RequestController@chiefshow')->name('requests.chiefshow');
    Route::get('/requests/{request}/so/transport', 'RequestController@transport')->name('requests.transport');
    Route::put('/requests/{request}/employee/approve', 'RequestController@employeeapprove')->name('requests.employeeapprove');
    Route::put('/requests/{request}/waiting/document', 'RequestController@waitingdocument')->name('requests.waitingdocument');
    Route::get('/requests/{request}/waiting/document/show', 'RequestController@waitingdocumentshow')->name('requests.waitingdocumentshow');
    Route::put('/requests/{request}/upload/document', 'RequestController@uploaddocument')->name('requests.uploaddocument');
    Route::get('/requests/{request}/upload/document/show', 'RequestController@uploaddocumentshow')->name('requests.uploaddocumentshow');
    Route::get('/requests/stage/{stage}', 'RequestController@viewperstage')->name('requests.viewperstage');
    Route::get('/requests/all/{stage}', 'RequestController@viewnotin')->name('requests.viewnotin');
    Route::get('/requests/{request}/srf/reject/show', 'RequestController@srfrejectshow')->name('requests.srfrejectshow');
    Route::get('/requests/{request}/srf/approve/show', 'RequestController@srfapproveshow')->name('requests.srfapproveshow');
    Route::put('/requests/{request}/srf/reject', 'RequestController@srfreject')->name('requests.srfreject');
    Route::put('/requests/{request}/srf/notes', 'RequestController@srfnotes')->name('requests.srfnotes');
    Route::get('/requests/{request}/srf/notes/form', 'RequestController@formnotes')->name('requests.formnotes');
    Route::put('/requests/{request}/arsip', 'RequestController@arsip')->name('requests.arsip');
    Route::put('/incident/{incident}/arsip', 'IncidentController@arsip')->name('incidents.arsip');
    Route::get('/incident/{incident}/note/show', 'IncidentController@noteshow')->name('incidents.noteshow');
    Route::get('/requests/{request}/note/show', 'RequestController@noteshow')->name('requests.noteshow');
    Route::post('/incident/fail', 'IncidentController@fail')->name('incidents.fail');
    Route::post('/incident/escalation', 'IncidentController@escalation')->name('incidents.escalation');
    Route::get('/', 'HomeController@index')->name('home.index');
    Route::get('/dasboard', 'HomeController@dasboard')->name('dasboard');
    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('/employe', 'HomeController@indexemployee')->name('homeemployee');

    // approvals
    Route::resource('/approval', 'ApprovalController');

    Route::group(['prefix' => 'bps'], function(){
        Route::get('','RequestChangeBpsController@index')
            ->name('bps.index');
        Route::get('/show/bps/{request_change_id}','RequestChangeBpsController@showBps')
            ->name('crf.show.bps');
        Route::get('/view/bps/{request_change_id}','RequestChangeBpsController@viewBps')
            ->name('crf.view.bps');
        
        // so route
        Route::get('/{request_change_id}/so/form/bps', 'RequestChangeBpsController@SoFormBps')
            ->name('crf.so.form.bps');
        Route::put('/{request_change_id}/so/create/bps', 'RequestChangeBpsController@soCreateBps')
            ->name('crf.so.create.bps');
        Route::get('/{request_change_id}/so/edit/bps', 'RequestChangeBpsController@SoEditBps')
            ->name('crf.so.edit.bps');
        Route::put('/{request_change_id}/so/edit/bps', 'RequestChangeBpsController@SoUpdateBps')
            ->name('crf.so.edit.bps');
            
         // coordinaator route
        Route::put('/{request_change_bps_id}/co/approval/ticket','RequestChangeBpsController@coApprovalBps')
            ->name('bps.co.approval');
        Route::get('/{request_change_bps_id}/co/approval/ticket','RequestChangeBpsController@coFormApprovalBps')
            ->name('bps.co.form.approval');

        // manager beict route
        Route::put('/{request_change_bps_id}/manager/approval/ticket','RequestChangeBpsController@managerApprovalBps')
          ->name('bps.manager.approval');
        Route::get('/{request_change_bps_id}/manger/approval/ticket','RequestChangeBpsController@managerFormApprovalBps')
          ->name('bps.manager.form.approval');
    });

    // routes request change (CRF)
    Route::group(['prefix' => 'crf'], function () {
        // employee all
        Route::get('', 'RequestChangeController@index')
            ->name('crf.index');
        Route::get('/create', 'RequestChangeController@create')
            ->name('crf.create');
        Route::post('', 'RequestChangeController@store')
            ->name('crf.store');
        Route::get('/show/crf/{request_change_id}','RequestChangeController@showCrf')
            ->name('crf.show.crf');
        Route::get('/show/crf/{request_change_id}/detail','RequestChangeController@showDetailCrf')
            ->name('crf.detail.show.crf');
        Route::get('/{id}/view','RequestChangeController@viewPerstage')
            ->name('crf.view.perstage');
        
        // pdf route
        Route::get('/show/crf/{request_change_id}/pdf','RequestChangeController@createPdf')
            ->name('crf.pdf.show.crf');

        // boss route
        Route::get('/{request_change_id}/boss/form/approval', 'RequestChangeController@FormBossApproval')
            ->name('crf.form.boss.approval');
        Route::put('/{request_change_id}/boss/approval', 'RequestChangeController@BossApproval')
            ->name('crf.boss.approval');

        // spsd route
        Route::get('/{request_change_id}/spsd/form/escalation', 'RequestChangeController@SpsdFormEscalation')
            ->name('crf.form.spsd.escalation');
        Route::put('/{request_change_id}/spsd/escalation', 'RequestChangeController@SpsdEscalation')
            ->name('crf.spsd.escalation');

        // service owner route
        Route::put('/{request_change_id}/so/approval', 'RequestChangeController@soApproval')
            ->name('crf.so.approval');
        Route::get('/{request_change_id}/so/form/approval', 'RequestChangeController@soFormApprovalCrf')
            ->name('crf.so.form');

        // service desk route
        Route::get('/{request_change_id}/servicdesk/form/ticket','RequestChangeController@servicedeskFormTicket')
            ->name('crf.servicedesk.ticket');
        Route::put('/{request_change_bps_id}/servicdesk/ticket','RequestChangeController@servicedeskInputTicket')
            ->name('crf.ticket');

        // manager beict
        Route::get('/{request_change_bps_id}/beict/form/approval','RequestChangeController@beictFormApproval')
            ->name('crf.form.approval');
        Route::put('/{request_change_id}/beict/approval','RequestChangeController@beictCrfApproval')
            ->name('crf.beict.approval');
        Route::get('/{request_change_bps_id}/beict/form/reject','RequestChangeController@beictFormReject')
            ->name('crf.form.reject');
        Route::put('/{request_change_id}/beict/reject','RequestChangeController@beictCrfReject')
            ->name('crf.beict.reject');
    });

});

Route::get('/requests/{user}', 'RequestController@index')->middleware('signedurl')->name('signed.index');
Route::get('/requests/{request}/detail/crf/{user}', 'RequestController@detailcrfshow')->middleware('signedurl')->name('signed.detailcrf');
Route::get('/requests/{request}/approve/show/{user}', 'RequestController@approveshow')->middleware('signedurl')->name('signed.approveshow');
Route::get('/requests/{request}/{service}/bps/{user}', 'RequestController@bpsshow')->middleware('signedurl')->name('signed.bpsshow');
Route::get('/requests/{request}/signed/coo/show/{user}', 'RequestController@coordinatorshow')->middleware('signedurl')->name('signed.coordinatorshow');
Route::put('/requests/{request}/signed/coo/approve/{user}', 'RequestController@coordinatorapprove')->middleware('signedurl')->name('signed.coordinatorapprove');
Route::get('/requests/{request}/manager/approve/{user}', 'RequestController@managerbeictapprove')->middleware('signedurl')->name('signed.managerbeictapprove');
Route::get('/requests/{request}/manager/show/{user}', 'RequestController@managerbeictshow')->middleware('signedurl')->name('signed.managerbeictshow');
Route::get('/requests/{request}/edit/ticket/{user}', 'RequestController@editticket')->middleware('signedurl')->name('signed.editticket');
Route::get('/requests/{request}/show/{user}', 'RequestController@show')->middleware('signedurl')->name('signed.show');
Route::get('/requests/{request}/update/transport/crf/{user}', 'RequestController@trasportupdate')->middleware('signedurl')->name('signed.trasportupdate');
Route::get('/requests/{request}/nda/crf/show/{user}', 'RequestController@ndaapproveshow')->middleware('signedurl')->name('signed.ndaapproveshow');
Route::get('/requests/{request}/transport/crf/update/{user}', 'RequestController@trasportconfirm')->middleware('signedurl')->name('signed.trasportconfirm');
Route::get('/requests/{request}/detail/crf/show/{user}', 'RequestController@detailcrfshow')->middleware('signedurl')->name('signed.detailcrfshow');
Route::put('/requests/{request}/employee/approve{user}', 'RequestController@employeeapprove')->middleware('signedurl')->name('signed.employeeapprove');
Route::get('/requests/{request}/spsd/approve/{user}', 'RequestController@spsdapprove')->middleware('signedurl')->name('signed.spsdapprove');
Route::get('/requests/{request}/spsd/show/{user}', 'RequestController@spsdshow')->middleware('signedurl')->name('signed.spsdshow');
Route::get('/requests/{request}/form/rekomendasi/{user}', 'RequestController@editrecomedation')->middleware('signedurl')->name('signed.editrecomedation');
Route::get('/incident/{incident}/ticket/show/{user}', 'IncidentController@ticketshow')->middleware('signedurl')->name('signed.incidents.ticketshow');
Route::get('/incident/{incident}/detailshow/{user}', 'IncidentController@detailshow')->middleware('signedurl')->name('signed.incidents.detailshow');
Route::get('/incident/{incident}/show/{user}', 'IncidentController@show')->middleware('signedurl')->name('signed.incidents.show');
Route::get('/incident/{incident}/detailshow/{user}', 'IncidentController@detailshow')->middleware('signedurl')->name('signed.incidents.detailshow');
Route::get('/incident/{incident}/approve/show/{user}', 'IncidentController@approveshow')->middleware('signedurl')->name('signed.incidents.approveshow');
Route::get('/requests/{request}/input/konfirmasi/transport/{user}', 'RequestController@konfirmasitrshow')->middleware('signedurl')->name('signed.konfirmasitrshow');
Route::get('/requests/{request}/upload/document/show/{user}', 'RequestController@uploaddocumentshow')->middleware('signedurl')->name('signed.uploaddocumentshow');
Route::get('/requests/{request}/co/show/{user}', 'RequestController@coshow')->middleware('signedurl')->name('signed.coshow');
Route::get('/requests/{request}/chief/show/{user}', 'RequestController@chiefshow')->middleware('signedurl')->name('signed.chiefshow');
Route::get('/requests/{request}/srf/reject', 'RequestController@srfreject')->middleware('signedurl')->name('signed.chiefshow');



Route::get('/kaseya/notes', 'KeseyaApi@notes')->name('notes');
Route::get('/kaseya/procedureAgent', 'KeseyaApi@procedureAgent')->name('procedureAgent');

Route::get('/kaseya/incident', 'KeseyaApi@incident')->name('incident');
Route::get('/kaseya/close/incident', 'KeseyaApi@closeticketincident')->name('closeticketincident');
Route::get('/kaseya/status/incident', 'KeseyaApi@incidentstatus')->name('incidentstatus');
Route::get('/kaseya/show/incident', 'KeseyaApi@showIncident')->name('showIncident');
Route::get('/kaseya/resolve/incident', 'KeseyaApi@resolve')->name('resolve');

Route::get('/kaseya/srf', 'KeseyaApi@srf')->name('srf');
Route::get('/kaseya/resolve/srf', 'KeseyaApi@resolvesrf')->name('resolvesrf');
Route::get('/kaseya/show/srf', 'KeseyaApi@showsrf')->name('resolvesrf');
Route::get('/kaseya/close/srf', 'KeseyaApi@closeticketsrf')->name('closeticketsrf');

Route::get('a/{personnel_no}/{email}', 'Auth\LoginController@programaticallyEmployeeLogin')
    ->name('login.a');
Route::get('b/{email}', 'Auth\LoginController@programaticallyServiceDeskLogin')
    ->name('login.b');
Route::get('c/{email}', 'Auth\LoginController@programaticallySecretaryLogin')
    ->name('login.c');

Route::group(['middleware' => ['auth:secr']], function () {
    Route::resource('secretary', 'SecretaryController');
    Route::get('/secretary/request/show/{request}', 'SecretaryController@show')->name('secretary.show');
    Route::get('/subord', 'SecretaryController@subordinate')->name('subord');
    Route::get('/secr/nikbos/{id}', 'SecretaryController@cekboss')->name('setnikboss');
    Route::get('/secr/subor/{id}', 'SecretaryController@bossSubordinate')->name('suborBoss');
});