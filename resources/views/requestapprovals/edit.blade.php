@extends('layouts.app')
@section('content')

<script>
    function ceking(isi){
        $('#aksi').val(isi);
        return true;
    }
</script>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form Persetujuan Layanan</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" 
                        @role('boss')
                            action="{{ route('requests.approvesave', $request->id) }}" 
                        @endrole
                        @role('so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                            action="{{ route('requests.soapprove', $request->id) }}" 
                        @endrole
                        @role('operation ict')
                            action="{{ route('requests.spictapprove', $request->id) }}" 
                        @endrole
                        @role('operation sd')
                            action="{{ route('requests.spsdapprove', $request->id) }}" 
                        @endrole name="myForm"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Judul</label>
                                <input 
                                @role('service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                                    readonly
                                @endrole
                                type="text" name="title" id="summernote" rows="2" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" autofocus value="{{$request->title}}">
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                        </div>
                        <div class="form-group">
                            <label for="service_id">Layanan</label>
                            <select 
                            @role('service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                                readonly
                            @endrole
                            id="idservice" name="service_id" class="form-control {{ $errors->has('service_id') ? ' is-invalid' : '' }}">
                                    <option>Pilih Service</option>
                                @foreach ($services as $service)
                                    @if($service->id == $request->service_id)
                                        <option selected value={{$service->id}}>{{$service->name}}</option>
                                    @else
                                        <option value={{$service->id}}>{{$service->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('service_id') }}</strong>
                            </span>
                        </div>
                        <div class="form-group" >
                            <label for="categories">Kategori Layanan</label>
                            <select 
                            @role('service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                            readonly
                            @endrole
                            id="idcategories" name="categories" class="form-control {{ $errors->has('categories') ? ' is-invalid' : '' }}">
                                <option>Pilih Kategori</option>
                                @foreach ($categories as $category)
                                @if($category->id == $request->category_id)
                                    <option selected value={{$category->id}}>{{$category->name}}</option>
                                @else
                                    <option value={{$category->id}}>{{$category->name}}</option>
                                @endif
                            @endforeach
                            </select>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('categories') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_need">Kebutuhan Bisnis</label>
                            <textarea 
                            @role('service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                            readonly
                            @endrole
                            name="business_need" id="summernote" rows="6" class="form-control {{ $errors->has('business_need') ? ' is-invalid' : '' }}" autofocus>{{ $request->business_need }}</textarea>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('business_need') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_benefit">Manfaat Bisnis</label>
                            <textarea 
                            @role('service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                            readonly
                            @endrole
                            name="business_benefit" rows="6" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ $request->business_benefit }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('business_benefit') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="reason">Keterangan tambahan ( jenis aplikasi, level akses, dll)</label>
                            <textarea 
                                @role('service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                                readonly
                                @endrole name="keterangan" rows="6" class="form-control {{ $errors->has('keterangan') ? ' is-invalid' : '' }}" autofocus>{{ $request->keterangan }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('keterangan') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lampiran</div>
                                <div class="panel-body">
                                    @foreach($request->requestAttachments as $item)
                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                        <a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span> {{$item->alias}} </span> </a><br/><br/>
                                    @endforeach
                                </div>
                            </div>
                            {{-- <a class="btn btn-primary" href="{{asset('storage/' . $request->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> --}}
                        </div>
                        @if($request->category_id == 2)
                        <div class="form-group">
                            <br/>
                            <div class="custom-control custom-checkbox">
                                <label class="custom-control-label" for="customCheck1">Sudah menyetujui Kebijakan dan Aturan Penggunaan Layanan <span type="button" class="badge badge-danger" data-toggle="modal" data-target="#myModal"> View </span></label>
                            </div>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nda') }}</strong>
                            </span>
                        </div>
                        @endif
                        @role('operation ict')
                        <div class="form-group">
                            <label for="business_benefit">File Lampiran Service Owner</label>
                        </div>
                        <div class="form-group">
                            @foreach($request->requestReasonfiles as $item)
                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                            @endforeach
                            {{-- <a class="btn btn-primary" href="{{asset('storage/' . $request->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> --}}
                        </div>
                        <div class="form-group">
                            <label for="reason">Alasan Service Owner</label>
                            <textarea readonly name="reason" rows="6" class="form-control {{ $errors->has('reason') ? ' is-invalid' : '' }}" autofocus>{{ $request->reason }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('reason') }}</strong>
                            </span>
                        </div>
                        @endrole
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus></textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div>
                        @role('boss|operation ict')
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                {{-- <select id="aksi" name="aksi" class="form-control {{ $errors->has('aksi') ? ' is-invalid' : '' }}">
                                    <option>Pilih aksi</option>
                                    <option value="1">Setujui</option>
                                    @role('boss|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging|operation ict')
                                    <option value="2">Tolak</option>
                                    @endrole
                                </select> --}}
                                <input type="hidden" name="aksi" value="" id="aksi" />
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('aksi') }}</strong>
                                </span>
                            </div>
                            <div class="btn-group mb-3" role="group">
                                <button type="submit" class="btn btn-primary" id="idtolak" onclick="return ceking(2)">
                                    Tolak
                                </button>
                            </div>
                            <div class="btn-group mb-3" role="group">
                                <button type="submit" class="btn btn-primary" id="idsetuju" onclick="return ceking(1)">
                                    Setuju
                                </button>
                            </div>
                        </div>
                        @endrole
                        <!-- Modal -->
                        @include('requests.nda')
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection