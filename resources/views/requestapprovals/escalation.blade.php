@extends('layouts.app')
@section('content')

<script>
    function ceking(isi){
        $('#aksi').val(isi);
        return true;
    }
</script>
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form Persetujuan Layanan Specialist Service Desk</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('requests.spsdapprove', $request->id) }}" name="myForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$request->title}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                <label for="service_id">Layanan: </label>
                                    <br/>
                                    <input style="color:black;" type="hidden"  name="service_idold" value="{{$request->service_id}}" />
                                    <select style="color:black;" require id="idservice" name="service_id" class="form-control {{ $errors->has('service_id') ? ' is-invalid' : '' }}">
                                        <option value="">Pilih Layanan</option>
                                        <optgroup label="Layanan aplikasi mes">
                                            @foreach ($services as $service)
                                                @if($service->groupserv == "mes")
                                                    <option 
                                                    @if($service->id == $request->service_id)
                                                        selected
                                                    @endif
                                                    value="{{$service->id}}">{{$service->name}}</option>  
                                                @endif
                                            @endforeach
                                        </optgroup> 
                                        <optgroup label="Layanan aplikasi lainya">
                                            @foreach ($services as $service)
                                                @if($service->groupserv != "mes" && $service->groupserv != "sap")
                                                    <option 
                                                    @if($service->id == $request->service_id)
                                                        selected
                                                    @endif
                                                    value="{{$service->id}}">{{$service->name}}</option>  
                                                @endif
                                            @endforeach
                                        </optgroup> 
                                </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group" >
                                    <label for="categories">Kategori Layanan:</label>
                                    <br/>
                                    {{$request->category->name}}     
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $request->business_need }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $request->business_benefit }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Keterangan tambahan ( jenis aplikasi, level akses, dll): </label>
                                    <br/>
                                    {{ $request->keterangan }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Kebijakan dan Aturan:</label><br/>
                                    <label class="custom-control-label" for="customCheck1">Sudah menyetujui Kebijakan dan Aturan Penggunaan Layanan <span type="button" class="badge badge-danger" data-toggle="modal" data-target="#myModal"> View </span></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $request->user->name }} ( {{ $request->user->id }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $request->user->email }}, Tepl: {{ $request->telp }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan:</label>
                                    <br/>
                                    {{ $request->stage->name }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($request->requestAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @role('operation ict')
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Alasan:</label>
                                    <br/>
                                    {{ $request->reason }}
                                </div>
                            </div>
                        </div>
                        @endrole
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>RIWAYAT</b></div>
                                    <div class="panel-body">
                                        <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                            <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                <tbody class="table-success">
                                                    <tr>
                                                        <td><b>Tanggal</b></td>
                                                        <td><b>Pengguna</b></td>
                                                        <td><b>Aksi</b></td>
                                                    </tr>
                                                    <tbody class="table-success">	
                                                        @foreach ($request->requestActions as $item)
                                                            <tr>
                                                                <td>{{$item->created_at}}</td>
                                                                <td>
                                                                    {{$item->users->name}}
                                                                    <br/>
                                                                    <br/>
                                                                    {{-- {{$item->requestActionNotes}} --}}
                                                                    Note: {{ $item->requestActionNotes == "" ? '' : $item->requestActionNotes->note }}
                                                                </td>
                                                                <td>{{$item->action->name}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="6"><b>PENYELESAIAN</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">{{$request->detail}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus></textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div>
                        @role('operation sd')
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                {{-- <select id="aksi" name="aksi" class="form-control {{ $errors->has('aksi') ? ' is-invalid' : '' }}">
                                    <option>Pilih aksi</option>
                                    <option value="1">Setujui</option>
                                    @role('boss|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging|operation ict')
                                    <option value="2">Tolak</option>
                                    @endrole
                                </select> --}}
                                <input type="hidden" name="aksi" value="" id="aksi" />
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('aksi') }}</strong>
                                </span>
                            </div>
                            <div class="btn-group mb-3" role="group">
                                <button type="submit" class="btn btn-primary" id="ideskalasi" onclick="return ceking(2)">
                                    Eskalasi
                                </button>
                            </div>
                            <div class="btn-group mb-3" role="group">
                                <button type="submit" class="btn btn-primary" id="idsetuju" onclick="return ceking(1)">
                                    Setuju
                                </button>
                            </div>
                        </div>
                        @endrole
                        <!-- Modal -->
                        
                        @include('requests.nda')

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection