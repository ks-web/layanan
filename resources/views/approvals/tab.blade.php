
<ul class="nav nav-tabs">
@role('boss|operation sd|so mes flat|so web|so mes long|so sap hr|so sap ppqm|operation ict|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
							
    <li 
            class="@if(Request::segment(2) == 'request')
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('approval.show','request') }}">Permintaan Layanan 
                @if(isset($countWaitingApproved) && $countWaitingApproved > 0)
                    <span class="badge badge-warning">{{$countWaitingApproved}}</span>
                @endif
            </a>
        </li>
    @endrole
    @role('operation sd|so mes flat|so web|so mes long|so sap hr|so sap ppqm|operation ict|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
							
        <li 
            class="@if(Request::segment(2) == 'crf')
                    active
                @endif"
            role="presentation">
            <a href="{{ route('approval.show', 'crf') }}">Permintaan CRF
                @if(isset($countRequestChange) && $countRequestChange > 0)
                    <span class="badge badge-warning">{{$countRequestChange}}</span>
                @endif
            </a>
        </li>
        <li 
            class="@if(Request::segment(2) == 'bps')
                    active
                @endif"
            role="presentation">
            <a href="{{ route('approval.show', 'bps') }}">Permintaan BPS
                @if(isset($countRequestChangeBps) && $countRequestChangeBps > 0)
                    <span class="badge badge-warning">{{$countRequestChangeBps}}</span>
                @endif
            </a>
        </li>
    @endrole

    @role('boss')
        <li 
            class="@if(Request::segment(2) == 'crf')
                    active
                @endif"
            role="presentation">
            <a href="{{ route('approval.show', 'crf') }}">Permintaan CRF
                <!-- counting change request -->
                @if(isset($countRequestChange) && $countRequestChange > 0)
                    <span class="badge badge-warning">{{$countRequestChange}}</span>
                    
                @endif
            </a>
        </li>
    @endrole

    @role('manager beict')
    <li 
        class="@if(Request::segment(2) == 'bps')
                active
            @endif"
        role="presentation">
        <a href="{{ route('approval.show', 'bps') }}">Permintaan BPS
            @if(isset($countRequestChangeBps) && $countRequestChangeBps > 0)
                <span class="badge badge-warning">{{$countRequestChangeBps}}</span>
            @endif
        </a>
        </li>
    @endrole

    {{-- @role('coordinator aplikasi|coordinator infra|')
        <li 
            class="@if(Request::segment(2) == 'bps')
                    active
                @endif"
            role="presentation">
            <a href="{{ route('approval.show', 'bps') }}">Permintaan BPS
                @if(isset($countRequestChangeBps) && $countRequestChangeBps > 0)
                    <span class="badge badge-warning">{{$countRequestChangeBps}}</span>
                @endif
            </a>
        </li>
    @endrole --}}


    @role('operation sd')
        <li 
            class="@if(Request::segment(2) == 'incident')
                    active
                @endif"
            role="presentation">
            <a href="{{ route('approval.show', 'incident') }}">Laporan Gangguan
                @if(isset($countEscalation) && $countEscalation > 0)
                    <span class="badge badge-warning">{{$countEscalation}}</span>
                @endif
            </a>
        </li>
    @endrole
</ul>
