@extends('layouts.app')
@section('content')
{{-- <ul class="nav nav-tabs">

</ul> --}}
@include('approvals.tab')
<br/>
@if (session('success'))
	<div class="alert alert-success" role="alert">
		{{ session('success') }}
	</div>
@endif
<div class="panel panel-default table-responsive" >
  	<div class="panel-heading">Data Permintaan / Perubahan Layanan</div>
	<div class="panel-body">
		<table id="dataChangeCrf" class="display" cellspacing="0" width="100%">
			<thead class="table-success">
				<tr>
					<th class="center" width="15%">Nomor Tiket</th>
					<th class="center" width="20%">Bussiness Prosess Justification</th>
					<th class="center" width="20%">Global Desain</th>
					<th class="center" width="20%">Detail Specification</th>
					<th class="center" width="20%">Tahap</th>
					<th class="center" width="15%">Tanggal Buat</th>
					<th> --- </th>
				</tr>
			</thead>
			<tbody>
					@foreach ($bps as $item)
					<tr>
						<td>{{$item->requestChange->ticket}}</td>
						<td>{{str_limit($item->bpj,50)}}</td>
                        <td>{{str_limit($item->global_design,50)}}</td>
						<td>{{str_limit($item->detail_specification,50)}}</td>
						<td>{{str_limit($item->stage->name,50)}}</td>
                        <td>{{$item->created_at}}</td>
						<td>
							@include('bps._action')
							@include('bps._action-view')
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
	</div>
</div>
{{-- modal --}}
<div id="modalresponse" style="margin-top:2rem">
{{--  end modal --}}
<script>
	jQuery(document).ready(function(){
		$('.id-modal').on('click',function(e){
			var token = $("meta[name='csrf-token']").attr("content");
			var url = $(event.target).data('url');
			$.ajax({ 
				url: url,
				type: "GET",
				data: {"_token": token, },
				success: function (data, textStatus, jqXHR) {
					$("#modalresponse").html(data.html);
					$('#myModal').modal('show');
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert("AJAX error: " + textStatus + ' : ' + errorThrown);
				},
			});
		});
	});
</script>
@endsection
