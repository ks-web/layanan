@extends('layouts.app')
@section('content')
@include('approvals.tab')
<br/>
@if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif
<div class="panel panel-default">
  	<div class="panel-heading">Data Layanan Gangguan</div>
	<div class="panel-body">
        <table id="dataTablesInc" class="display" cellspacing="0" width="100%">
            <thead class="table-success">
                <tr>
                    <th width="3%">ID</th>
                    <th width="10%">Tiket</th>
                    <th width="15%">Dibuat</th>
                    <th width="15%">Target Selesai</th>
                    <th width="32%">Deskripsi</th>
                    <th width="10%">Dampak</th>
                    <th width="10%">Prioritas</th>
                    <th width="15%">Detail Layanan</th>
                    <th width="10%">User</th>
                    <th width="5%">Tahap</th>
                    <th width="10%">Telp</th>
                    <th width="15%">Lokasi</th>
                    <th width="15%">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($paginated as $incident)
                <tr>
                    <td class="boldmetightInc"   
                        @if($incident->breach_status == 1)
                            style="background-color:red;color:white;"
                        @endif
                    >{{$incident->id}}</td>

                    <td>{{$incident->ticket}}</td>
                    <td>{{$incident->created_at}}</td>
                    <td>{{$incident->duedate}}</td>

                    <td>{{str_limit($incident->description,50)}}</td>
                    <td>{{str_limit($incident->impact,50)}}</td>
                    <td>{{$incident->priority->name}}</td>
                
                    <td>{{$incident->detail}}</td>
                    <td>{{$incident->user->IdWithName}}</td>
                    <td>{{$incident->stage->name}}</td>
                    <td>{{$incident->telp}}</td>
                    <td>{{$incident->location}}</td>
                    <td>
                        <a class="btn btn-primary myModal" data-route-id="{{$incident->id}}" data-toggle="tooltip" data-placement="left" title="View Detail"><span class="glyphicon glyphicon-eye-open" data-route-id="{{$incident->id}}" ></span></a>
                        @role('operation sd')
                            @if($incident->stage->id == 2)
                                <a class="btn btn-danger failModal1" data-url="{{$incident->id}}" data-toggle="tooltip" data-placement="left" title="Fail tiket"><span class="glyphicon glyphicon-remove" data-url="{{$incident->id}}"></span></a>
                            @endif
                        @endrole
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
    <div class="modal fade" id="modalBro" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body" id="test">
                    <label for="title">Judul</label>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal2" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Form input note specialist servicedesk</h4>
                </div>
                <form id="formField2" class="form" method="POST" action="{{ route('incidents.fail') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body" id="test2">
                        <input type="hidden" name="id2" value="" id="idIncident2"/>
                        <label>Note:</label>
                        <textarea style="color:black;" id="note2" name="note2" rows="4" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ old('note2') }}</textarea>
                            <span class="invalid-feedback validasinote" role="alert">
                                <strong>{{ $errors->first('note2') }}</strong>
                            </span>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group" style="margin-top:1em">
                            <div role="group">
                                <button id="simpan2" class="btn btn-primary" onclick="return formFalidateNote1();">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Form input note service desk</h4>
                    </div>
                    <form id="formField" class="form" method="POST" action="{{ route('incidents.escalation') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body" id="test1">
                            <input type="hidden" name="id1" value="" id="idIncident1"/>
                            <label>Note:</label>
                            <textarea style="color:black;" id="note1" name="note1" rows="4" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ old('note1') }}</textarea>
                                <span class="invalid-feedback validasinote1" role="alert">
                                    <strong>{{ $errors->first('note1') }}</strong>
                                </span>
                        </div>
                        <div class="modal-footer">
                            <div class="form-group" style="margin-top:1em">
                                <div role="group">
                                    <button id="simpan" class="btn btn-primary" onclick="return formFalidateNote();">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <script>
        jQuery(document).ready(function(){
            jQuery('.myModal').on('click',function(e){
                var id = e.target.getAttribute("data-route-id");
                //alert(id);
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ url('incident/show') }}"+"/"+id,
                    method: 'GET',
                    data: {
                        name: jQuery('#name').val(),
                    },
                    success: function(result){
                        var length = result.incidentActions.length;
                        var lengthattach = result.incidentAttachments.length;
                        console.log(length);
                        var response = 
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='service_id'>Deskripsi Gangguan:</label>"+
                                    "<br/>"+result.incident.description+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group' >"+
                                    "<label for='categories'>Dampak:</label>"+
                                    "<br/>"+result.incident.impact+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Prioritas</label>"+
                                    "<br/>"+result.incident.priority.name+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_benefit'>Cakupan:</label>"+
                                    "<br/>"+result.incident.severity.name+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Peminta:</label>"+
                                    "<br/>"+result.incident.user.id+" (" + result.incident.user.name + ")</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_benefit'>Email:</label>"+
                                    "<br/>"+result.incident.user.email+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Contact:</label>"+
                                    "<br/>"+result.incident.telp+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Stage:</label>"+
                                    "<br/>"+result.incident.stage.name+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Nomor Tiket Kaseya:</label>"+
                                    "<br/>"+result.incident.ticket+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_benefit'>Detail Pelayanan:</label>"+
                                    "<br/>"+result.incident.detail+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Target Selesai:</label>"+
                                    "<br/>"+result.incident.duedate+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='panel panel-default'>"+
                            "<div class='panel-heading'><b>USER LAMPIRAN</b></div>"+
                            "<div class='panel-body'>"+
                                "<div class='table-responsive'>"+
                                    "<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
                                        "<tbody class='table-success'>"+
                                            "<tr>"+
                                                "<td colspan='12'>";
                                                    for(var i=0; i<lengthattach; i++)
                                                    {
                                                        response += "<a class='btn btn-primary'"+
                                                                    "href='/itos/storage/"+ result.incidentAttachments[i].attachment +"' target='_blank'>"+
                                                                    "<span class='glyphicon glyphicon-file'></span>File</a>"+
                                                                    "<a "+
                                                                    "href='/itos/storage/"+ result.incidentAttachments[i].attachment +"' target='_blank'> "+ result.incidentAttachments[i].alias +"</a><br>";
                                                    }		         
                                                        
                                response +=      "</td>"+
                                                    "</tr>"+
                                                "</tbody>"+
                                            "</table>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                                "<div class='row'>"+
                                    "<div class='col-lg-12'>"+
                                        "<div class='panel panel-default'>"+
                                            "<div class='panel-heading'><b>RIWAYAT</b></div>"+
                                            "<div class='panel-body'>"+
                                                "<div class='table-responsive' style='overflow-y: scroll; height:250px;'>"+
                                                    "<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
                                                        "<tbody class='table-success'>"+
                                                            "<tr>"+
                                                                "<td><b>Tanggal</b></td>"+
                                                                "<td><b>Pengguna</b></td>"+
                                                                "<td><b>Aksi</b></td>"+
                                                            "</tr>"+
                                                            "<tbody class='table-success table tes'>";
                                                                for(var i=0; i<length; i++)
                                                                {
                                                                    response += "<tr><td>"+result.incidentActions[i].created_at+"</td>"+
                                                                                "<td>"+result.incidentActions[i].users.name+"<br/>";
                                                                                if(result.incidentActions[i].incident_action_notes !== null)
                                                                                {
                                                                                    response += "Note: " + result.incidentActions[i].incident_action_notes.note
                                                                                }
                                                                    response += "</td>"+"<td>"+result.incidentActions[i].action.name+"</td></tr>";
                                                                    
                                                                }			
                                            response += 											
                                                        "</tbody>"+
                                                            "</tbody>"+
                                                                "</table>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>"+
                                                "</div>"+
                                            "</div>";
            $("#test").html(response);
            jQuery('#modalBro').modal();
                }});
            });

            jQuery('.failModal1').on('click',function(e){
                var id1 = e.target.getAttribute("data-url");
                $("#idIncident2").val(id1);
                jQuery('#modal2').modal();
                return false;
            });

            jQuery('.failModal10').on('click',function(e){
                var id1 = e.target.getAttribute("data-url");
                $("#idIncident2").val(id1);
                jQuery('#modal2').modal();
                return false;
            });

            jQuery('.failModal').on('click',function(e){
                var id = e.target.getAttribute("data-url");
                $("#idIncident1").val(id);
                jQuery('#modal1').modal();
                return false;
            });
	});

    function formFalidateNote() {
        var note = $('#note1').val();
        if (note.trim() == ""){
            $('.validasinote1').html("<strong>Tidak boleh kosong.</strong>");
            return false;
        }
        else{
            return true;
        }
    }

    function formFalidateNote1() {
        var note = $('#note2').val();
        if (note.trim() == ""){
            $('.validasinote').html("<strong>Tidak boleh kosong.</strong>");
            return false;
        }
        else{
            return true;
        }
    }
    </script>
@endsection
