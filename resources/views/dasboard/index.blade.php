@extends('layouts.app')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading"><b>Dashboard</b></div>
        <div class="panel-body">
            {{-- Tampilan dasboard untuk boss --}}
            @role('boss')
                <div class="row">
                    <a href="{{ route('approval.show', 'request') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu Persetujuan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_waiting_boss_approved}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', '11') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Ditolak</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_rejected}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', 'all') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Semua Permintaan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_boss_approved}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- End tampilan dasboard untuk boss --}}
            {{-- Tampilan dasboard untuk specialist service desk --}}
            @role('operation sd')
                <div class="row">
                    <a href="{{ route('approval.show', 'request') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu Persetujuan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_waiting_spsd_approved}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', '10') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">SRF Telah dieskalasi</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_eskalasi}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', 'all') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Semua permintaan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_spsd_approved}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- end tampilan dasbaoard specialist service desk --}}
            {{-- Tampilan dasboard untuk ict --}}
            @role('operation ict')
                <div class="row">
                    <a href="{{ route('approval.show', 'request') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu Persetujuan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_waiting_spict_approved}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', 'all') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Semua permintaan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_all}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- end tampilan dasboard ict --}}
            {{-- Mulai tampilan dasboard untuk service desk --}}
            @role('service desk')
                <div class="row">
                    <a href="{{ route('requests.viewperstage', '3') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu input tiket</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_waiting_tiket}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.index') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Sedang Berjalan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_rejected}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- end tampilan data dasboard service desk --}}
            {{-- mulai tampilan data dasboar service owner (SO) --}}
            @role('so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
                <div class="row">
                    <a href="{{ route('requests.index') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu Persetjuan SO</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_waiting_so_approved}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', 'all') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Semua Permintaan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_sedang_berjalan}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', '9') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Selesai</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_closed}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- end tampilan data dasboard service owner (SO) --}}
            {{-- mulai tampilan data dasboard coordinator aplikasi --}}
            @role('coordinator aplikasi|coordinator infra')
                <div class="row">
                    <a href="{{ route('requests.index') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu Persetujuan</h3> 
                                    <h1 style="text-align:center">{{$waiting_co_approved_count}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', 'all') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Semua Permintaan</h3> 
                                    <h1 style="text-align:center">{{$all}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- end tampilan data dasboard coordinator aplikasi --}}
            {{-- mulai tampilan data dasboard chief --}}
            @role('chief')
                <div class="row">
                    <a href="{{ route('requests.index') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu Persetujuan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_waiting_chief_approved}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', 'all') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Semua Permintaan</h3> 
                                    <h1 style="text-align:center">{{$jumlah_request_all}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- end tampilan data dasboard chief --}}
        </div> 
        </div> 
    </div>
</div>
<script>
	function view(id){
		$.colorbox({
			iframe:true, 
			width:"80%", 
			height:"80%",
			transition:'none',
			title: "Preview Data"
			//href:"#"
		});
	}
</script>
@endsection