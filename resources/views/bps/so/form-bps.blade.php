@extends('layouts.app')
@section('content')
<script>
    function myFunction() {
        var copyText = document.getElementById("bussiness_note");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }
    
    function copyTitle() {
        var copyText = document.getElementById("title");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyBusinessBenefit() {
        var copyText = document.getElementById("business_benefit");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyKeterangan() {
        var copyText = document.getElementById("keterangan");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    $('#demodate').datetimepicker({
        inline:true,
    });
</script>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan / perubahan layanan: create BPS</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('crf.so.create.bps', $requestchange->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$requestchange->title}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_need }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_benefit }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $requestchange->user->name }} ( {{ $requestchange->user->id }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $requestchange->user->email }}, Telp: {{ $requestchange->telp }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan: </label>
                                    <br/>
                                    {{ $requestchange->stage->name }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Nomor Tiket: </label>
                                    <br/>
                                    {{ $requestchange->ticket }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($requestchange->requestChangeAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label  for="difficulty">Difficulty</label>
                                    <select required style="color:black;" id="difficulty" name="difficulty" class="form-control {{ $errors->has('difficulty') ? ' is-invalid' : '' }}">
                                            <option value="">--- Pilih ---</option>
                                        @foreach ($difficulties as $difficulty)
                                            <option value={{$difficulty->id}}>{{ $difficulty->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('difficulty') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="priority">priority</label>
                                    <select required style="color:black;" id="priority" name="priority" class="form-control {{ $errors->has('priority') ? ' is-invalid' : '' }}">s
                                            <option value="">--- Pilih ---</option>
                                        @foreach ($priorities as $priority)
                                            <option value={{$priority->id}}>{{ $priority->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="preparedby">Prepared By</label>
                                    <input required type="text" name="preparedby" values="{{ $requestchange->preparedby }}" class="form-control {{ $errors->has('preparedby') ? ' is-invalid' : '' }}" autofocus>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('preparedby') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="module">Module</label>
                                    <input required type="text" name="module" values="{{ $requestchange->module }}" class="form-control {{ $errors->has('module') ? ' is-invalid' : '' }}" autofocus>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('module') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input required type="text" name="email" values="{{ $requestchange->email }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" autofocus>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input required type="text" name="phone" values="{{ $requestchange->phone }}" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" autofocus>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="start_plan_date">Start Plan Date</label>
                                    <input required type="text" name="start_plan_date" values="{{ $requestchange->start_plan_date }}" class="tanggal form-control {{ $errors->has('start_plan_date') ? ' is-invalid' : '' }}" autofocus>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('start_plan_date') }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="end_plan_date">End Plan Date</label>
                                    <input required type="text" name="end_plan_date" values="{{ $requestchange->end_plan_date }}" class="tanggal form-control {{ $errors->has('end_plan_date') ? ' is-invalid' : '' }}" autofocus>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('end_plan_date') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="bpj">Bussines Process Justification</label>
                                    <textarea style="color:black;" required name="bpj" id="bpj" rows="3" class="form-control {{ $errors->has('bpj') ? ' is-invalid' : '' }}" autofocus>{{ old('bpj') }}</textarea>
                                    <span class="invalid-feedback validasibpj" role="alert">
                                        <strong>{{ $errors->first('bpj') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="global_design">Golbal Design</label>
                                    <textarea style="color:black;" required name="global_design" id="global_design" rows="3" class="form-control {{ $errors->has('global_design') ? ' is-invalid' : '' }}" autofocus>{{ old('global_design') }}</textarea>
                                    <span class="invalid-feedback validasiglobal_design" role="alert">
                                        <strong>{{ $errors->first('global_design') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="detail_specification">Detail Specification</label>
                                    <textarea style="color:black;" required name="detail_specification" id="detail_specification" rows="3" class="form-control {{ $errors->has('detail_specification') ? ' is-invalid' : '' }}" autofocus>{{ old('detail_specification') }}</textarea>
                                    <span class="invalid-feedback validasidetail_specification" role="alert">
                                        <strong>{{ $errors->first('detail_specification') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="effort">Effort</label>
                                    <textarea style="color:black;" required name="effort" id="effort" rows="2" class="form-control {{ $errors->has('effort') ? ' is-invalid' : '' }}" autofocus>{{ old('effort') }}</textarea>
                                    <span class="invalid-feedback validasieffort" role="alert">
                                        <strong>{{ $errors->first('effort') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="reason">Lampiran: Format file (image/pdf )</label>
                                    <div class="input-group control-group increment">
                                        <div class="custom-file">
                                            <input style="color:black;" class="custom-file-input" id="attachment" type="file" name="attachment[]" class="form-control">
                                        </div>
                                        <div class="input-group-btn"> 
                                            <button style="color:black;" class="btn btn-success btn-sm tambah1" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                        </div>
                                    </div>
                                    <div>
                                        <span class="invalid-feedback validasiattachment" role="alert">
                                            <strong>{{ $errors->first('attachment') }}</strong>
                                        </span>
                                    </div>
                                    <div class="clone hide">
                                        <div class="control-group input-group" style="margin-top:10px">
                                            <div class="custom-file">
                                                <input class="custom-file-input" type="file" name="attachment[]" class="form-control">
                                            </div>
                                            <div class="input-group-btn"> 
                                                <button class="btn btn-danger btn-sm kurang1" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="input-group-btn"> 
                                        <button style="color:black;" class="btn btn-success btn-sm tambah2" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>*input tidak boleh kosong, isi attribut - jika tidak memiliki nilai</p>
                        <div class="row">
                            <div class="col-lg-12">
                               
                                <div class="form-group ">
                                        <div class="table-responsive" style="overflow-y: scroll;">
                                            <table class="table table-striped table-responsive" cellspacing="0">
                                                <thead class="table-success">
                                                    <tr>
                                                        <th style="width: 10%">
                                                            Activity
                                                        </th>
                                                        <th style="width: 10%">                                               
                                                            Mandays
                                                        </th>
                                                        <th style="width: 10%">                                                
                                                            PIC                                            
                                                        </th>
                                                        <th style="width: 10%">                                            
                                                            Level                                       
                                                        </th>
                                                        <th style="width: 10%">
                                                            Start Date                                            
                                                        </th>
                                                        <th style="width: 10%">                                               
                                                            End Date     
                                                        </th>
                                                    </tr>
                                                </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="btn-group btn-group-lg" role="group">
                                <button type="submit" class="btn btn-primary">
                                    Simpan
                                </button>
                            </div>
                        </div>
                        <!-- Modal -->
                        @include('requests.nda')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection