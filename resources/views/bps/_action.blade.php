{{-- begin of coordinator so role --}}
@role('coordinator aplikasi|coordinator infra')
    @if($item->stage_id == 13)
        <a href="{{ route('bps.co.form.approval', $item->id) }}" class="btn btn-primary btn-sm"><span  data-url="{{ route('bps.co.form.approval', $item->id) }}" class="glyphicon glyphicon-pencil"></span></a>
    @endif
@endrole
{{-- end of coordinator service owner --}}

{{-- begin of manager beict --}}
@role('manager beict')
    @if($item->stage_id == 12)
        <a href="{{ route('bps.manager.form.approval', $item->id) }}" class="btn btn-primary btn-sm"><span  data-url="{{ route('bps.manager.form.approval', $item->id) }}" class="glyphicon glyphicon-pencil"></span></a>
    @endif
@endrole
{{-- end of coordinator manager beict --}}

@role('so web|so mes flat|so mes long|so sap hr|so sap ppqm|operation ict|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
    @if($item->stage_id == 40 || $item->stage_id == 13)
        @if(in_array($item->service_id ,$service))
            <a href="{{ route('crf.so.edit.bps', $item->id) }}" class="btn btn-primary btn-sm"><span  data-url="{{ route('crf.so.edit.bps', $item->id) }}" class="glyphicon glyphicon-pencil"></span></a>
        @endif
    @endif
@endrole