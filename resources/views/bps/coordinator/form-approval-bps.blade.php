@extends('layouts.app')
@section('content')
<script>
    function myFunction() {
        var copyText = document.getElementById("bussiness_note");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }
    
    function copyTitle() {
        var copyText = document.getElementById("title");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyBusinessBenefit() {
        var copyText = document.getElementById("business_benefit");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyKeterangan() {
        var copyText = document.getElementById("keterangan");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    $('#demodate').datetimepicker({
        inline:true,
    });

    function ceking(isi){
        $('#aksi').val(isi);
        return true;
    }
</script>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form permintaan / perubahan layanan (CRF): Persetujuan BPS</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('bps.co.approval', $requestchangeBps->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$requestchangeBps->requestchange->title}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $requestchangeBps->requestchange->business_need }}
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $requestchangeBps->requestchange->business_benefit }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $requestchangeBps->requestchange->user->name }} ( {{ $requestchangeBps->requestchange->user->id }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $requestchangeBps->requestchange->user->email }}, Tepl: {{ $requestchangeBps->requestchange->telp }}
                                </div>
                            </div>
                        </div>

                        {{--stage / tahap  --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan: </label>
                                    <br/>
                                    {{ $requestchangeBps->requestchange->stage->name }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Nomor Tiket: </label>
                                    <br/>
                                    {{ $requestchangeBps->requestchange->ticket }}
                                </div>
                            </div>
                        </div>
                        
                        {{-- lampiran --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($requestchangeBps->requestchange->requestChangeAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- bps --}}
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Data Bussiness Prcess Specification</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Priority: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->priority) ? $requestchangeBps->priority->name : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Difficulty: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->difficulty) ? $requestchangeBps->difficulty->name : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Prepared By: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->preparedby) ? $requestchangeBps->preparedby : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Module: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->module) ? $requestchangeBps->module : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Email: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->email) ? $requestchangeBps->email : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Phone: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->phone) ? $requestchangeBps->phone : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Start Plan Data: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->start_plan_date) ? $requestchangeBps->start_plan_date : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Finish Plan Data: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->finish_plan_date) ? $requestchangeBps->finish_plan_date : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Business Process Justification: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->bpj) ? $requestchangeBps->bpj : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Global Design: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->bpj) ? $requestchangeBps->bpj : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Detail Specification: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->detail_specification) ? $requestchangeBps->detail_specification : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Effort: </label>
                                                <br/>
                                                {{ isset($requestchangeBps->effort) ? $requestchangeBps->effort : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Time Duration Calculation</div>
                                    <div class="panel-body">
                                        <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                            <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                <thead class="table-success">
                                                    <tr>
                                                        <td><b>Activity</b></td>                                                    
                                                        <td><b>Mandays</b></td>
                                                        <td><b>Pic</b></td>                                                   
                                                        <td><b>Level</b></td>                                                    
                                                        <td><b>Start Date</b></td>                                                   
                                                        <td><b>Finish Date</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody class="table-success">	
                                                    @if(isset($requestchangeBps->requestChangeBpsDurations))
                                                        @foreach ($requestchangeBps->requestChangeBpsDurations as $item)
                                                            <tr>
                                                                <td>{{ isset($item->activity) ? $item->activity : '' }}</td>
                                                                <td>{{ isset($item->mandays) ? $item->mandays : '' }}</td>                  
                                                                <td>{{ isset($item->pic) ? $item->pic : ''}}</td>                         
                                                                <td>{{ isset($item->level_id) ? $item->level_id : ''}}</td>
                                                                <td>{{ isset($item->start_date) ? $item->start_date : ''}}</td>
                                                                <td>{{ isset($item->finish_date) ? $item->finish_date : ''}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{-- note --}}
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea style="color:black;" id=note name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus>{{ $requestchangeBps->requestchange->note }}</textarea>
                            <span class="invalid-feedback validasinote" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div>

                        {{-- history --}}
                        @include('bps._riwayat')

                        {{-- jenis aksi 1 stuju 2 tolak --}}
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                <input type="hidden" name="aksi" value="" id="aksi" />
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('aksi') }}</strong>
                                </span>
                            </div>
                        </div>

                        {{-- tombo tolak --}}
                        <div class="btn-group btn-group-lg" role="group">
                            <button type="submit" class="btn btn-danger btn" id="idtolak" onclick="return ceking(2)">
                                Tolak
                            </button>
                            <button type="submit" class="btn btn-primary" id="idsetuju" onclick="return ceking(1)">
                                Setuju
                            </button>
                            <button type="submit" class="btn btn-success" id="idperbaiki" onclick="return ceking(3)">
                                Perbaiki
                            </button>
                        </div>
                        <!-- Modal -->
                        @include('requests.nda')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection