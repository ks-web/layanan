@extends('layouts.app')
@section('content')
<p>&nbsp;</p>
<ul class="nav nav-tabs">
    @role('operation sd')
        @if($stage == null)
            <li role="presentation" class="active">
                <a href="{{ route('incidents.index') }}">In Progress  
                    @if($countInprogress > 0)
                        <span class="badge badge-warning">{{$countInprogress}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation">
                <a href="{{ route('incidents.showperstage', '2') }}">Menunggu Aksi 
                    @if($countEscalation > 0)
                        <span class="badge badge-warning">{{$countEscalation}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', 'breach') }}">Breach 
                    @if($countBreach > 0)
                        <span class="badge badge-warning">{{$countBreach}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', 'all') }}">Semua 
                    @if($countAll > 0)
                        <span class="badge badge-warning">{{$countAll}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', '9') }}">Selesai 
                    @if($countClose > 0)
                        <span class="badge badge-warning">{{$countClose}}</span>
                    @endif
                </a>
            </li>
        @else
            <li role="presentation" class="">
                <a href="{{ route('incidents.index') }}">In Progress 
                    @if($countInprogress > 0)
                        <span class="badge badge-warning">{{$countInprogress}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == '2') 
                    active 
                @endif ">
                <a href="{{ route('incidents.showperstage', '2') }}">Menunggu aksi 
                    @if($countEscalation > 0)
                        <span class="badge badge-warning">{{$countEscalation}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == 'breach') 
                    active 
                @endif ">
                <a href="{{ route('incidents.showperstage', 'breach') }}">Breach 
                    @if($countBreach > 0)
                        <span class="badge badge-warning">{{$countBreach}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == 'all') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', 'all') }}">Semua 
                    @if($countAll > 0)
                        <span class="badge badge-warning">{{$countAll}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == '9') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', '9') }}">Selesai 
                    @if($countClose > 0)
                        <span class="badge badge-warning">{{$countClose}}</span>
                    @endif
                </a>
            </li>
        @endif
    @endrole
    @role('so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
        @if($stage == null)
            <li role="presentation" class="active">
                <a href="{{ route('incidents.index') }}">In Progress 
                    @if($countInprogress > 0)
                        <span class="badge badge-warning">{{$countInprogress}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', 'breach') }}">Breach 
                    @if($countBreach > 0)
                        <span class="badge badge-warning">{{$countBreach}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', 'all') }}">Semua 
                    @if($countAll > 0)
                        <span class="badge badge-warning">{{$countAll}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', '9') }}">Selesai 
                    @if($countClose > 0)
                        <span class="badge badge-warning">{{$countClose}}</span>
                    @endif
                </a>
            </li>
        @else
            <li role="presentation" class="">
                <a href="{{ route('incidents.index') }}">In Progress 
                    @if($countInprogress > 0)
                        <span class="badge badge-warning">{{$countInprogress}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == 'breach') 
                    active 
                @endif ">
                <a href="{{ route('incidents.showperstage', 'breach') }}">Breach 
                    @if($countBreach > 0)
                        <span class="badge badge-warning">{{$countBreach}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == 'all') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', 'all') }}">Semua 
                    @if($countAll > 0)
                        <span class="badge badge-warning">{{$countAll}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == '9') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', '9') }}">Selesai 
                    @if($countClose > 0)
                        <span class="badge badge-warning">{{$countClose}}</span>
                    @endif
                </a>
            </li>
        @endif
    @endrole
    @role('service desk')
        @if($stage == null)
            <li role="presentation" class="active">
                <a href="{{ route('incidents.index') }}">Sedang berjalan 
                    @if($countInprogress > 0)
                        <span class="badge badge-warning">{{$countInprogress}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', '3') }}">Baru
                    @if($countTicket > 0)
                        <span class="badge badge-warning">{{$countTicket}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', '29') }}">User Menolak 
                    @if($countDeniedByUser > 0)
                        <span class="badge badge-warning">{{$countDeniedByUser}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', '5') }}">Resolve 
                    @if($countResolve > 0)
                        <span class="badge badge-warning">{{$countResolve}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', 'breach') }}">Breach 
                    @if($countBreach > 0)
                        <span class="badge badge-warning">{{$countBreach}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', '9') }}">Close 
                    @if($countClose > 0)
                        <span class="badge badge-warning">{{$countClose}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="">
                <a href="{{ route('incidents.showperstage', 'all') }}">Semua 
                    @if($countAll > 0)
                        <span class="badge badge-warning">{{$countAll}}</span>
                    @endif
                </a>
            </li>
        @else
            <li role="presentation" class="">
                <a href="{{ route('incidents.index') }}">Sedang berjalan
                    @if($countInprogress > 0)
                        <span class="badge badge-warning">{{$countInprogress}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == '3') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', '3') }}">Baru
                    @if($countTicket > 0)
                        <span class="badge badge-warning">{{$countTicket}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == '29') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', '29') }}">User Menolak
                    @if($countDeniedByUser > 0)
                        <span class="badge badge-warning">{{$countDeniedByUser}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == '5') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', '5') }}">Resolve
                    @if($countResolve > 0)
                        <span class="badge badge-warning">{{$countResolve}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == 'breach') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', 'breach') }}">Breach
                    @if($countBreach > 0)
                        <span class="badge badge-warning">{{$countBreach}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == '9') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', '9') }}">Close
                    @if($countClose > 0)
                        <span class="badge badge-warning">{{$countClose}}</span>
                    @endif
                </a>
            </li>
            <li role="presentation" class="
                @if($stage == 'all') 
                    active 
                @endif">
                <a href="{{ route('incidents.showperstage', 'all') }}">Semua
                    @if($countAll > 0)
                        <span class="badge badge-warning">{{$countAll}}</span>
                    @endif
                </a>
            </li>
        @endif
    @endrole
</ul>
<br/>
@role('employee')
    <a class="btn btn-primary" href="{{route('incidents.create')}}">
        Buat Laporan Gangguan
    </a>
@endrole
<br/>
<br/>
@if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif
<div class="panel panel-default">
  	<div class="panel-heading">Data Layanan Gangguan</div>
	<div class="panel-body">
        <table id="dataTablesInc" class="display" cellspacing="0" width="100%">
            <thead class="table-success">
                <tr>
                    <th width="3%">ID</th>
                    <th width="10%">Ticket</th>
                    <th width="15%">Dibuat</th>
                    <th width="15%">Target Selesai</th>
                    <th width="32%">Deskripsi</th>
                    <th width="10%">Dampak</th>
                    <th width="10%">Prioritas</th>
                    <th width="15%">Detail Layanan</th>
                    <th width="10%">User</th>
                    <th width="5%">Tahap</th>
                    <th width="10%">Telp</th>
                    <th width="15%">Lokasi</th>
                    <th width="15%">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($paginated as $incident)
                <tr>
                    <td class="boldmetightInc"   
                        @if($incident->breach_status == 1)
                            style="background-color:red;color:white;"
                        @endif
                    >{{$incident->id}}</td>

                    <td>{{$incident->ticket}}</td>
                    <td>{{$incident->created_at}}</td>
                    <td>{{$incident->duedate}}</td>

                    <td>{{str_limit($incident->description,50)}}</td>
                    <td>{{str_limit($incident->impact,50)}}</td>
                    <td>{{$incident->priority->name}}</td>
                
                    <td>{{$incident->detail}}</td>
                    <td>{{$incident->user->IdWithName}}</td>
                    <td>{{$incident->stage->name}}</td>
                    <td>{{$incident->telp}}</td>
                    <td>{{$incident->location}}</td>
                    <td>
                    <a class="btn btn-primary myModal" data-route-id="{{$incident->id}}" data-toggle="tooltip" data-placement="left" title="View Detail"><span class="glyphicon glyphicon-eye-open" data-route-id="{{$incident->id}}" ></span></a>
                    @if($stage != 'all')
                        @role('service desk')
                            @if($incident->stage->id == 4)
                                {{-- <a class="btn btn-primary" href="{{ route('incidents.detailshow', $incident->id) }}">Detail Input</a> --}}
                            @elseif($incident->stage->id == 3)
                                <a class="btn btn-primary" href="{{ route('incidents.ticketshow', $incident->id) }}" data-toggle="tooltip" data-placement="left" title="Input tiket"><span class="glyphicon glyphicon-pencil"></span></a>

                                <a class="btn btn-danger failModal" data-url="{{$incident->id}}" data-toggle="tooltip" data-placement="left" title="Fail tiket"><span class="glyphicon glyphicon-remove" data-url="{{$incident->id}}"></span></a>
                            @elseif($incident->stage->id == 9)
                                <form method="POST" action="{{route('incidents.arsip', $incident->id)}}">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <button class="btn btn-danger" type="submit" data-toggle="tooltip" data-placement="left" title="Arsipkan Permintaan"><span class="glyphicon glyphicon-book"></span></button>
                                </form>
                            @elseif($incident->stage->id == 4 || $incident->stage->id == 29)
                                {{-- <a class="btn btn-primary" href="{{ route('incidents.detailshow', $incident->id) }}">Detail Input</a> --}}
                            @endif     
                        @endrole
                        @role('operation sd')
                            @if($incident->stage->id == 2)
                                <a class="btn btn-danger failModal1" data-url="{{$incident->id}}" data-toggle="tooltip" data-placement="left" title="Fail tiket"><span class="glyphicon glyphicon-remove" data-url="{{$incident->id}}"></span></a>
                            @endif
                        @endrole
                        @role('employee')
                            @if($incident->stage->id == 6 || $incident->stage->id == 5)
                                <!-- <form method="POST" action="{{route('incidents.approveshow', $incident->id)}}">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <button class="btn btn-primary" type="submit" data-toggle="tooltip" data-placement="left" title="Setujui"><span class="glyphicon glyphicon-ok" ></span></button>
                                </form> -->
                                <!-- <a class="btn btn-primary" href="{{ route('incidents.rejectform', $incident->id) }}" data-toggle="tooltip" data-placement="left" title="Tolak"><span class="glyphicon glyphicon-remove"></span></a> -->
                            @endif
                        @endrole
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
    <div class="modal fade" id="modalBro" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body" id="test">
                    <label for="title">Judul</label>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal2" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Form input note specialist servicedesk</h4>
                </div>
                <form id="formField2" class="form" method="POST" action="{{ route('incidents.fail') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body" id="test2">
                        <input type="hidden" name="id2" value="" id="idIncident2"/>
                        <label>Note:</label>
                        <textarea style="color:black;" id="note2" name="note2" rows="4" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ old('note2') }}</textarea>
                            <span class="invalid-feedback validasinote" role="alert">
                                <strong>{{ $errors->first('note2') }}</strong>
                            </span>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group" style="margin-top:1em">
                            <div role="group">
                                <button id="simpan2" class="btn btn-primary" onclick="return formFalidateNote1();">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Form input note service desk</h4>
                    </div>
                    <form id="formField" class="form" method="POST" action="{{ route('incidents.escalation') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body" id="test1">
                            <input type="hidden" name="id1" value="" id="idIncident1"/>
                            <label>Note:</label>
                            <textarea style="color:black;" id="note1" name="note1" rows="4" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ old('note1') }}</textarea>
                                <span class="invalid-feedback validasinote1" role="alert">
                                    <strong>{{ $errors->first('note1') }}</strong>
                                </span>
                        </div>
                        <div class="modal-footer">
                            <div class="form-group" style="margin-top:1em">
                                <div role="group">
                                    <button id="simpan" class="btn btn-primary" onclick="return formFalidateNote();">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <script>
        jQuery(document).ready(function(){
            jQuery('.myModal').on('click',function(e){
                var id = e.target.getAttribute("data-route-id");
                //alert(id);
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ url('incident/show') }}"+"/"+id,
                    method: 'GET',
                    data: {
                        name: jQuery('#name').val(),
                    },
                    success: function(result){
                        var length = result.incidentActions.length;
                        var lengthattach = result.incidentAttachments.length;
                        console.log(length);
                        var response = 
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='service_id'>Deskripsi Gangguan:</label>"+
                                    "<br/>"+result.incident.description+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group' >"+
                                    "<label for='categories'>Dampak:</label>"+
                                    "<br/>"+result.incident.impact+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Prioritas</label>"+
                                    "<br/>"+result.incident.priority.name+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_benefit'>Cakupan:</label>"+
                                    "<br/>"+result.incident.severity.name+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Peminta:</label>"+
                                    "<br/>"+result.incident.user.id+" (" + result.incident.user.name + ")</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_benefit'>Email:</label>"+
                                    "<br/>"+result.incident.user.email+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Contact:</label>"+
                                    "<br/>"+result.incident.telp+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Stage:</label>"+
                                    "<br/>"+result.incident.stage.name+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Nomor Tiket Kaseya:</label>"+
                                    "<br/>"+result.incident.ticket+"</div>"+
                            "</div>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_benefit'>Detail Pelayanan:</label>"+
                                    "<br/>"+result.incident.detail+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row'>"+
                            "<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label for='business_need'>Target Selesai:</label>"+
                                    "<br/>"+result.incident.duedate+"</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='panel panel-default'>"+
                            "<div class='panel-heading'><b>USER LAMPIRAN</b></div>"+
                            "<div class='panel-body'>"+
                                "<div class='table-responsive'>"+
                                    "<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
                                        "<tbody class='table-success'>"+
                                            "<tr>"+
                                                "<td colspan='12'>";
                                                    for(var i=0; i<lengthattach; i++)
                                                    {
                                                        response += "<a class='btn btn-primary'"+
                                                                    "href='/itos/storage/"+ result.incidentAttachments[i].attachment +"' target='_blank'>"+
                                                                    "<span class='glyphicon glyphicon-file'></span>File</a>"+
                                                                    "<a "+
                                                                    "href='/itos/storage/"+ result.incidentAttachments[i].attachment +"' target='_blank'> "+ result.incidentAttachments[i].alias +"</a><br>";
                                                    }		         
                                                        
                                response +=      "</td>"+
                                                    "</tr>"+
                                                "</tbody>"+
                                            "</table>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                                "<div class='row'>"+
                                    "<div class='col-lg-12'>"+
                                        "<div class='panel panel-default'>"+
                                            "<div class='panel-heading'><b>RIWAYAT</b></div>"+
                                            "<div class='panel-body'>"+
                                                "<div class='table-responsive' style='overflow-y: scroll; height:250px;'>"+
                                                    "<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
                                                        "<tbody class='table-success'>"+
                                                            "<tr>"+
                                                                "<td><b>Tanggal</b></td>"+
                                                                "<td><b>Pengguna</b></td>"+
                                                                "<td><b>Aksi</b></td>"+
                                                            "</tr>"+
                                                            "<tbody class='table-success table tes'>";
                                                                for(var i=0; i<length; i++)
                                                                {
                                                                    response += "<tr><td>"+result.incidentActions[i].created_at+"</td>"+
                                                                                "<td>"+result.incidentActions[i].users.name+"<br/>";
                                                                                if(result.incidentActions[i].incident_action_notes !== null)
                                                                                {
                                                                                    response += "Note: " + result.incidentActions[i].incident_action_notes.note
                                                                                }
                                                                    response += "</td>"+"<td>"+result.incidentActions[i].action.name+"</td></tr>";
                                                                    
                                                                }			
                                            response += 											
                                                        "</tbody>"+
                                                            "</tbody>"+
                                                                "</table>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>"+
                                                "</div>"+
                                            "</div>";
            $("#test").html(response);
            jQuery('#modalBro').modal();
                }});
            });

            jQuery('.failModal1').on('click',function(e){
                var id1 = e.target.getAttribute("data-url");
                $("#idIncident2").val(id1);
                jQuery('#modal2').modal();
                return false;
            });

            jQuery('.failModal10').on('click',function(e){
                var id1 = e.target.getAttribute("data-url");
                $("#idIncident2").val(id1);
                jQuery('#modal2').modal();
                return false;
            });

            jQuery('.failModal').on('click',function(e){
                var id = e.target.getAttribute("data-url");
                $("#idIncident1").val(id);
                jQuery('#modal1').modal();
                return false;
            });
	});

    function formFalidateNote() {
        var note = $('#note1').val();
        if (note.trim() == ""){
            $('.validasinote1').html("<strong>Tidak boleh kosong.</strong>");
            return false;
        }
        else{
            return true;
        }
    }

    function formFalidateNote1() {
        var note = $('#note2').val();
        if (note.trim() == ""){
            $('.validasinote').html("<strong>Tidak boleh kosong.</strong>");
            return false;
        }
        else{
            return true;
        }
    }
    </script>
@endsection
