@extends('layouts.app')
@section('content')
	<ul class="nav nav-tabs">
@role('employee')
	@if($stage == null)
		<li class="active" role="presentation"><a href="{{ route('requests.index') }}">Sedang berjalan
			@if($countInprogressEmployee > 0)  
				<span class="badge badge-warning">{{$countInprogressEmployee}}</span>
			@endif
		</a></li>
		<li class="" role="presentation"><a href="{{ route('requests.viewperstage', '9') }}">Selesai
			@if($countCloseEmployee > 0)  
				<span class="badge badge-warning">{{$countCloseEmployee}}</span>
			@endif
		</a></li>
		<li class="" role="presentation"><a href="{{ route('requests.viewperstage', '11') }}">ditolak
			@if($countRejectedEmployee > 0)  
				<span class="badge badge-warning">{{$countRejectedEmployee}}</span>
			@endif
		</a></li>
	@else
		<li class="" role="presentation"><a href="{{ route('requests.index') }}">Sedang berjalan
			@if($countInprogressEmployee > 0)  
				<span class="badge badge-warning">{{$countInprogressEmployee}}</span>
			@endif
		</a></li>
		<li class="
			@if($stage == '9')
				active
			@endif
			"
			role="presentation"><a href="{{ route('requests.viewperstage', '9') }}">Selesai
			@if($countCloseEmployee > 0)  
				<span class="badge badge-warning">{{$countCloseEmployee}}</span>
			@endif
		</a></li>
		<li class="
			@if($stage == '11')
				active
			@endif
			"
			role="presentation"><a href="{{ route('requests.viewperstage', '11') }}">ditolak
			@if($countRejectedEmployee > 0)  
				<span class="badge badge-warning">{{$countRejectedEmployee}}</span>
			@endif
		</a></li>
	@endif
@endrole
	</ul>
<br/>
@role('employee')
	<a href="{{route('requests.create')}}" class="control">
		<button class="btn btn-primary">Buat Permintaan</button>
	</a>
@endrole
<p>&nbsp;</p>
@if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif
<div class="panel panel-default">
  	<div class="panel-heading">Data Permintaan Layanan</div>
	<div class="panel-body">
		<table id="dataTables1" class="display" cellspacing="0" width="100%">
			<thead class="table-success">
				<tr>
					<th class="center" width="3%"> ID </th>
					<th width="6%">TIKET</th>
					<th width="10%">TANGGAL BUAT</th>
					<th width="10%">TARGET SELESAI</th>
					<th width="10%">LAYANAN</th>
					<th width="15%">KATEGORI</th>
					<th width="15%">JUDUL</th>
					{{-- <th width="15%">ALASAN PERMINTAAN</th>
					<th width="15%">MANFAAT TERHADAP BISNIS</th> --}}
					@role('boss|service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
						<th width="10%">PEMINTA</th>
					@endrole
					<th width="15%">TAHAP</th>
					<th width="10%">TANGGAL UBAH</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($paginated as $request)
					<tr 
						@if($request->stage->id == '28')
							style="background-color:yellow"
						@endif
						@if($request->breach_status == '1')
							style="background-color:red;color:white;"
						@endif
						>
						<td class="boldmetight">{{$request->id}}</td>
						<td>{{$request->ticket}}</td>
						<td>{{$request->created_at}}</td>
						<td>{{$request->end_date}}</td>
						<td>{{$request->service->name}}</td>
						<td>{{$request->category->name}}</td>
						<td>{{str_limit($request->title,50)}}</td>
						@role('boss|service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
						<td>{{$request->user->IdWithName}}</td>
						@endrole
						<td>{{$request->stage->name}}</td>
						<td>{{$request->updated_at}}</td>
						<td>
							<a class="btn btn-primary myModal" data-route-id="{{$request->id}}" data-toggle="tooltip" data-placement="left" title="View Detail"><span data-route-id="{{$request->id}}" class="glyphicon glyphicon-eye-open"></span></a>
							@if($stage != 'all')
								@role('boss|service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
									@role('service desk')
											@if($request->category->id == 1)
												@if($request->stage->id == 3)
													<a class="btn btn-primary" href="{{ route('requests.ticketcrfshow', $request->id) }}">Tiket</a>
												@elseif($request->stage->id == 17)
													<a class="btn btn-primary" href="{{ route('requests.detailcrfshow', $request->id) }}">Detil</a>
												@endif
											@else
												@if($request->stage->id == 3)
													<a class="btn btn-primary" href="{{ route('requests.editticket', $request->id) }}">Ticket</a>
												@elseif($request->stage->id == 4)
													<a class="btn btn-primary" href="{{ route('requests.editdetail', $request->id) }}">Detil</a>
												@endif
											@endif
									@endrole
									@role('boss')
											@if($request->category->id == 2)
												@if($request->stage->id == 1)
													@if($userId != $request->user_id)
														<a class="btn btn-primary" href="{{ route('requests.approveshow', $request->id) }}">Persetujuan</a>
													@endif
												@elseif($request->stage->id == 2 or $request->stage->id == 4 or $request->stage->id == 5)

												@endif
											@else
												@if($request->stage->id == 1)
													<a class="btn btn-primary" href="{{ route('requests.approveshow', $request->id) }}">Persetujuan</a>
												@elseif($request->stage->id ==  16)
													<a class="btn btn-primary" href="{{ route('requests.ndaapproveshow', $request->id) }}">Persetujuan</a>
												@endif	
											@endif
									@endrole
									@role('operation sd')
										@if($request->stage->id == 2)
											<a class="btn btn-primary" href="{{ route('requests.spsdshow', $request->id) }}">Persetujuan</a> 
										@elseif($request->stage->id == 7)
								
										@endif
									@endrole
									@role('operation ict')
										@if($request->stage->id == 7)
											<a class="btn btn-primary" href="{{ route('requests.approveshow', $request->id) }}">Persetujuan</a>
										@elseif($request->stage->id == 3)
											
										@elseif($request->stage->id == 5)
											<a class="btn btn-primary" href="{{ route('requests.approveshow', $request->id) }}">Persetujuan</a>
										@endif
									@endrole
									@role('so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
										@if($request->category->id ==  2)
											@if($request->stage->id == 3)
												<a  class="btn btn-success" disabled href="#">Success</a>
											@elseif($request->stage->id == 5)
												<a class="btn btn-primary" href="{{ route('requests.approveshow', $request->id) }}">Persetujuan</a>
											@elseif($request->stage->id == 10)
												<a class="btn btn-primary" href="{{ route('requests.editrecomedation', $request->id) }}">Persetujuan</a>
											@endif
										@else
											@if($request->stage->id == 10 or $request->stage->id == 15)
												<a class="btn btn-primary btn-sm" href="{{ route('requests.bpsshow', $request->id) }}">Input Bps</a>
												<a class="btn btn-primary btn-sm" href="{{ route('requests.releasebpsshow', $request->id) }}">Rilis</a>
											@elseif($request->stage->id == 4)
												<a class="btn btn-primary btn-sm" href="{{ route('requests.trasportupdate', $request->id) }}">Konfirmasi</a>
												@elseif($request->stage->id == 18)
												<a class="btn btn-primary btn-sm" href="{{ route('requests.trasportconfirm', $request->id) }}">Transport</a>
											@endif
										@endif
									@endrole
								@endrole
								@role('employee')
									@if($request->stage->id == 6 || $request->stage->id == 5) 
										@if($request->category->id ==  1)
											<form method="POST" action="{{route('requests.employeeapprove', $request->id)}}">
												{{ csrf_field() }}
												{{ method_field('PUT') }}
												<button class="btn btn-primary" type="submit" data-toggle="tooltip" data-placement="left" title="Konfirmasi"><span class="glyphicon glyphicon-pencil"></span></button>
											</form>
										@else
											<a class="btn btn-primary" href="{{ route('requests.srfapproveshow', $request->id) }}" data-toggle="tooltip" data-placement="left" title="Setujui"><span class="glyphicon glyphicon-pencil"></span></a>
											<a class="btn btn-primary" href="{{ route('requests.srfrejectshow', $request->id) }}" data-toggle="tooltip" data-placement="left" title="Tolak"><span class="glyphicon glyphicon-remove"></span></a>
										@endif
									@elseif($request->stage->id == 28)
										<a class="btn btn-primary" href="{{ route('requests.uploaddocumentshow', $request->id) }}" data-toggle="tooltip" data-placement="left" title="Upload Document"><span class="glyphicon glyphicon-pencil"></span></a>
									@endif
								@endrole
								@role('coordinator aplikasi')
									@if($request->category->id ==  1)
										@if($request->stage->id == 13) 
											<a class="btn btn-primary btn-sm" href="{{ route('requests.coordinatorshow', $request->id) }}">Persetujuan</a>
										@endif
									@endif
								@endrole
								@role('chief')
									@if($request->category->id ==  1)
										@if($request->stage->id == 19) 
											<a class="btn btn-primary btn-sm" href="{{ route('requests.chiefupdate', $request->id) }}">Persetujuan</a>
										@endif
									@endif
								@endrole
								@role('manager beict')
									@if($request->category->id ==  1)
										@if($request->stage->id == 12) 
											<a class="btn btn-primary btn-sm" href="{{ route('requests.managerbeictapprove', $request->id) }}">Setujui</a>
											<a class="btn btn-primary btn-sm" href="{{ route('requests.managerreject', $request->id) }}">Tolak</a>
										@endif
									@endif
								@endrole
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modalBro" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body" id="test">
				
			</div>
		</div>
	</div>
</div>
<script>
	function view(id){
		$.colorbox({
			iframe:true, 
			width:"80%", 
			height:"80%",
			transition:'none',
			title: "Preview Data"
			//href:"#"
		});
	}

	jQuery(document).ready(function(){
		jQuery('.myModal').on('click',function(e){
			var id = e.target.getAttribute("data-route-id");
			//alert(id);
			e.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				url: "{{ url('requests/show') }}"+"/"+id,
				method: 'GET',
				data: {
					name: jQuery('#name').val(),
				},
				success: function(result){
					var length = result.requestActions.length;
					var lengthattach = result.requestAttachments.length;
					var lengthso = result.requestSos.length;
					console.log(length);
					var response = 
					"<div class='row'>"+
                    	"<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label>Judul:</label>"+
									"<br/>"+result.request.title+"</div>"+
                            "</div>"+
                        "</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='service_id'>Layanan:</label>"+
								"<br/>"+result.request.service.name+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group' >"+
								"<label for='categories'>Kategori Layanan:</label>"+
								"<br/>"+result.request.category.name+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Kebutuhan Bisnis:</label>"+
								"<br/>"+result.request.business_need+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_benefit'>Manfaat Bisnis:</label>"+
								"<br/>"+result.request.business_benefit+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Peminta:</label>"+
								"<br/>"+result.request.user.name+" ("+result.request.user.id+")</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_benefit'>Contact:</label>"+
								"<br/>Email:"+result.request.user.email+" Telp:"+result.request.telp+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Nomor Tiket Kaseya:</label>"+
								"<br/>"+result.request.ticket+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_benefit'>Detail Pelayanan:</label>"+
								"<br/>"+result.request.detail+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Target Selesai:</label>"+
								"<br/>"+result.request.end_date+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Stage:</label>"+
								"<br/>"+result.request.stage.name+"</div>"+
						"</div>"+
					"</div>";
	if(result.request.category_id == 1)
	{
		response +=
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<div class='panel panel-default'>"+
								"<div class='panel-heading'><b>LAMPIRAN BPS</b></div>"+
								"<div class='panel-body'>"+
									"<div class='table-responsive'>"+
										"<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
											"<tbody class='table-success'>"+
												"<tr>"+
													"<td colspan='12'>";
														for(var i=0; i<lengthso; i++)
														{
															var sobps = result.requestSos[i].request_so_bps.length;
															response += "<tr>"+
																			"<td>"+ result.requestSos[i].service.name +"</td>"+
																		"</tr>";
															for(var u=0; u<sobps; u++)
															{
																response += "<tr>"+
																				"<td>"+ 
																				"<a class='btn btn-primary'"+
																				"href='/itos/storage/"+ result.requestSos[i].request_so_bps[u].attachment +
																				"' target='_blank'>"+
																				"<span class='glyphicon glyphicon-file'></span>File</a>"+
																				"<a href='/itos/storage/"+ result.requestSos[i].request_so_bps[u].attachment +"' target='_blank'> "+ result.requestSos[i].request_so_bps[u].alias +"</a>"+
																				"</td>"+
																			"</tr>";
															}
														}		         
															
								response +=      	"</td>"+
												"</tr>"+
											"</tbody>"+
										"</table>"+
									"</div>"+
								"</div>"+
							"</div>"+
						"</div>"+
					"</div>";
	}
	response +=
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<div class='panel panel-default'>"+
								"<div class='panel-heading'><b>LAMPIRAN USER</b></div>"+
								"<div class='panel-body'>"+
									"<div class='table-responsive'>"+
										"<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
											"<tbody class='table-success'>"+
												"<tr>"+
													"<td colspan='12'>";
														for(var i=0; i<lengthattach; i++)
														{
															response += "<a class='btn btn-primary'"+
																		"href='itos/storage/"+ result.requestAttachments[i].attachment +"' target='_blank'>"+
																		"<span class='glyphicon glyphicon-file'></span>File</a>"+
																		"<a "+
																		"href='/itos/storage/"+ result.requestAttachments[i].attachment +"' target='_blank'>"+ result.requestAttachments[i].alias +"</a><br/><br/>";
														}		         
															
								response +=      	"</td>"+
												"</tr>"+
											"</tbody>"+
										"</table>"+
									"</div>"+
								"</div>"+
							"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<div class='panel panel-default'>"+
								"<div class='panel-heading'><b>RIWAYAT</b></div>"+
								"<div class='panel-body'>"+
									"<div class='table-responsive' style='overflow-y: scroll; height:250px;'>"+
										"<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
											"<tbody class='table-success'>"+
												"<tr>"+
													"<td><b>Tanggal</b></td>"+
													"<td><b>Pengguna</b></td>"+
													"<td><b>Aksi</b></td>"+
												"</tr>"+
												"<tbody class='table-success table tes'>";
													for(var i=0; i<length; i++)
													{
														response += "<tr><td>"+result.requestActions[i].created_at+"</td>"+
																	"<td>"+result.requestActions[i].users.name+"<br>";
														if(result.requestActions[i].request_action_notes !== null)
														{
															response += result.requestActions[i].request_action_notes.note;
														}
														response +=	"</td>"+"<td>"+result.requestActions[i].action.name+"</td></tr>";
														
													}			
										response += 											
												"</tbody>"+
													"</tbody>"+
														"</table>"+
													"</div>"+
												"</div>"+
											"</div>"+
										"</div>"+
									"</div>";
		$("#test").html(response);
		jQuery('#modalBro').modal();
			}});
		});
	});
</script>
@endsection
