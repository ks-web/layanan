@extends('layouts.app')
@section('content')
<ul class="nav nav-tabs">
	@role('operation sd|boss|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging|manager beict')
		@if($stage == '')
			<li role="presentation" class="active">
				<a href="{{ route('requests.index') }}">Permintaan Saya
					@if($countWaitingApproved > 0)
						<span class="badge badge-warning">{{$countWaitingApproved}}</span>
					@endif
				</a>
			</li>
			<li role="presentation" class="">
				<a href="{{ route('requests.viewperstage', 'breach') }}">Breach
					@if($countBreach > 0)
						<span class="badge badge-warning">{{$countBreach}}</span>
					@endif
				</a>
			</li>
			<li role="presentation" >
				<a href="{{ route('requests.viewperstage', 'all') }}">Semua
					@if($countAll > 0)
						<span class="badge badge-warning">{{$countAll}}</span>
					@endif
				</a>
			</li>
		@else
			<li role="presentation"
				@if($stage == '10')
					class="active"
				@endif>
				<a href="{{ route('requests.index') }}">Menunggu Persetujuan
					@if($countWaitingApproved > 0)
						<span class="badge badge-warning">{{$countWaitingApproved}}</span>
					@endif
				</a>
			</li>
			<li role="presentation" 
				@if($stage == 'breach')
					class="active"
				@endif>
					<a href="{{ route('requests.viewperstage', 'breach') }}">Breach
					@if($countBreach > 0)
						<span class="badge badge-warning">{{$countBreach}}</span>
					@endif
				</a>
			</li>
			<li role="presentation" 
				@if($stage == 'all')
					class="active"
				@endif>
				<a href="{{ route('requests.viewperstage', 'all') }}">Semua
					@if($countAll > 0)
						<span class="badge badge-warning">{{$countAll}}</span>
					@endif
				</a>
			</li>
		@endif
	@endrole
	@role('coordinator aplikasi|coordinator infra')
		@if($stage == 'all')
			<li role="presentation">
				<a href="{{ route('requests.index') }}">Menunggu Persetujuan
					@if($countWaitingApproved > 0)
						<span class="badge badge-warning">{{$countWaitingApproved}}</span>
					@endif
				</a>
			</li>
			<li role="presentation" class="active">
				<a href="{{ route('requests.viewperstage', 'all') }}">Semua 
					@if($all > 0)
						<span class="badge badge-warning">{{$all}}</span>
					@endif
				</a>
			</li>
		@else
			<li role="presentation" class="active">
				<a href="{{ route('requests.index') }}">Menunggu Persetujuan
					@if($countWaitingApproved > 0)
						<span class="badge badge-warning">{{$countWaitingApproved}}</span>
					@endif
				</a>
			</li>
			<li role="presentation">
				<a href="{{ route('requests.viewperstage', 'all') }}">Semua
					@if($all > 0)
						<span class="badge badge-warning">{{$all}}</span>
					@endif
				</a>
			</li>
		@endif
	@endrole
</ul>
<br/>
@role('employee')
	<a href="{{route('requests.create')}}" class="control">
		<button class="btn btn-primary">Buat Permintaan</button>
	</a>
@endrole
<p>&nbsp;</p>
@if (session('success'))
	<div class="alert alert-success" role="alert">
		{{ session('success') }}
	</div>
@endif
<div class="panel panel-default">
  	<div class="panel-heading">Data Permintaan Layanan</div>
	<div class="panel-body">
		<table id="dataTables" class="display" cellspacing="0" width="100%">
			<thead class="table-success">
				<tr>
					<th class="center" width="3%"> ID </th>
					<th width="6%">TIKET</th>
					<th width="15%">TANGGAL</th>
					<th width="10%">TARGET SELESAI</th>
					<th width="15%">LAYANAN</th>
					<th width="15%">KATEGORI</th>
					<th width="15%">JUDUL</th>
					{{-- <th width="15%">ALASAN PERMINTAAN</th>
					<th width="15%">MANFAAT TERHADAP BISNIS</th> --}}
					@role('boss|service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
						<th width="10%">PEMINTA</th>
					@endrole
					<th width="15%">TAHAP</th>
					<th width="15%">TANGGAL UBAH</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($paginated as $requestSo)
					@if(!empty($requestSo->request->id))
					<tr>
						<td class="boldmetight" 
							@if($requestSo->request->breach_status == '1')
								style="background-color:red;color:white;"
							@endif
						>{{$requestSo->request->id}}</td>
						<td>{{$requestSo->request->ticket}}</td>
						<td>{{$requestSo->request->created_at}}</td>
						<td>{{$requestSo->request->end_date}}</td>
						<td>{{$requestSo->request->service->name}}</td>
						<td>{{$requestSo->request->category->name}}</td>
						<td>{{str_limit($requestSo->request->title,50)}}</td>
						@role('boss|service desk|operation sd|operation ict|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
						<td>{{$requestSo->request->user->IdWithName}}</td>
						@endrole
						<td>{{$requestSo->request->stage->name}}</td>
						<td>{{$requestSo->request->updated_at}}</td>
						<td>
							<a class="btn btn-primary myModal" data-route-id="{{$requestSo->request->id}}" ><span data-route-id="{{$requestSo->request->id}}" class="glyphicon glyphicon-eye-open"></span></a>
							@if($stage != 'all')
								@role('so web|so mes flat|so mes long|so sap hr|so sap ppqm|operation ict|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')
									@if($requestSo->request->category->id ==  2)
										@if($requestSo->request->stage->id == 3)
											
										@elseif($requestSo->request->stage->id == 5)
											{{-- <a class="btn btn-primary" href="{{ route('requests.approveshow', $requestSo->request->id) }}">Persetujuan</a> --}}
										@elseif($requestSo->request->stage->id == 10)
											<a class="btn btn-primary" href="{{ route('requests.editrecomedation', $requestSo->request->id) }}" data-toggle="tooltip" data-placement="left" title="Aksi"><span class="glyphicon glyphicon-pencil"></span></a>
											{{-- <a class="btn btn-primary" href="{{ route('requests.waitingdocumentshow', $requestSo->request->id) }}">Note</a> --}}
										@endif
									@else
										@if($requestSo->request->stage->id == 10 )
											@if($requestSo->request->service_id == $requestSo->service_id)
												<a class="btn btn-primary" href="{{ route('requests.bpsshow', [$requestSo->request->id, $requestSo->service_id]) }}" data-toggle="tooltip" data-placement="left" title="Upload bps"><span class="glyphicon glyphicon-upload"></span></a>

												<a class="btn btn-primary" href="{{ route('requests.choosesoshow', [$requestSo->request->id,$requestSo->service_id]) }}" data-toggle="tooltip" data-placement="left" title="Pilih So"><span class="glyphicon glyphicon-list-alt"></span></a>
											@else
												<a class="btn btn-primary" href="{{ route('requests.bpsshow', [$requestSo->request->id, $requestSo->service_id]) }}" data-toggle="tooltip" data-placement="left" title="Upload bps"><span class="glyphicon glyphicon-upload"></span></a>
											@endif
										@elseif($requestSo->request->stage->id == 15 or $requestSo->request->stage->id == 14)
											@if($requestSo->request->service_id == $requestSo->service_id)
												<a class="btn btn-primary" href="{{ route('requests.bpsshow', [$requestSo->request->id, $requestSo->service_id]) }}" data-toggle="tooltip" data-placement="left" title="Upload bps"><span class="glyphicon glyphicon-upload"></span></a>

												<a class="btn btn-primary" href="{{ route('requests.choosesoshow', [$requestSo->request->id,$requestSo->service_id]) }}" data-toggle="tooltip" data-placement="left" title="Pilih So"><span class="glyphicon glyphicon-list-alt"></span></a>

												<form method="POST" action="{{route('requests.release', [$requestSo->request->id, $requestSo->service_id])}}">
													{{ csrf_field() }}
													{{ method_field('PUT') }}
													<button class="btn btn-danger" type="submit" data-toggle="tooltip" data-placement="left" title="Release"><span class="glyphicon glyphicon-log-in"></span></button>
												</form>
											@else
												<a class="btn btn-primary" href="{{ route('requests.bpsshow', [$requestSo->request->id, $requestSo->service_id]) }}" data-toggle="tooltip" data-placement="left" title="Upload bps"><span class="glyphicon glyphicon-upload"></span></a>
											@endif
										@elseif($requestSo->request->stage->id == 13)
											@if($requestSo->request->service_id == $requestSo->service_id)
												<form method="POST" action="{{route('requests.unrelease', [$requestSo->request->id, $requestSo->service_id])}}">
													{{ csrf_field() }}
													{{ method_field('PUT') }}
													<button class="btn btn-danger" type="submit" data-toggle="tooltip" data-placement="left" title="Rilis Bps"><span class="glyphicon glyphicon-log-out"></button>
												</form>
											@endif
										@elseif($requestSo->request->stage->id == 21)
											<a class="btn btn-primary" href="{{ route('requests.trasportconfirm', $requestSo->request->id) }}">Transport</a>
										@endif
									@endif
								@endrole
								@role('coordinator aplikasi|coordinator infra')
									@if($requestSo->request->category->id ==  1)
										@if($requestSo->request->stage->id == 13)
											@if($requestSo->status == 1)
												<a class="btn btn-primary" href="{{ route('requests.coshow', $requestSo->request->id) }}" data-toggle="tooltip" data-placement="left" title="Persetujuan"><span class="glyphicon glyphicon-pencil"></span></a>
											@endif
										@endif
									@endif
								@endrole
							@endif
						</td>
					</tr>
					@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modalBro" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body" id="test">
				
			</div>
		</div>
	</div>
</div>
<script>
	function view(id){
		$.colorbox({
			iframe:true, 
			width:"80%", 
			height:"80%",
			transition:'none',
			title: "Preview Data"
			//href:"#"
		});
	}

	jQuery(document).ready(function(){
		jQuery('.myModal').on('click',function(e){
			var id = e.target.getAttribute("data-route-id");
			//alert(id);
			e.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			jQuery.ajax({
				url: "{{ url('requests/show') }}"+"/"+id,
				method: 'GET',
				data: {
					name: jQuery('#name').val(),
				},
				success: function(result){
					var length = result.requestActions.length;
					var lengthattach = result.requestAttachments.length;
					var lengthso = result.requestSos.length;
					console.log(length);
					var response = 
					"<div class='row'>"+
                    	"<div class='col-lg-6'>"+
                                "<div class='form-group'>"+
                                    "<label>Judul:</label>"+
									"<br/>"+result.request.title+"</div>"+
                            "</div>"+
                        "</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='service_id'>Layanan:</label>"+
								"<br/>"+result.request.service.name+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group' >"+
								"<label for='categories'>Kategori Layanan:</label>"+
								"<br/>"+result.request.category.name+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Kebutuhan Bisnis:</label>"+
								"<br/>"+result.request.business_need+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_benefit'>Manfaat Bisnis:</label>"+
								"<br/>"+result.request.business_benefit+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Peminta:</label>"+
								"<br/>"+result.request.user.name+" ("+result.request.user.id+")</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_benefit'>Contact:</label>"+
								"<br/>Email: "+result.request.user.email+"   Telp: "+result.request.telp+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Nomor Tiket Kaseya:</label>"+
								"<br/>"+result.request.ticket+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_benefit'>Detail Pelayanan:</label>"+
								"<br/>"+result.request.detail+"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Target Selesai:</label>"+
								"<br/>"+result.request.end_date+"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='form-group'>"+
								"<label for='business_need'>Stage:</label>"+
								"<br/>"+result.request.stage.name+"</div>"+
						"</div>"+
					"</div>";
	if(result.request.category_id == 1)
	{
		response +=
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<div class='panel panel-default'>"+
								"<div class='panel-heading'><b>LAMPIRAN BPS</b></div>"+
								"<div class='panel-body'>"+
									"<div class='table-responsive'>"+
										"<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
											"<tbody class='table-success'>"+
												"<tr>"+
													"<td colspan='12'>";
														for(var i=0; i<lengthso; i++)
														{
															var sobps = result.requestSos[i].request_so_bps.length;
															response += "<tr>"+
																			"<td>"+ result.requestSos[i].service.name +"</td>"+
																		"</tr>";
															for(var u=0; u<sobps; u++)
															{
																response += "<tr>"+
																				"<td>"+ 
																				"<a class='btn btn-primary'"+
																				"href='/itos/storage/"+ result.requestSos[i].request_so_bps[u].attachment +
																				"' target='_blank'>"+
																				"<span class='glyphicon glyphicon-file'></span>File</a>"+
																				"<a href='/itos/storage/"+ result.requestSos[i].request_so_bps[u].attachment +"' target='_blank'> "+ result.requestSos[i].request_so_bps[u].alias +"</a>"+
																				"</td>"+
																			"</tr>";
															}
														}		         
															
								response +=      	"</td>"+
												"</tr>"+
											"</tbody>"+
										"</table>"+
									"</div>"+
								"</div>"+
							"</div>"+
						"</div>"+
					"</div>";
	}
	response +=
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<div class='panel panel-default'>"+
								"<div class='panel-heading'><b>LAMPIRAN USER</b></div>"+
								"<div class='panel-body'>"+
									"<div class='table-responsive'>"+
										"<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
											"<tbody class='table-success'>"+
												"<tr>"+
													"<td colspan='12'>";
														for(var i=0; i<lengthattach; i++)
														{
															response += "<a class='btn btn-primary'"+
																		"href='/itos/storage/"+ result.requestAttachments[i].attachment +"' target='_blank'>"+
																		"<span class='glyphicon glyphicon-file'></span>File</a>"+
																		"<a "+
																		"href='/itos/storage/"+ result.requestAttachments[i].attachment +"' target='_blank'>"+ result.requestAttachments[i].alias +"</a><br/><br/>";
														}		         
															
								response +=      	"</td>"+
												"</tr>"+
											"</tbody>"+
										"</table>"+
									"</div>"+
								"</div>"+
							"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<div class='panel panel-default'>"+
								"<div class='panel-heading'><b>RIWAYAT</b></div>"+
								"<div class='panel-body'>"+
									"<div class='table-responsive' style='overflow-y: scroll; height:250px;'>"+
										"<table class='table table-striped table-responsive' cellspacing='0' width='100%'>"+
											"<tbody class='table-success'>"+
												"<tr>"+
													"<td><b>Tanggal</b></td>"+
													"<td><b>Pengguna</b></td>"+
													"<td><b>Aksi</b></td>"+
												"</tr>"+
												"<tbody class='table-success table tes'>";
													for(var i=0; i<length; i++)
													{
														response += "<tr><td>"+result.requestActions[i].created_at+"</td>"+
																	"<td>"+result.requestActions[i].users.name+"<br>";
														if(result.requestActions[i].request_action_notes !== null)
														{
															response += result.requestActions[i].request_action_notes.note;
														}
														response +=	"</td>"+"<td>"+result.requestActions[i].action.name+"</td></tr>";
														
													}			
										response += 											
												"</tbody>"+
													"</tbody>"+
														"</table>"+
													"</div>"+
												"</div>"+
											"</div>"+
										"</div>"+
									"</div>";
		$("#test").html(response);
		jQuery('#modalBro').modal();
			}});
		});
	});
</script>
@endsection
