@extends('layouts.app')
@section('content')
<script>
       function formFalidate() {
            var title = $('#title').val();	
            var idservice = $('#idservice').val();			
            var idcateories = $('#idcategories').val();
            var business_need = $('#business_need').val();
            var business_benefit = $('#business_benefit').val();
            var keterangan = $('#keterangan').val();
            var attachment  = $('#attachment').val();
            var telp  = $('#telp').val();
            var location  = $('#location').val();
            var nda = $('#nda').val();

			if (title == ""){
                $('.titlevalidasi').html("<strong>Tidak boleh kosong.</strong>");
                return false;
            }
            else if(idservice == ""){
                $('.validasiserviceid').html("<strong>Tidak boleh kosong.</strong>");	
                return false;		
            }
            else if(idcateories == ""){
                $('.validasicategory').html("<strong>Tidak boleh kosong.</strong>");
                return false;
            }
            else if(business_need.trim() == ""){
                $('.validasibusiness_need').html("<strong>Tidak boleh kosong.</strong>");
                return false;			
            }
            else if(business_benefit.trim() == ""){
                $('.validasibusiness_benefit').html("<strong>Tidak boleh kosong.</strong>");	
                return false;			
            }
            else if(keterangan.trim() == ""){
                $('.validasiketerangan').html("<strong>Tidak boleh kosong.</strong>");	
                return false;			
            }
            else if(telp.trim() == ""){
                $('.validasitelp').html("<strong>Tidak boleh kosong.</strong>");	
                return false;			
            }
            else if(location.trim() == ""){
                $('.validasilocation').html("<strong>Tidak boleh kosong.</strong>");	
                return false;			
            }
            else if(attachment.trim() == ""){
                $('.validasiattachment').html("<strong>Tidak boleh kosong.</strong>");	
                return false;			
            }
            else {
                $('#myModal').modal('show');
                return false;
            }
            
		}

        function formFalidateCrf() {
            var title = $('#title').val();	
            var idservice = $('#idservice').val();			
            var idcateories = $('#idcategories').val();
            var business_need = $('#business_need').val();
            var business_benefit = $('#business_benefit').val();
            var keterangan = $('#keterangan').val();
            var telp  = $('#telp').val();
            var location  = $('#location').val();
            var nda = $('#nda').val();

			if (title == ""){
                $('.titlevalidasi').html("<strong>Tidak boleh kosong.</strong>");
                return false;
            }
            else if(idservice == ""){
                $('.validasiserviceid').html("<strong>Tidak boleh kosong.</strong>");	
                return false;		
            }
            else if(idcateories == ""){
                $('.validasicategory').html("<strong>Tidak boleh kosong.</strong>");
                return false;
            }
            else if(business_need.trim() == ""){
                $('.validasibusiness_need').html("<strong>Tidak boleh kosong.</strong>");	
                return false;		
            }
            else if(business_benefit.trim() == ""){
                $('.validasibusiness_benefit').html("<strong>Tidak boleh kosong.</strong>");	
                return false;		
            }
            else if(telp.trim() == ""){
                $('.telpvalidasi').html("<strong>Tidak boleh kosong.</strong>");	
                return false;		
            }
            else if(location.trim() == ""){
                $('.locationvalidasi').html("<strong>Tidak boleh kosong.</strong>");	
                return false;		
            }
		}

        $(document).ready(function(){
            $('#simpan').hide();
            $('#simpan1').hide();
            // $('#lanjut').hide();

            $('#nda').change(function(){
                if(this.checked){
                    $('#simpan').show();
                }
                else{
                    $('#simpan').hide()
                }
            })
            
            $('#simpan').click(function() {
                $(this).attr('disabled','disabled');
                $(this).attr('value', 'Sedang mengirim...')
                $('#formField').submit();
            });

            // $('#simpan').click(function(){
            //     $('#simpan').attr('disabled','disabled');
            // });

            // $("#idcategories").click(function(){
            //     var id = $("#idcategories").val();
            //     if(id == 2){
            //         $('#lanjut').show();
            //         $('#simpan1').hide();
            //     }
            //     else if(id == 1){
            //         $('#simpan1').show();
            //         $('#lanjut').hide();
            //     }
            // });
            
        });
</script>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Layanan</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form id="formField" class="form" method="POST" action="{{ route('requests.store') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input style="color:black;" readonly type="hidden"  name="golongan" class="form-control {{ $errors->has('golongan') ? ' is-invalid' : '' }}" autofocus value="{{ $golongan }}" />

                        <div class="form-group">
                            @if($golongan != 'A')
                            <label for="boss">Pengesahan oleh atasan</label>
                                <input style="color:black;" readonly type="text"  name="boss" class="form-control {{ $errors->has('boss') ? ' is-invalid' : '' }}" autofocus value="{{Auth::user()->boss()->name}} ({{Auth::user()->boss()->id}})" />
                                <span class="invalid-feedback " role="alert">
                                    <strong>{{ $errors->first('boss') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="title">Judul</label>
                            <input style="color:black;" id="title" name="title" rows="2" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" autofocus value="{{ old('title') }}" />
                            <span class="invalid-feedback titlevalidasi" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="service_id">Layanan</label>
                            <select style="color:black;" require id="idservice" name="service_id" class="form-control {{ $errors->has('service_id') ? ' is-invalid' : '' }}">
                                    <option value="">Pilih Layanan</option>
                                    <optgroup label="Layanan aplikasi mes">
                                        @foreach ($services as $service)
                                            @if($service->groupserv == "mes")
                                                <option value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                                     {{-- <optgroup label="Layanan aplikasi sap">
                                        @foreach ($services as $service)
                                            @if($service->groupserv == "sap")
                                                <option value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup>  --}}
                                    <optgroup label="Layanan aplikasi lainya">
                                        @foreach ($services as $service)
                                            @if($service->groupserv != "mes" && $service->groupserv != "sap")
                                                <option value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                            </select>
                            <span class="invalid-feedback validasiserviceid" role="alert">
                                <strong>{{ $errors->first('service_id') }}</strong>
                            </span>
                        </div>
                        <div class="form-group" >
                            <label for="category">Kategori Layanan</label>
                            <select style="color:black;" require id="idcategories" name="category" class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}">
                            </select>
                            <span class="invalid-feedback validasicategory" role="alert">
                                <strong>{{ $errors->first('category') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_need">Alasan Permintaan</label>
                            <textarea style="color:black;" require name="business_need" id="business_need" rows="6" class="form-control {{ $errors->has('business_need') ? ' is-invalid' : '' }}" autofocus>{{ old('business_need') }}</textarea>
                            <span class="invalid-feedback validasibusiness_need" role="alert">
                                <strong>{{ $errors->first('business_need') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_benefit">Manfaat Terhadap Bisnis</label>
                            <textarea style="color:black;" id=business_benefit name="business_benefit" rows="6" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ old('business_benefit') }}</textarea>
                            <span class="invalid-feedback validasibusiness_benefit" role="alert">
                                <strong>{{ $errors->first('business_benefit') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan tambahan ( jenis aplikasi, level akses, dll)</label>
                            <textarea style="color:black;" id="keterangan" name="keterangan" rows="4" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ old('keterangan') }}</textarea>
                            <span class="invalid-feedback validasiketerangan" role="alert">
                                <strong>{{ $errors->first('keterangan') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="telp">Nomor Telp</label>
                            <input style="color:black;" id="telp" name="telp" rows="2" class="form-control {{ $errors->has('telp') ? ' is-invalid' : '' }}" autofocus value="{{ old('telp') }}" />
                            <span class="invalid-feedback validasitelp" role="alert">
                                <strong>{{ $errors->first('telp') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="location">Lokasi</label>
                            <input style="color:black;" id="location" name="location" rows="2" class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }}" autofocus value="{{ old('location') }}" />
                            <span class="invalid-feedback validasilocation" role="alert">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                        </div>
                        {{-- <div class="form-group">
                            <div class="col-md-12">
                                <label for="customFile">Lampiran</label>
                                <input type="file" class="form-control {{ $errors->has('attachment') ? 'is-invalid' : '' }}" id="customFile" name="attachment">
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('attachment') }}</strong>
                                </span>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <label for="reason">Lampiran: Format file ( image/pdf ) ( Contoh: Job desk, dll )</label>
                            <div class="input-group control-group increment">
                                <div class="custom-file">
                                    <input style="color:black;" class="custom-file-input" id="attachment" type="file" name="attachment[]" class="form-control">
                                </div>
                                <div class="input-group-btn"> 
                                    <button style="color:black;" class="btn btn-success tambah1" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                </div>
                            </div>
                            <div>
                                <span class="invalid-feedback validasiattachment" role="alert">
                                    <strong>{{ $errors->first('attachment') }}</strong>
                                </span>
                            </div>
                            <div class="clone hide">
                                <div class="control-group input-group" style="margin-top:10px">
                                    <div class="custom-file">
                                        <input class="custom-file-input" type="file" name="attachment[]" class="form-control">
                                    </div>
                                    <div class="input-group-btn"> 
                                        <button class="btn btn-danger kurang1" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="nda" name="nda" value='1' required >
                                <label class="custom-control-label" for="customCheck1">Kebijakan dan Aturan Penggunaan Layanan <span type="button" class="badge badge-danger" data-toggle="modal" data-target="#myModal">Show NDA</span></label>
                            </div>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nda') }}</strong>
                            </span>
                        </div> --}}
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Kebijakan dan Aturan</h4>
                                </div>
                                <div class="modal-body">
                                    Pengguna layanan harus:
                                    <br/>
                                    <br/>
                                    <ol>
                                        <li>Mentaati dan mematuhi seluruh peraturan, regulasi dan perundang-undangan yang mengatur tentang data, informasi dan perangkat elektronik yang berlaku di Negara Kesatuan Republik Indonesia.</li>
                                        <li>Mentaati kebijakan penggunaan layanan jaringan intranet PT Krakatau Steel (Persero) Tbk.</li>
                                        <li>Mentaati kebijakan penggunaan userid dan password di PT Krakatau Steel (Persero) Tbk.</li>
                                        <li>Menjaga rahasia PT Krakatau Steel (Persero) Tbk.terkait data dan informasi yang didapat dengan adanya penggunaan jaringan intranet PT Krakatau Steel (Persero) Tbk. ini.</li>
                                        <li>Menjaga keamanan data dan informasi yang saya dapatkan dengan adanya penggunaan perangkat pribadi di jaringan PT Krakatau Steel (Persero) Tbk.</li>
                                        <li>Tidak melakukan instalasi perangkat lunak hasil bajakan atau perangkat lunak hasil cracking pada perangkat pribadi tersebut (baik saya lakukan sendiri atau dengan dengan bantuan pihak lain).</li>
                                    </ol>
                                </div>
                                <div class="modal-footer">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="nda" id="nda" value="1" /><label class="custom-control-label" for="customCheck1">Kebijakan dan Aturan Penggunaan Layanan</label>
                                    </div>
                                    <div class="form-group" style="margin-top:1em">
                                        <div role="group">
                                            <input id="simpan" type="submit" class="btn btn-primary" value="Simpan" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                <button id="lanjut" onclick="return formFalidate()" class="btn btn-primary" >
                                    Lanjutkan
                                </button>
                                <button id="simpan1" onclick="return formFalidateCrf()" class="btn btn-primary" >
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection