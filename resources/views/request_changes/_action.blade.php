{{-- begin if role boss --}}
@role('boss')
    {{-- approval --}}
    @if($rc->stage_id == 1)
        <a class="btn btn-primary btn-sm" href="{{ route('crf.form.boss.approval', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="Approval form"><span class="glyphicon glyphicon-pencil"></span></a>
    @endif
@endrole
{{-- end role operation boss --}}

{{-- begin of role peration sd  --}}
@role('operation sd')
    @if($rc->stage_id == 2)
        {{-- escalation --}}
        <a class="btn btn-primary btn-sm" href="{{ route('crf.so.form', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="Escalation form"><span class="glyphicon glyphicon-pencil"></span></a>
    @endif
@endrole
{{-- end role operation sd --}}

{{-- begin of role service owner --}}
@role('boss|operation sd|so mes flat|so web|so mes long|so sap hr|so sap ppqm|operation ict|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging')

@if(isset($rc->stage->id))
        @if($rc->stage_id  == 10)
            {{-- <a class="btn btn-primary btn-sm" href="{{ route('crf.so.form.bps', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="Input Bps"><span class="glyphicon glyphicon-pencil"></span></a> --}}

            <a class="btn btn-primary btn-sm" href="{{ route('crf.so.form', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="approve crf"><span class="glyphicon glyphicon-pencil"></span></a>
        @elseif($rc->stage_id == 14)
            <a class="btn btn-primary btn-sm" href="{{ route('crf.so.form.bps', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="create new bps"><span class="glyphicon glyphicon-plus"></span></a>
        @endif
        
    @endif
@endrole
{{-- end of role service owner --}}

{{-- begin of roler service desk --}}
{{--  --}}
@role('service desk')
    @if($rc->stage_id == 3)
        <a class="btn btn-primary btn-sm" href="{{ route('crf.servicedesk.ticket', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="Input Ticket"><span class="glyphicon glyphicon-pencil"></span></a>
    @endif
    {{-- <a data-url="{{ route('crf.show.bps', $rc->id) }}" class="btn btn-success btn-sm id-modal"><span  data-url="{{ route('crf.show.bps', $rc->id) }}" class="glyphicon glyphicon-eye-open"></span></a> --}}
@endrole
{{-- end of role service desk --}}

{{-- begin of coordinator so role --}}
@role('coordinator aplikasi|coordinator infra')
    @if($rc->stage_id == 13)
    <form method="POST" action="{{route('crf.co.approval', $rc->id)}}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <button class="btn btn-primary btn-sm" type="submit"><span class="glyphicon glyphicon-ok"></span></a></button>
    </form>
    {{-- <a data-url="{{ route('crf.show.bps', $rc->id) }}" class="btn btn-success btn-sm id-modal"><span  data-url="{{ route('crf.show.bps', $rc->id) }}" class="glyphicon glyphicon-eye-open"></span></a> --}}
    @endif
@endrole
{{-- end of coordinator service owner --}}

@role('manager beict')
    @if($rc->stage_id == 12 and $rc->action_id == 3)
        <a class="btn btn-primary btn-sm" href="{{ route('crf.form.reject', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="Approval"><span class="glyphicon glyphicon-pencil"></span></a>
    @elseif($rc->stage_id == 12 and $rc->action_id != 3)
        <a class="btn btn-primary btn-sm" href="{{ route('crf.form.approval', $rc->id) }}" data-toggle="tooltip" data-placement="left" title="Approval"><span class="glyphicon glyphicon-pencil"></span></a>
    @endif
   
@endrole