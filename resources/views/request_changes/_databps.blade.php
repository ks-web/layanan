@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Data Customer Request Form</b></div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('crf.bps.approval', $requestchange->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{ isset($requestchange->requestChange->title) ? $requestchange->requestChange->title : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ isset($requestchange->requestChange->business_need) ? $requestchange->requestChange->business_need : ''}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ isset($requestchange->requestchange->business_benefit) ? $requestchange->requestchange->business_benefit : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ isset($requestchange->requestChange->user->name) ? $requestchange->requestChange->user->name : ''}} ( {{ isset($requestchange->requestChange->user->id) ? $requestchange->requestChange->user->id :'' }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ isset($requestchange->requestChange->user->email) ? $requestchange->requestChange->user->email : '' }}, Telp: {{ isset($requestchange->telp) ? $requestchange->telp: '' }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan: </label>
                                    <br/>
                                    {{ isset($requestchange->requestChange->stage->name) ? $requestchange->requestChange->stage->name : '' }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Nomor Tiket: </label>
                                    <br/>
                                    {{ isset($requestchange->ticket_no) ? $requestchange->ticket_no:''}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($requestchange->requestChange->requestChangeAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Data Bussiness Prcess Specification</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Priority: </label>
                                                <br/>
                                                {{ isset($requestchange->priority->name) ? $requestchange->priority->name : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Difficulty: </label>
                                                <br/>
                                                {{ isset($requestchange->difficulty->name) ? $requestchange->difficulty->name : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Prepared By: </label>
                                                <br/>
                                                {{ isset($requestchange->preparedby) ? $requestchange->preparedby : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Module: </label>
                                                <br/>
                                                {{ isset($requestchange->module) ? $requestchange->module : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Email: </label>
                                                <br/>
                                                {{ isset($requestchange->email) ? $requestchange->email : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Phone: </label>
                                                <br/>
                                                {{ isset($requestchange->phone) ? $requestchange->phone : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Start Plan Data: </label>
                                                <br/>
                                                {{ isset($requestchange->start_plan_date) ? $requestchange->start_plan_date : '' }}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="reason">Finish Plan Data: </label>
                                                <br/>
                                                {{ isset($requestchange->finish_plan_date) ? $requestchange->finish_plan_date : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Business Process Justification: </label>
                                                <br/>
                                                {{ isset($requestchange->bpj) ? $requestchange->bpj : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Global Design: </label>
                                                <br/>
                                                {{ isset($requestchange->bpj) ? $requestchange->bpj : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Detail Specification: </label>
                                                <br/>
                                                {{ isset($requestchange->detail_specification) ? $requestchange->detail_specification : '' }}
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="reason">Effort: </label>
                                                <br/>
                                                {{ isset($requestchange->effort) ? $requestchange->effort : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>Time Duration Calculation</b></div>
                                    <div class="panel-body">
                                        <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                            <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                <thead class="table-success">
                                                    <tr>
                                                        <td><b>Activity</b></td>                                                    
                                                        <td><b>Mandays</b></td>
                                                        <td><b>Pic</b></td>                                                   
                                                        <td><b>Level</b></td>                                                    
                                                        <td><b>Start Date</b></td>                                                   
                                                        <td><b>Finish Date</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody class="table-success">	
                                                    @foreach ($requestchange->requestChangeBpsDurations as $item)
                                                        <tr>
                                                            <td>{{ isset($item->activity) ? $item->activity : '' }}</td>
                                                            <td>{{ isset($item->mandays) ? $item->mandays : '' }}</td>                  
                                                            <td>{{ isset($item->pic) ? $item->pic : ''}}</td>                         
                                                            <td>{{ isset($item->level_id) ? $item->level_id : ''}}</td>
                                                            <td>{{ isset($item->start_date) ? $item->start_date : ''}}</td>
                                                            <td>{{ isset($item->finish_date) ? $item->finish_date : ''}}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection