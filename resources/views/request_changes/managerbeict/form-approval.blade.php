@extends('layouts.app')
@section('content')
<script>
    function myFunction() {
        var copyText = document.getElementById("bussiness_note");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }
    
    function copyTitle() {
        var copyText = document.getElementById("title");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyBusinessBenefit() {
        var copyText = document.getElementById("business_benefit");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyKeterangan() {
        var copyText = document.getElementById("keterangan");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }
    
    function ceking(isi){
        if(isi == 2){
            $('#aksi').val(isi);
            $('#myModal').modal('show');
            return false;
        }
        else{
            $('#aksi').val(isi);
            return true;
        }
    }

    function simpanData(){
        alert($('#aksi').val());
        if($('#attachment').val() == ''){
            $('.attachment').html("<strong>Tidak boleh kosong.</strong>");	
            return false;		
        }
        else {
            $('#formField').submit();
        }
    }

    $('#demodate').datetimepicker({
        inline:true,
    });
</script>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form permintaan layanan (CRF)</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('crf.beict.approval', $requestchange->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        {{-- judul dan kebutuhan bisnis --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$requestchange->title}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_need }}
                                </div>
                            </div>
                        </div>

                        {{-- manfaat bisnis dan user yang mengajukan permintaan --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_benefit }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $requestchange->user->name }} ( {{ $requestchange->user->id }} )
                                </div>
                            </div>
                        </div>

                        {{-- email dan tahapan --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $requestchange->user->email }}, Telp: {{ $requestchange->telp }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan: </label>
                                    <br/>
                                    {{ $requestchange->stage->name }}
                                </div>
                            </div>
                        </div>
                        
                        {{-- nomor tiket --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Nomor Tiket: </label>
                                    <br/>
                                    {{ $requestchange->ticket_no }}
                                </div>
                            </div>
                        </div>

                        {{-- note --}}
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea style="color:black;" id=note name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus>{{ $requestchange->note }}</textarea>
                            <span class="invalid-feedback validasinote" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div>

                        {{-- history --}}
                        @include('request_changes._riwayat')
                        
                        {{-- <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($requestchange->requestChange->requestChangeAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>Time Duration Calculation</b></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Priority: </label>
                                                    <br/>
                                                    {{ $requestchange->priority->name }}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Difficulty: </label>
                                                    <br/>
                                                    {{ $requestchange->difficulty->name }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Prepared By: </label>
                                                    <br/>
                                                    {{ $requestchange->preparedby }}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Module: </label>
                                                    <br/>
                                                    {{ $requestchange->module }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Email: </label>
                                                    <br/>
                                                    {{ $requestchange->email }}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Phone: </label>
                                                    <br/>
                                                    {{ $requestchange->phone }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Start Plan Data: </label>
                                                    <br/>
                                                    {{ $requestchange->start_plan_date }}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="reason">Finish Plan Data: </label>
                                                    <br/>
                                                    {{ $requestchange->finish_plan_date }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reason">Business Process Justification: </label>
                                                    <br/>
                                                    {{ $requestchange->bpj }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reason">Global Design: </label>
                                                    <br/>
                                                    {{ $requestchange->bpj }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reason">Detail Specification: </label>
                                                    <br/>
                                                    {{ $requestchange->detail_specification }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reason">Effort: </label>
                                                    <br/>
                                                    {{ $requestchange->effort }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>Time Duration Calculation</b></div>
                                    <div class="panel-body">
                                        <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                            <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                <thead class="table-success">
                                                    <tr>
                                                        <td><b>Activity</b></td>                                                    
                                                        <td><b>Mandays</b></td>
                                                        <td><b>Pic</b></td>                                                   
                                                        <td><b>Level</b></td>                                                    
                                                        <td><b>Start Date</b></td>                                                   
                                                        <td><b>Finish Date</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody class="table-success">	
                                                    @foreach ($requestchange->requestChangeBpsDurations as $item)
                                                        <tr>
                                                            <td>{{$item->activity}}</td>                                
                                                            <td>{{$item->mandays}}</td>                                                      
                                                            <td>{{$item->pic}}</td>                                                        
                                                            <td>{{$item->level_id}}</td>
                                                            <td>{{$item->start_date}}</td>
                                                            <td>{{$item->finish_date}}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea style="color:black;" id=note name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus>{{ $requestchange->note }}</textarea>
                            <span class="invalid-feedback validasinote" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div> --}}
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                {{-- <select id="aksi" name="aksi" class="form-control {{ $errors->has('aksi') ? ' is-invalid' : '' }}">
                                    <option>Pilih aksi</option>
                                    <option value="1">Setujui</option>
                                    @role('boss|so web|so mes flat|so mes long|so sap hr|so sap ppqm|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging|operation ict')
                                    <option value="2">Tolak</option>
                                    @endrole
                                </select> --}}
                                <input type="hidden" name="aksi" value="" id="aksi" />
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('aksi') }}</strong>
                                </span>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="btn-group btn-group-lg" role="group">
                                <button type="submit" class="btn btn-danger" id="idtolak" onclick="return ceking(2)">
                                    Tolak
                                </button>
                                <button type="submit" class="btn btn-primary" id="idsetuju" onclick="return ceking(1)">
                                    Setuju
                                </button>
                            </div>
                        </div>

                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Pilih Lampiran (Memo Dinas):</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{-- lampiran --}}
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">                                             
                                                    <div class="input-group control-group increment">
                                                        <div class="custom-file">
                                                            <input style="color:black;" class="custom-file-input" id="attachment" type="file" name="attachment[]" class="form-control">
                                                            <span class="invalid-feedback attachment" role="alert">
                                                            </span>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="btn-group btn-group-lg" role="group">
                                            <input id="simpan" type="submit" class="btn btn-primary" value="Simpan" onclick="return simpanData()" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        @include('requests.nda')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection