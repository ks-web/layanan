@extends('layouts.app')
@section('content')
{{-- <ul class="nav nav-tabs">

</ul> --}}
@include('request_changes._tab')
<br/>
@role('employee')
	<a href="{{route('crf.create')}}" class="control">
		<button class="btn btn-primary">Buat Permintaan</button>
	</a>
@endrole
<p>&nbsp;</p>
@if (session('success'))
	<div class="alert alert-success" role="alert">
		{{ session('success') }}
	</div>
@endif
<div class="panel panel-default">
  	<div class="panel-heading">Data Permintaan / Perubahan Layanan</div>
	<div class="panel-body">
		<table id="dataChangeCrf" class="display" cellspacing="0" width="100%">
			<thead class="table-success">
				<tr>
					<th class="center" width="3%"> ID </th>
					<th class="center" width="15%">LAYANAN</th>
					<th class="center" width="15%">JUDUL</th>
					<th class="center" width="15%">KEBUTUHAN BISNIS</th>
					<th class="center" width="15%">MANFAAT TERHADAP BISNIS</th>
			        <th class="center" width="10%">PEMINTA</th>
					<th class="center" width="15%">TAHAP</th>
					<th class="center" width="15%">AKSI</th>
					<th class="center" width="10%">TANGGAL UBAH</th>
					<th> --- </th>
				</tr>
			</thead>
			<tbody>
					@foreach ($requestchange as $rc)
					<tr>
						<td>{{$rc->id}}</td>
						<td>{{$rc->service->name}}</td>
						<td>{{$rc->title}}</td>
						<td>{{str_limit($rc->business_need,50)}}</td>
						<td>{{str_limit($rc->business_benefit,50)}}</td>
						<td>{{$rc->user->idWithName}}</td>
						<td>{{$rc->stage->name}}</td>
						<td>{{$rc->action->name}}</td>
						<td>{{$rc->updated_at}}</td>
						<td>
							{{-- <a data-url="{{ route('crf.detail.show.crf', $rc->id) }}" class="btn btn-success btn-sm id-modal"><span class="glyphicon glyphicon-eye-open" data-url="{{ route('crf.detail.show.crf', $rc->id) }}"></span></a> --}}
							@include('request_changes._actions')
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
	</div>
</div>
{{-- modal --}}
<div id="modalresponse" style="margin-top:2rem">
{{--  end modal --}}
<script>
	jQuery(document).ready(function(){
		$('.id-modal').on('click',function(e){
			var token = $("meta[name='csrf-token']").attr("content");
			var url = $(event.target).data('url');
			$.ajax({ 
				url: url,
				type: "GET",
				data: {"_token": token, },
				success: function (data, textStatus, jqXHR) {
					$("#modalresponse").html(data.html);
					$('#myModal').modal('show');
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert("AJAX error: " + textStatus + ' : ' + errorThrown);
				},
			});
		});
	});
</script>
@endsection
