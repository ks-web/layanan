<ul class="nav nav-tabs">
    @role('service desk')
        <li 
            class="@if(Request::segment(1) == 'crf' && Request::segment(2) == "")
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('crf.index') }}">Sedang Berjalan 
                @if(isset($countInprogress) && $countInprogress > 0)
                    <span class="badge badge-warning">{{$countInprogress}}</span>
                @endif
            </a>
        </li>
        <li 
            class="@if(Request::segment(1) == 'crf' && Request::segment(2) == '3')
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('crf.view.perstage','3') }}">Menunggu Input Tiket 
                @if(isset($waitingApprovals) && $waitingApprovals > 0)
                    <span class="badge badge-warning">{{$waitingApprovals}}</span>
                @endif
            </a>
        </li>
        <li 
            class="@if(Request::segment(1) == 'crf' && Request::segment(2) == '9')
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('crf.view.perstage','9') }}">Selesai
                @if(isset($ticketClose) && $ticketClose > 0)
                    <span class="badge badge-warning">{{$ticketClose}}</span>
                @endif
            </a>
        </li>
    @endrole
    @hasallroles('boss')
        <li 
            class="@if(Request::segment(1) == 'crf' && Request::segment(2) == 'me')
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('crf.view.perstage','me') }}">Permintaan Saya
                @if(isset($countMyRequest) && $countMyRequest > 0)
                    <span class="badge badge-warning">{{$countMyRequest}}</span>
                @endif
            </a>
        </li>
        <li 
            class="@if(Request::segment(1) == 'crf' && Request::segment(2) == null))
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('crf.index') }}">Permintaan CRF
                @if(isset($countAllRequest) && $countAllRequest > 0)
                    {{-- <span class="badge badge-warning">{{$countAllRequest}}</span> --}}
                @endif
            </a>
        </li>
        <li 
            class="@if(Request::segment(1) == 'bps')
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('bps.index') }}">Permintaan BPS
                @if(isset($countAllRequestBps) && $countAllRequestBps > 0)
                    {{-- <span class="badge badge-warning">{{$countAllRequestBps}}</span> --}}
                @endif
            </a>
        </li>
    @endhasallroles
</ul>