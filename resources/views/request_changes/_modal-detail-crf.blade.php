<div class="modal fade" data-dismiss="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h3 class="modal-title" id="myModalLabel">Permintaan / perubahan layanan (CRF)</h3>
            </div>
            
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Customer Request Form</div>
                        <div class="panel-body">
                            @if (session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <form method="POST" action="" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Judul:</label>
                                            <br/>
                                            {{ isset($requestchange->title) ? $requestchange->title : '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="business_need">Kebutuhan Bisnis:</label>
                                            <br/>
                                            {{ isset($requestchange->business_need) ? $requestchange->business_need : ''}}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="business_benefit">Manfaat Bisnis:</label>
                                            <br/>
                                            {{ isset($requestchange->business_benefit) ? $requestchange->business_benefit : '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="business_need">Peminta:</label>
                                            <br/>
                                            {{ isset($requestchange->user->name) ? $requestchange->user->name : ''}} ( {{ isset($requestchange->user->id) ? $requestchange->user->id :'' }} )
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="business_benefit">Contact</label>
                                            <br/>
                                            Email: {{ isset($requestchange->user->email) ? $requestchange->user->email : '' }}, Telp: {{ isset($requestchange->telp) ? $requestchange->telp: '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reason">Tahapan: </label>
                                            <br/>
                                            {{ isset($requestchange->stage->name) ? $requestchange->stage->name : '' }}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reason">Nomor Tiket: </label>
                                            <br/>
                                            {{ isset($requestchange->ticket) ? $requestchange->ticket:''}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Lampiran User</div>
                                                <div class="panel-body">
                                                    @foreach($requestchange->requestChangeAttachments as $item)
                                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                        <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if(isset($requestchange->RequestChangeRejectAttachments))
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">Lampiran Memodinas</div>
                                                    <div class="panel-body">
                                                        @foreach($requestchange->RequestChangeRejectAttachments as $item)
                                                            <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                            <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- history crf --}}
                                @include('request_changes._riwayat')

                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Data Bussiness Prcess Specification</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Priority: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->priority) ? $requestchange->requestChangeBps->priority->name : '' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Difficulty: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->difficulty) ? $requestchange->requestChangeBps->difficulty->name : '' }}
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Prepared By: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->preparedby) ? $requestchange->requestChangeBps->preparedby : '' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Module: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->module) ? $requestchange->requestChangeBps->module : '' }}
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Email: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->email) ? $requestchange->requestChangeBps->email : '' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Phone: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->phone) ? $requestchange->requestChangeBps->phone : '' }}
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Start Plan Data: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->start_plan_date) ? $requestchange->requestChangeBps->start_plan_date : '' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reason">Finish Plan Data: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->finish_plan_date) ? $requestchange->requestChangeBps->finish_plan_date : '' }}
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reason">Business Process Justification: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->bpj) ? $requestchange->requestChangeBps->bpj : '' }}
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reason">Global Design: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->bpj) ? $requestchange->requestChangeBps->bpj : '' }}
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reason">Detail Specification: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->detail_specification) ? $requestchange->requestChangeBps->detail_specification : '' }}
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reason">Effort: </label>
                                                        <br/>
                                                        {{ isset($requestchange->requestChangeBps->effort) ? $requestchange->requestChangeBps->effort : '' }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Lampiran Bussiness Prcess Specification</div>
                                                <div class="panel-body">
						@if(isset($requestchange->requestChangeBps->requestChangeBpsAttachments))
                                                    @foreach($requestchange->requestChangeBps->requestChangeBpsAttachments as $item)
                                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                        <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                                    @endforeach
						@endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Time Duration Calculation</div>
                                            <div class="panel-body">
                                                <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                        <thead class="table-success">
                                                            <tr>
                                                                <td><b>Activity</b></td>                                                    
                                                                <td><b>Mandays</b></td>
                                                                <td><b>Pic</b></td>                                                   
                                                                <td><b>Level</b></td>                                                    
                                                                <td><b>Start Date</b></td>                                                   
                                                                <td><b>Finish Date</b></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="table-success">	
                                                            @if(isset($requestchange->requestChangeBps->requestChangeBpsDurations))
                                                                @foreach ($requestchange->requestChangeBps->requestChangeBpsDurations as $item)
                                                                    <tr>
                                                                        <td>{{ isset($item->activity) ? $item->activity : '' }}</td>
                                                                        <td>{{ isset($item->mandays) ? $item->mandays : '' }}</td>                  
                                                                        <td>{{ isset($item->pic) ? $item->pic : ''}}</td>                         
                                                                        <td>{{ isset($item->level_id) ? $item->level_id : ''}}</td>
                                                                        <td>{{ isset($item->start_date) ? $item->start_date : ''}}</td>
                                                                        <td>{{ isset($item->finish_date) ? $item->finish_date : ''}}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                               {{-- history --}}
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">RIWAYAT BPS</div>
                                            <div class="panel-body">
                                                <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                        <thead class="table-success">
                                                            <tr>
                                                                <td>Tanggal</td>
                                                                <td>Pengguna</td>
                                                                <td>Aksi</td>
                                                            </tr>
                                                        </thead>
                                                            <tbody class="table-success">
                                                                @if(isset($requestchange->requestChangeBps->requestChangeBpsActions))
                                                                    @foreach ($requestchange->requestChangeBps->requestChangeBpsActions as $item)
                                                                        <tr>
                                                                            <td>{{$item->created_at}}</td>
                                                                            <td>
                                                                                {{$item->user->name}}
                                                                                <br/>
                                                                                <br/>
                                                                                {{ $item->requestChangeBpsActionNotes == "" ? '' :'Note: '. $item->requestChangeBpsActionNotes->note }}
                                                                            </td>
                                                                            <td>{{$item->action->name}}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endif
                                                            </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="modalclose" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
