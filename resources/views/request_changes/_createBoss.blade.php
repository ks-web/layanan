@extends('layouts.app')
@section('content')
<script>
$(document).ready(function(){
    $('#simpan').click(function() {
        if(($('#title').val()) && ($('#idservice').val()) && ($('#business_need').val()) && ($('#business_benefit').val()) && ($('#telp').val()) && ($('#location').val())) {
            $(this).attr('disabled','disabled');
            $(this).attr('value', 'Sedang mengirim...')
            $('#formField').submit();
        }
    });    
});
</script>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan / Perubahan Layanan (CRF)</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form id="formField" class="form" method="POST" action="{{ route('crf.store') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}         

                        <div class="form-group">
                            <label for="title">Judul</label>
                            <input required style="color:black;" id="title" name="title" rows="2" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" autofocus value="{{ old('title') }}" />
                            <span class="invalid-feedback titlevalidasi" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="service_id">Layanan</label>
                            <select required style="color:black;" require id="idservice" name="service_id" class="form-control {{ $errors->has('service_id') ? ' is-invalid' : '' }}">
                                    <option value="">Pilih Layanan</option>
                                    <optgroup label="Layanan aplikasi mes">
                                        @foreach ($services as $service)
                                            @if($service->groupserv == "mes")
                                                <option value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                                    <optgroup label="Layanan aplikasi sap">
                                        @foreach ($services as $service)
                                            @if($service->groupserv == "sap")
                                                <option value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                                    <optgroup label="Layanan aplikasi lainya">
                                        @foreach ($services as $service)
                                            @if($service->groupserv != "mes" && $service->groupserv != "sap")
                                                <option value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                            </select>
                            <span class="invalid-feedback validasiserviceid" role="alert">
                                <strong>{{ $errors->first('service_id') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_need">Alasan Permintaan</label>
                            <textarea required style="color:black;" require name="business_need" id="business_need" rows="6" class="form-control {{ $errors->has('business_need') ? ' is-invalid' : '' }}" autofocus>{{ old('business_need') }}</textarea>
                            <span class="invalid-feedback validasibusiness_need" role="alert">
                                <strong>{{ $errors->first('business_need') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_benefit">Manfaat Terhadap Bisnis</label>
                            <textarea required style="color:black;" id=business_benefit name="business_benefit" rows="6" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ old('business_benefit') }}</textarea>
                            <span class="invalid-feedback validasibusiness_benefit" role="alert">
                                <strong>{{ $errors->first('business_benefit') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="telp">Nomor Telp</label>
                            <input required style="color:black;" id="telp" name="telp" rows="2" class="form-control {{ $errors->has('telp') ? ' is-invalid' : '' }}" autofocus value="{{ old('telp') }}" />
                            <span class="invalid-feedback validasitelp" role="alert">
                                <strong>{{ $errors->first('telp') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="location">Lokasi</label>
                            <input required style="color:black;" id="location" name="location" rows="2" class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }}" autofocus value="{{ old('location') }}" />
                            <span class="invalid-feedback validasilocation" role="alert">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="reason">Lampiran: Format file (image/pdf )</label>
                            <div class="input-group control-group increment">
                                <div class="custom-file">
                                    <input style="color:black;" class="custom-file-input" id="attachment" type="file" name="attachment[]" class="form-control">
                                </div>
                                <div class="input-group-btn"> 
                                    <button style="color:black;" class="btn btn-success tambah1" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                </div>
                            </div>
                            <div>
                                <span class="invalid-feedback validasiattachment" role="alert">
                                    <strong>{{ $errors->first('attachment') }}</strong>
                                </span>
                            </div>
                            <div class="clone hide">
                                <div class="control-group input-group" style="margin-top:10px">
                                    <div class="custom-file">
                                        <input class="custom-file-input" type="file" name="attachment[]" class="form-control">
                                    </div>
                                    <div class="input-group-btn"> 
                                        <button class="btn btn-danger kurang1" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group btn-group-lg" role="group">
                            <input id="simpan" type="submit" class="btn btn-primary" value="Simpan" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection