<ul class="nav nav-tabs">
    @role('so web|so mes flat|so mes long|so sap hr|so sap ppqm|operation ict|so sap fico|so sap sd|so sap pm|so sap mm|so sap psim|so perangkat komputer|so aplikasi office|so vicon|so printer|so jaringan|so messaging|manager beict|coordinator aplikasi|coordinator infra')
    <li 
            class="@if(Request::segment(1) == 'crf' && Request::segment(2) == 'me')
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('crf.view.perstage','me') }}">Permintaan Saya
                @if(isset($countMyRequest) && $countMyRequest > 0)
                    <span class="badge badge-warning">{{$countMyRequest}}</span>
                @endif
            </a>
        </li>
        <li 
            class="@if(Request::segment(1) == 'crf'  && Request::segment(2) == null)
                        active
                    @endif"
            role="presentation">
            <a href="{{ route('crf.index') }}">Permintaan CRF
                @if(isset($ticketClose) && $ticketClose > 0)
                    <span class="badge badge-warning">{{$ticketClose}}</span>
                @endif
            </a>
        </li>
        <li 
        class="@if(Request::segment(1) == 'bps')
                    active
                @endif"
        role="presentation">
        <a href="{{ route('bps.index') }}">Permintaan BPS
            @if(isset($ticketClose) && $ticketClose > 0)
                <span class="badge badge-warning">{{$ticketClose}}</span>
            @endif
        </a>
    </li>
    @endrole
</ul>