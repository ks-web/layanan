@extends('layouts.app')
@section('content')

<script>
    function ceking(isi){
        $('#aksi').val(isi);
        return true;
    }
</script>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form persetujuan permintaan / perubahan layanan (CRF)</div>
                <div class="panel-body">
                    {{-- flash session --}}
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" 
                        @role('boss')
                            action="{{ route('crf.boss.approval', $requestchange->id) }}" 
                        @endrole
                        enctype="multipart/form-data"
                    >
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        {{-- atasan --}}
                        <div class="form-group">
                            {{-- <input style="color:black;" readonly type="hidden"  name="boss" class="form-control {{ $errors->has('boss') ? ' is-invalid' : '' }}" autofocus value="{{Auth::user()->boss()->name}} ({{Auth::user()->boss()->id}})" />
                            <span class="invalid-feedback " role="alert">
                                <strong>{{ $errors->first('boss') }}</strong>
                            </span> --}}
                        </div>

                        {{-- title --}}
                        <div class="form-group">
                            <label for="title">Judul</label>
                            <input style="color:black;" id="title" name="title" rows="2" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" autofocus value="{{ $requestchange->title }}" />
                            <span class="invalid-feedback titlevalidasi" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        </div>

                        {{-- service id --}}
                        <div class="form-group">
                            <label for="service_id">Layanan</label>
                            <select style="color:black;" require id="idservice" name="service_id" class="form-control {{ $errors->has('service_id') ? ' is-invalid' : '' }}">
                                    <option value="">Pilih Layanan</option>
                                    <optgroup label="Layanan aplikasi mes">
                                        @foreach ($services as $service)
                                            @if($service->groupserv == "mes")
                                                <option 
                                                @if($requestchange->service_id == $service->id)
                                                    selected
                                                @endif
                                                value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                                    <optgroup label="Layanan aplikasi sap">
                                        @foreach ($services as $service)
                                            @if($service->groupserv == "sap")
                                                <option
                                                @if($requestchange->service_id == $service->id)
                                                    selected
                                                @endif
                                                value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                                    <optgroup label="Layanan aplikasi lainya">
                                        @foreach ($services as $service)
                                            @if($service->groupserv != "mes" && $service->groupserv != "sap")
                                                <option 
                                                @if($requestchange->service_id == $service->id)
                                                    selected
                                                @endif
                                                value={{$service->id}}>{{$service->name}}</option>  
                                            @endif
                                        @endforeach
                                    </optgroup> 
                            </select>
                            <span class="invalid-feedback validasiserviceid" role="alert">
                                <strong>{{ $errors->first('service_id') }}</strong>
                            </span>
                        </div>
                        {{-- service id --}}

                        {{-- Alasan permintaan --}}
                        <div class="form-group">
                            <label for="business_need">Alasan Permintaan</label>
                            <textarea style="color:black;" require name="business_need" id="business_need" rows="6" class="form-control {{ $errors->has('business_need') ? ' is-invalid' : '' }}" autofocus>{{ $requestchange->business_need }}</textarea>
                            <span class="invalid-feedback validasibusiness_need" role="alert">
                                <strong>{{ $errors->first('business_need') }}</strong>
                            </span>
                        </div>

                        {{-- Manfaat terhadap bisnis --}}
                        <div class="form-group">
                            <label for="business_benefit">Manfaat Terhadap Bisnis</label>
                            <textarea style="color:black;" id=business_benefit name="business_benefit" rows="6" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ $requestchange->business_benefit }}</textarea>
                            <span class="invalid-feedback validasibusiness_benefit" role="alert">
                                <strong>{{ $errors->first('business_benefit') }}</strong>
                            </span>
                        </div>

                        {{-- Nomor telp --}}
                        <div class="form-group">
                            <label for="telp">Nomor Telp</label>
                            <input style="color:black;" id="telp" name="telp" rows="2" class="form-control {{ $errors->has('telp') ? ' is-invalid' : '' }}" autofocus value="{{ $requestchange->telp }}" />
                            <span class="invalid-feedback validasitelp" role="alert">
                                <strong>{{ $errors->first('telp') }}</strong>
                            </span>
                        </div>

                        {{-- lokasi --}}
                        <div class="form-group">
                            <label for="location">Lokasi</label>
                            <input style="color:black;" id="location" name="location" rows="2" class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }}" autofocus value="{{ $requestchange->location }}" />
                            <span class="invalid-feedback validasilocation" role="alert">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                        </div>

                        {{-- Lampiran --}}
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lampiran</div>
                                <div class="panel-body">
                                    @foreach($requestchange->requestChangeAttachments as $item)
                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                        <a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span> {{$item->alias}} </span> </a><br/><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        {{-- note --}}
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea style="color:black;" id=note name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus>{{ $requestchange->note }}</textarea>
                            <span class="invalid-feedback validasinote" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div>
                    
                        {{-- button --}}
                        @role('boss')
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                <input type="hidden" name="aksi" value="" id="aksi" />
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('aksi') }}</strong>
                                </span>
                            </div>
                            
                            <div class="btn-group btn-group-lg" role="group">
                                <button type="submit" class="btn btn-danger" id="idtolak" onclick="return ceking(2)">
                                    Tolak
                                </button>
                                <button type="submit" class="btn btn-primary" id="idsetuju" onclick="return ceking(1)">
                                    Setuju
                                </button>
                            </div>
                        </div>
                        @endrole                        
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection